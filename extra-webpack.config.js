'use strict';

var webpack = require('webpack');

/**
 * Нужен для того что бы выпилить лишние локализации из moment.js
 * @type {{plugins : webpack}}
 */
module.exports = {
  plugins: [new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en-au/)]
};
