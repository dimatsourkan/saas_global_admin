import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn : 'root'
})
export class LogInterceptor implements HttpInterceptor {

  intercept(request : HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>> {

    return next.handle(request)
      .pipe(
        map(res => {
          if(res.type !== 0) {
            console.log('|');
            console.log('| ---------------------------------------------------------------------------------');
            console.log(`| ${request.url}`);
            console.log('| Request data    : ', request);
            console.log('| Request success : ', res);
            console.log('| ---------------------------------------------------------------------------------');
            console.log('|');
          }
          return res;
        }),
        catchError<HttpEvent<any>>((error) : never => {
          console.log('|');
          console.log('| ---------------------------------------------------------------------------------');
          console.log(`| ${request.url}`);
          console.log('| Request data  : ', request);
          console.log('| Request error : ', error);
          console.log('| ---------------------------------------------------------------------------------');
          console.log('|');
          throw(error);
        })
      )
  }
}
