import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';

@Injectable({
  providedIn : 'root'
})
export class TokenInterceptor implements HttpInterceptor {

  constructor(private tokenService : TokenService) {}

  intercept(request : HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>> {

    if(this.tokenService.token) {
      request = request.clone({
        setHeaders : {
          Authorization : this.tokenService.tokenHeader
        }
      });
    }

    return next.handle(request);
  }
}
