import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/internal/operators';
import { ENVIRONMENT } from '../../../environments/environment';
import { RequestErrorService } from '../services/request-error.service';

@Injectable({
  providedIn : 'root'
})
export class RequestInterceptor implements HttpInterceptor {

  constructor(private requestErrorService : RequestErrorService) {

  }

  intercept(request : HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError<HttpEvent<any>>((e) => this.catchErrors(e))
      )
  }

  catchErrors(error : HttpErrorResponse) : never {

    if(ENVIRONMENT.ERROR_NOTIFICATIONS) {
      this.requestErrorService.showError(error);
    }

    throw(error);
  }
}
