import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { LanguageService } from '../services/language.service';

@Injectable({
  providedIn : 'root'
})
export class LanguageInterceptor implements HttpInterceptor {

  constructor(private languageService : LanguageService) {}

  intercept(request : HttpRequest<any>, next : HttpHandler) : Observable<HttpEvent<any>> {

    request = request.clone({
      setHeaders : {
        Language : this.languageService.currentLang
      }
    });

    return next.handle(request);
  }
}
