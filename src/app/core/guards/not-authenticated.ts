import { CanActivate, CanLoad, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ApiUrlService } from '../services/api-url.service';



@Injectable({
  providedIn : 'root'
})
export class NotAuthenticated implements CanActivate, CanLoad {

  constructor(
    private apiUrlService : ApiUrlService,
    private authService : AuthService,
    private router : Router
  ) {

  }

  activate() : Promise<boolean> {

    return new Promise((resolve) => {

      if(!this.authService.isAuthenticated) {
        return resolve(true);
      }

      this.router.navigate(['/', this.apiUrlService.linkPathFromRole()]);
      return resolve(false);

    });
  }

  canActivate() : Promise<boolean> {
    return this.activate();
  }

  canLoad() : Promise<boolean> {
    return this.activate();
  }

}
