import { CanActivate, CanLoad, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { HOME_ROUTE } from '../../app.constants';



@Injectable({
  providedIn : 'root'
})
export class IsAuthenticated implements CanActivate, CanLoad {
  constructor(private authService : AuthService,
              private router : Router) {
  }

  activate() : Promise<boolean> {
    return new Promise((resolve) => {

      if(this.authService.isAuthenticated) {
        return resolve(true);
      }

      this.router.navigate([HOME_ROUTE]);
      return resolve(false);

    });
  }

  canActivate() : Promise<boolean> {
    return this.activate();
  }

  canLoad() : Promise<boolean> {
    return this.activate();
  }

}
