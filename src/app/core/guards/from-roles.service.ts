import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn : 'root'
})
export class FromRoles implements CanActivate, CanLoad {
  constructor(
    protected authService : AuthService,
    protected router : Router,
  ) {
  }

  activate(route : Route|ActivatedRouteSnapshot) : Promise<boolean> {

    return new Promise((resolve) => {

      if(this.authService.fromRoles(route.data.roles)) {

        this.onSuccess();
        return resolve(true);

      } else {

        this.onError();
        return resolve(false);

      }

    });

  }

  onSuccess() {

  }

  onError() {

  }

  canActivate(route : ActivatedRouteSnapshot) : Promise<boolean> {
    return this.activate(route);
  }

  canLoad(route : Route) : Promise<boolean> {
    return this.activate(route);
  }

}
