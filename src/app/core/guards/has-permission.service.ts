import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn : 'root'
})
export class HasPermission implements CanActivate, CanLoad {
  constructor(
    protected authService : AuthService,
    protected router : Router,
  ) {
  }

  activate(route : Route|ActivatedRouteSnapshot) : Promise<boolean> {

    return new Promise((resolve) => {
      if(this.checkPermission(route.data.permissions)) {

        this.onSuccess();
        return resolve(true);

      } else {

        this.onError();
        this.router.navigate(['/']);
        return resolve(false);

      }

    });

  }

  private checkPermission(permissions : any[]) {
    return this.authService.hasPermissions(permissions);
  }

  onSuccess() {

  }

  onError() {

  }

  canActivate(route : ActivatedRouteSnapshot) : Promise<boolean> {
    return this.activate(route);
  }

  canLoad(route : Route) : Promise<boolean> {
    return this.activate(route);
  }

}
