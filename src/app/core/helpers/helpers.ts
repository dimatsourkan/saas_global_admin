import { Injectable } from '@angular/core';
import { BaseModel } from '../models/base.model';
import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';
import forEach from 'lodash-es/forEach';
import { ClassTransformOptions, plainToClass } from 'class-transformer';

/**
 * Класс с вспомогательными функциями
 */
@Injectable({
  providedIn : 'root'
})
export class Helpers {

  /**
   * Функция добавляется к циклам в массиве для элементов у которых есть ID
   * Ускоряет перерисовку интерфейса при изменении данных
   * Почитать можно тут:
   * https://medium.freecodecamp.org/best-practices-for-a-clean-and-performant-angular-application-288e7b39eb6f
   * @param index
   * @param item
   */
  trackById(index : number, item : BaseModel) {
    return item.id;
  }

}

/**
 * Обновляет данные формы из переданного объекта
 * @param form
 * @param data
 */
export function patchFormValues(form : FormGroup, data : Object) {

  form.patchValue(data);

  forEach(data, (dataItem, key) => {

    let control : AbstractControl = form.controls[key];

    if(control instanceof FormGroup) {
      patchFormValues(control, dataItem);
    }

    if(control instanceof FormArray) {

      forEach(dataItem, (value, i) => {
        if(control[i]) {
          patchFormValues(control[i], value);
        }
      });
    }

  });

  return form;
}

/**
 * Проверяет наличие ключа в объекте
 */
export function hasKey(object : Object, key : string) {
  return Object.prototype.hasOwnProperty.call(object, key);
}

/**
 * Отдает ширину скроллбара
 */
export const scrollbarWidth : number = (function () {
  let outer = document.createElement('div');
  outer.style.visibility = 'hidden';
  outer.style.width = '100px';
  outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps

  document.body.appendChild(outer);

  let widthNoScroll = outer.offsetWidth;
  // force scrollbars
  outer.style.overflow = 'scroll';

  // add innerdiv
  let inner = document.createElement('div');
  inner.style.width = '100%';
  outer.appendChild(inner);

  let widthWithScroll = inner.offsetWidth;

  // remove divs
  outer.parentNode.removeChild(outer);

  return widthNoScroll - widthWithScroll;
})();

/**
 * Отдает URLSearchParams с текущими параметрами из урл
 */
export function UrlSearchParams() : URLSearchParams {
  return new URLSearchParams(location.search);
}

/**
 * Парсит строку вида key[key2][key3] в массив вида [key, key2, key3]
 * @param key
 */
export function parseValidationKeys(key : string) : string[] {

  let keys : string[] = key.split('[');

  for(let i = 0; i < keys.length; i++) {
    keys[i] = keys[i].replace(/\]/, '');
  }

  return keys;

}

export function myPlainToClass<T>(
  this : new (...args : any[]) => T,
  plain : Object = {},
  options : ClassTransformOptions = {}) {
  return <T>plainToClass(this, plain, options);
}
