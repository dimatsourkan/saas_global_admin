import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { Permission } from './permission.model';
import { DataStore } from '../../data-store/data-store.service';
import { Crud } from '../../decorators/crud.decorator';
import { PermissionData } from '../../data-store/data-models/permission-data.model';
import { Observable } from 'rxjs';
import { ResultList } from '../../models/result.model';
import { HttpParams } from '@angular/common/http';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn : 'root'
})
@Crud('permission', Permission, DataStore.permission)
export class PermissionService extends CRUDService<Permission, PermissionData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  private queryCache$ : Observable<ResultList<Permission>>;

  getAll(params ? : HttpParams) : Observable<ResultList<Permission>> {

    if(!this.queryCache$ || !DataStore.permission.list.getValue().items.length) {
      this.queryCache$ = super.getAll(params).pipe(
        shareReplay(1)
      );
    }

    return this.queryCache$;

  }
}
