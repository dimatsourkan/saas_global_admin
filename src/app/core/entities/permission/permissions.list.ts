export enum PERMISSIONS {
  MANAGE_ROLES = 'manage_roles',
  MANAGE_PROJECTS = 'manage_projects',
  MANAGE_BLOCKED_DOMAINS = 'manage_blocked_domains',

  MANAGE_MANAGERS = 'manage_managers',
  VIEW_MANAGERS = 'view_managers',

  MANAGE_PRODUCTS = 'manage_products',
  MANAGE_PRODUCT_PLANS = 'manage_product_plans',

  MANAGE_TENANTS = 'manage_tenants',
  VIEW_TENANTS = 'view_tenants',

  MANAGE_MAIL_GROUPS = 'manage_mail_groups',
  MASS_MAILING = 'mass_mailing',
  MAILBOX = 'mailbox',
  MANAGE_PROJECT_REQUESTS = 'manage_project_requests',
  MANAGE_INVOICES = 'manage_invoices',
  ASSIGN_MANAGER = 'assign_manager',
}
