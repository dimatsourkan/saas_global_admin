import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { DataStore } from '../../data-store/data-store.service';
import { TenantData } from '../../data-store/data-models/tenant-data.model';
import { Tenant } from './tenant.model';
import { Crud } from '../../decorators/crud.decorator';
import { Observable } from 'rxjs';

@Injectable({
  providedIn : 'root'
})
@Crud('manager/tenant', Tenant, DataStore.tenant)
export class TenantService extends CRUDService<Tenant, TenantData> {

  unsubscribe(token : string) : Observable<any> {
    return this.httpClient
      .get(`${this.apiUrl}/unsubscribe/${token}`, {headers : this.headers});
  }

  unsubscribeGroup(token : string, groupId : string) : Observable<any> {
    return this.httpClient
      .get(`${this.apiUrl}/unsubscribe/${token}/${groupId}`, {headers : this.headers});
  }

  acceptTerms() : Observable<any> {
    return this.httpClient
      .post(`${this.apiUrl}/tenant/accept_terms`, {}, {headers : this.headers});
  }

  requestEmailConfirmation() : Observable<any> {
    return this.httpClient
      .post(`${this.apiUrl}/tenant/request_email_confirmation`, {}, {headers : this.headers});
  }

}
