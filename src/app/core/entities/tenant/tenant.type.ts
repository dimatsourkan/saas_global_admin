import { TENANT_STATUS, TENANT_TYPE } from './tenant.enum';

export type tenantType = TENANT_TYPE.COMPANY | TENANT_TYPE.SELF_EMPLOYED;
export type tenantStatus =
  TENANT_STATUS.PENDING |
  TENANT_STATUS.ACTIVE |
  TENANT_STATUS.SUSPENDED |
  TENANT_STATUS.DEACTIVATED;
