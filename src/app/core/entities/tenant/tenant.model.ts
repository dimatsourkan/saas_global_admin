import { User } from '../user/user.model';
import { Project } from '../project/project.model';
import { Type } from 'class-transformer';
import { TENANT_STATUS, TENANT_TYPE } from './tenant.enum';
import { tenantStatus, tenantType } from './tenant.type';

export class Tenant extends User {

  @Type(() => Project)
  projects : Project[];

  type : tenantType = TENANT_TYPE.SELF_EMPLOYED;
  status : tenantStatus = TENANT_STATUS.ACTIVE;
  notes : string = null;
  managerId : string = null;

  get isCompany() {
    return this.type === TENANT_TYPE.COMPANY;
  }

  get isSelfEmployed() {
    return this.type === TENANT_TYPE.SELF_EMPLOYED;
  }

  get isActiveStatus() {
    return this.status === TENANT_STATUS.ACTIVE;
  }

  get isPendingStatus() {
    return this.status === TENANT_STATUS.PENDING;
  }

  get isSuspendedStatus() {
    return this.status === TENANT_STATUS.SUSPENDED;
  }

  get isDeactivatedStatus() {
    return this.status === TENANT_STATUS.DEACTIVATED;
  }

}
