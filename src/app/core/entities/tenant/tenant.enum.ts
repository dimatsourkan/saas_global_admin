import { User } from '../user/user.model';
import { Project } from '../project/project.model';
import { Type } from 'class-transformer';

export enum TENANT_TYPE {
  SELF_EMPLOYED = 1,
  COMPANY = 2
}

export enum TENANT_STATUS {
  PENDING = 1,
  ACTIVE = 2,
  SUSPENDED = 3,
  DEACTIVATED = 4
}

type tenantType = TENANT_TYPE.COMPANY | TENANT_TYPE.SELF_EMPLOYED;
type tenantStatus = TENANT_STATUS.PENDING | TENANT_STATUS.ACTIVE | TENANT_STATUS.SUSPENDED | TENANT_STATUS.DEACTIVATED;
