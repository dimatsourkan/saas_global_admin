import { BaseModel } from '../../models/base.model';
import { ProductPlan, ProductPlanLimit } from '../product-plan/product-plan.model';
import { Product } from '../product/product.model';
import { Project } from '../project/project.model';
import { PROJECT_STATUS } from '../project/project.enum';
import { Type } from 'class-transformer';
import { Tenant } from '../tenant/tenant.model';

export class ProjectRequestItem extends BaseModel {
  @Type(() => ProductPlan)
  productPlan : ProductPlan = new ProductPlan();
  @Type(() => Product)
  product : Product = new Product();
  @Type(() => ProductPlanLimit)
  extraLimits : ProductPlanLimit[] = [];
}

class Amount {
  amount : number = 0;
  currency : string = null;
}

export class ProjectRequest extends BaseModel {

  @Type(() => Tenant)
  tenant : Tenant = new Tenant();

  @Type(() => Project)
  project : Project = new Project();

  @Type(() => ProjectRequestItem)
  items : ProjectRequestItem[] = [];

  @Type(() => Amount)
  tax : Amount = new Amount();

  tenantId : string = null;
  payerName : string = null;
  status : number = null;

  get isPending() : boolean {
    return this.status === PROJECT_STATUS.PENDING;
  }
  get isActivated() : boolean {
    return this.status === PROJECT_STATUS.ACTIVATED;
  }
  get isDeactivated() : boolean {
    return this.status === PROJECT_STATUS.DEACTIVATED;
  }

}
