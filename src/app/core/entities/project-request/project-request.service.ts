import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { ProjectRequest } from './project-request.model';
import { DataStore } from '../../data-store/data-store.service';
import { Crud } from '../../decorators/crud.decorator';
import { ProjectRequestData } from '../../data-store/data-models/project-request-data.model';
import { tap } from 'rxjs/operators';
import { DataStoreHelper } from '../../data-store/data-store.helper';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
@Crud('project_requests', ProjectRequest, DataStore.projectRequest)
export class ProjectRequestService extends CRUDService<ProjectRequest, ProjectRequestData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  getNewRequestCount() : Observable<{ count : number }> {
    return this.httpClient
      .get(this.getUrl(`pending/count`), {headers : this.headers})
      .pipe(tap((res : { count : any }) =>
        DataStoreHelper.setDataCustom(this.dataStore, 'count', res.count)))
  }

  makeInvoice(id : string) : Observable<any> {
    return this.httpClient
      .post(this.getUrl(`${id}/invoice`), {}, {headers : this.headers});
  }

  approve(id : string) : Observable<any> {
    return this.httpClient
      .post(this.getUrl(`${id}/approve`), {}, {headers : this.headers});
  }

  decline(id : string) : Observable<any> {
    return this.httpClient
      .post(this.getUrl(`${id}/decline`), {}, {headers : this.headers});
  }

}

