import { BaseModel } from '../../models/base.model';
import { CURRENCIES } from '../../enums/currencies.enum';
import { Type } from 'class-transformer';

class PLanLimitPrice {
  amount : number = null;
  currency : string = CURRENCIES.USD;
}

export class ProductPlanLimit {
  @Type(() => PLanLimitPrice)
  extraUnitPrice : PLanLimitPrice = new PLanLimitPrice();
  name : string = null;
  value : number = null;
}

class ProductPlanPrice {
  amount : number = null;
  currency : string = CURRENCIES.USD;
}

export class ProductPlan extends BaseModel {
  @Type(() => ProductPlanLimit)
  limits : ProductPlanLimit[] = [];

  @Type(() => ProductPlanPrice)
  price : ProductPlanPrice = new ProductPlanPrice();

  shortDescription : string = null;
  description : string = null;
  custom : boolean = false;
  name : string = null;
  active : boolean = true;
}
