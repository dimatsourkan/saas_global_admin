import { TestBed } from '@angular/core/testing';

import { ProductPlanService } from './product-plan.service';

describe('ProductService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductPlanService = TestBed.get(ProductPlanService);
    expect(service).toBeTruthy();
  });
});
