import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { DataStore } from '../../data-store/data-store.service';
import { ProductPlan } from './product-plan.model';
import { ProductPlanData } from '../../data-store/data-models/product-plan-data.model';
import { Crud } from '../../decorators/crud.decorator';

@Injectable({
  providedIn: 'root'
})
@Crud('product', ProductPlan, DataStore.productPlan)
export class ProductPlanService extends CRUDService<ProductPlan, ProductPlanData> {

  private _productId : string;

  set productId(id : string) {
    this._productId = id;
  }

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  get baseUrl() {
    return `${this.apiUrl}/${this.path}/${this._productId}/plan`;
  }

}

