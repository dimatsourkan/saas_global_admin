import { BaseModel } from '../../models/base.model';
import { Transform, Type } from 'class-transformer';
import { User } from '../user/user.model';
import { PROJECT_STATUS } from './project.enum';
import { moment } from 'src/app/core/helpers/moment';
import { BASE_DATE_FORMAT } from '../../../app.constants';

class ProjectLimit {
  available : number = null;
  id : number = null;
  name : string = null;
  total : number = null;
  extra : number = null;
  initial : number = null;
  used : number = null;
}

class ProjectProduct {
  id : string = null;
  name : string = null;
}

class ProjectProductPlan {
  id : string = null;
  name : string = null;
}

class ProjectTrial {

  @Transform(value => value ? moment.utc(value).format(BASE_DATE_FORMAT) : null)
  startDate : string = null;

  @Transform(value => value ? moment.utc(value).format(BASE_DATE_FORMAT) : null)
  endDate : string = null;

  notes : string = null;
  isActive : boolean = false;
}

export class ProjectItem {
  @Type(() => ProjectLimit)
  limits : ProjectLimit[] = [];
  @Type(() => ProjectProduct)
  product : ProjectProduct = new ProjectProduct();
  @Type(() => ProjectProductPlan)
  productPlan : ProjectProductPlan = new ProjectProductPlan();
}

export class Project extends BaseModel {

  @Type(() => ProjectItem)
  items : ProjectItem[] = [];

  @Type(() => User)
  tenant : User = new User();

  @Type(() => ProjectTrial)
  @Transform(value => value ? value : new ProjectTrial())
  trialPeriod : ProjectTrial = new ProjectTrial();

  domain : string = null;
  name : string = null;
  status : number = null;
  tenantId : string = null;

  get trialEndAfter() : string {
    return moment(this.trialPeriod.endDate).fromNow(true);
  }

  get isPending() : boolean {
    return this.status === PROJECT_STATUS.PENDING;
  }
  get isActivated() : boolean {
    return this.status === PROJECT_STATUS.ACTIVATED;
  }
  get isDeactivated() : boolean {
    return this.status === PROJECT_STATUS.DEACTIVATED;
  }

}
