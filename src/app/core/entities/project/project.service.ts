import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { Project } from './project.model';
import { DataStore } from '../../data-store/data-store.service';
import { ProjectData } from '../../data-store/data-models/project-data.model';
import { Crud } from '../../decorators/crud.decorator';
import { ProjectRequest } from '../project-request/project-request.model';

@Injectable({
  providedIn: 'root'
})
@Crud('project', Project, DataStore.project)
export class ProjectService extends CRUDService<Project, ProjectData> {

  /**
   * Запрос для тенанта на создание проекта
   * @param id
   * @param data
   */
  request(id : string, data : ProjectRequest) {
    return this.httpClient
      .post(this.getUrl(`${id}/request`), data, {headers : this.headers});
  }

  activate(id : string) {
    return this.httpClient
      .post(this.getUrl(`${id}/activate`), {}, {headers : this.headers});
  }

  deactivate(id : string) {
    return this.httpClient
      .post(this.getUrl(`${id}/deactivate`), {}, {headers : this.headers});
  }

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

}

