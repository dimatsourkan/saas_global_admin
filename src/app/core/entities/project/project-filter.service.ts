import { FilterService, Params } from '../../services/filter.service';

class ProjectParams extends Params {
  tenantId : string = '';
}

export class ProjectFilter extends FilterService {

  protected params = new ProjectParams();

  /**
   * Tenant filter
   * @param tenantId
   */
  set tenantId(tenantId : string) {
    this.params.tenantId = tenantId;
    this.emitChange();
  }

  get tenantId() {
    return this.params.tenantId;
  }

}
