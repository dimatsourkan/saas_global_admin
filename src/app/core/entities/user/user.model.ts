import { BaseModel, modelId } from '../../models/base.model';
import { Transform, Type } from 'class-transformer';

class Person {
  firstName : string = null;
  lastName : string = null;
}

class Address {
  @Transform(value => value ? value.toUpperCase() : null)
  countryCode : string = null;

  state : string = null;
  city : string = null;
  address : string = null;
  zipCode : string = null;
}

class BillingInfo {
  @Transform(value => Number(value))
  taxPercent : number = 0;

  company : string = null;
  invoiceEmail : string = null;
  vat : string = null;
}

class Level {
  isEmailConfirmed : boolean = false;
  isProfileFilled : boolean = false;
  isTermsAccepted : boolean = false;
}

class UserMailGroup {
  id : modelId = '';
  name : string = '';
  subscribed : boolean = false;
}

export class User extends BaseModel {

  email : string;
  phone : string;
  billingAddress : string;

  @Transform(value => Number(value))
  type : number;

  @Transform(value => Number(value))
  status : number;

  @Type(() => Person)
  person : Person = new Person();

  @Type(() => Address)
  address : Address = new Address();

  @Type(() => BillingInfo)
  billingInfo : BillingInfo = new BillingInfo();

  @Type(() => Level)
  level : Level = new Level();

  @Type(() => UserMailGroup)
  mailGroups : UserMailGroup[];

  get fullName() {
    return `${this.person.firstName} ${this.person.lastName}`;
  }

  get showName() {
    if(!this.person.firstName) {
      return this.email;
    } else {
      return this.fullName;
    }
  }

}
