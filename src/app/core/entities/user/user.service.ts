import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { User } from './user.model';
import { DataStore } from '../../data-store/data-store.service';
import { UserData } from '../../data-store/data-models/user-data.model';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { DataStoreHelper } from '../../data-store/data-store.helper';
import { Crud } from '../../decorators/crud.decorator';

@Injectable({
  providedIn : 'root'
})
@Crud(null, User, DataStore.user)
export class UserService extends CRUDService<User, UserData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  profile() : Observable<User> {
    return this.httpClient
      .get(this.getUrl('profile'), {headers : this.headers}).pipe(
        map((res : any) : User => this.wrapObject(res)),
        tap((res : User) =>
          DataStoreHelper.setDataCustom(this.dataStore, 'current', res))
      );
  }

  updateProfile(data : User) : Observable<User> {
    return this.httpClient
      .post(this.getUrl('profile'), data, {headers : this.headers}).pipe(
        map((res : any) : User => this.wrapObject(res)),
        tap((res : User) =>
          DataStoreHelper.setDataCustom(this.dataStore, 'current', data))
      );
  }

  downloadData() : Observable<any> {
    return this.httpClient
      .get(this.getUrl('download'), {
        headers : this.headers,
        responseType: 'blob',
        observe: 'response'
      });
  }

}
