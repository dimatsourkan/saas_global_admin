import { ROLES } from './user.enum';

export type role = ROLES.MANAGER | ROLES.TENANT;
