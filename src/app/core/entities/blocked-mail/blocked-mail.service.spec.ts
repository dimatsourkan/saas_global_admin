import { TestBed } from '@angular/core/testing';

import { BlockedMailService } from './blocked-mail.service';

describe('MailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BlockedMailService = TestBed.get(BlockedMailService);
    expect(service).toBeTruthy();
  });
});
