import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { BlockedMail } from './blocked-mail.model';
import { DataStore } from '../../data-store/data-store.service';
import { BlockedMailData } from '../../data-store/data-models/blocked-mail-data.model';
import { Crud } from '../../decorators/crud.decorator';

@Injectable({
  providedIn: 'root'
})
@Crud('blocked_mail_domains', BlockedMail, DataStore.blockedMail)
export class BlockedMailService extends CRUDService<BlockedMail, BlockedMailData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

}

