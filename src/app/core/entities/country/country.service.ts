import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { Country } from './country.model';
import { DataStore } from '../../data-store/data-store.service';
import { CountryData } from '../../data-store/data-models/country-data.model';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { shareReplay } from 'rxjs/operators';
import { ResultList } from '../../models/result.model';
import { Crud } from '../../decorators/crud.decorator';
import { plainToClassFromExist } from 'class-transformer';

@Injectable({
  providedIn : 'root'
})
@Crud('country', Country, DataStore.country)
export class CountryService extends CRUDService<Country, CountryData> {

  private queryCache$ : Observable<ResultList<Country>>;

  getAll(params ? : HttpParams) {

    if(!this.queryCache$ || !DataStore.country.list.getValue().items.length) {
      this.queryCache$ = super.getAll(params).pipe(
        shareReplay(1)
      );
    }

    return this.queryCache$;

  }

  /**
   * Переопределил метод потому что сервер отдает список без пагинации,
   * Базовый сервис ожидает что там будет пагинация
   * @param res
   */
  protected wrapObjects(res : any) {
    return plainToClassFromExist(new ResultList<Country>(Country), { items : res });
  }

}
