import { BaseModel } from '../../models/base.model';
import { Transform } from 'class-transformer';

export class Country extends BaseModel {
  @Transform(code => code.toUpperCase())
  code : string = null;
  name : string = null;
}
