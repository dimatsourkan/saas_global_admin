export enum MESSAGE_TYPES {
  STARRED = 'starred',
  INBOX = 'inbox',
  SENT = 'sent',
}
