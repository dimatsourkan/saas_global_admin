import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { Mailbox } from './mailbox.model';
import { DataStore } from '../../data-store/data-store.service';
import { MailboxData } from '../../data-store/data-models/mailbox-data.model';
import { Crud } from '../../decorators/crud.decorator';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ResultList } from '../../models/result.model';
import { map, tap } from 'rxjs/operators';
import { DataStoreHelper } from '../../data-store/data-store.helper';

@Injectable({
  providedIn: 'root'
})
@Crud('mailbox', Mailbox, DataStore.mailbox)
export class MailboxService extends CRUDService<Mailbox, MailboxData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  /**
   * GET запрос для получения списка моделей
   * @param path - Объект с гет параметрами
   * @param params - Объект с гет параметрами
   * @returns {Observable<Object>}
   */
  private getMailbox(path : string, params ? : HttpParams) : Observable<ResultList<Mailbox>> {
    return this.httpClient
      .get(this.getUrl(path), {headers : this.headers, params}).pipe(
        map((res : ResultList<Mailbox>) : ResultList<Mailbox> => this.wrapObjects(res)),
        tap((res : ResultList<Mailbox>) => DataStoreHelper.setDataList(this.dataStore, res))
      );
  }

  inbox(params ? : HttpParams) {
    return this.getMailbox('inbox', params);
  }

  sent(params ? : HttpParams) {
    return this.getMailbox('sent', params);
  }

  starred(params ? : HttpParams) {
    return this.getMailbox('starred', params);
  }

  markRead(id : string) : Observable<Mailbox> {
    return this.httpClient
      .post(this.getUrl(`${id}/markRead`), {}, {headers : this.headers}).pipe(
        map((res : any) : Mailbox => this.wrapObject(res)),
        tap((res : Mailbox) => {
          DataStoreHelper.updateItemInList(res, this.dataStore);
          let count : any = this.dataStore.count.getValue() - 1;
          if(count >= 0) {
            DataStoreHelper.setDataCustom(this.dataStore, 'count', count);
          }
        })
      );
  }

  toggleStarred(id : string) : Observable<Mailbox> {
    return this.httpClient
      .post(this.getUrl(`${id}/starred`), {}, {headers : this.headers}).pipe(
        map((res : any) : Mailbox => this.wrapObject(res)),
        tap((res : Mailbox) => DataStoreHelper.updateItemInList(res, this.dataStore))
      );
  }

  getNewUnreadCount() : Observable<{ count : number }> {
    return this.httpClient
      .get(this.getUrl(`unread/counter`), {headers : this.headers})
      .pipe(tap((res : { count : any }) =>
        DataStoreHelper.setDataCustom(this.dataStore, 'count', res.count)))
  }

}

