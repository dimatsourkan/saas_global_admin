import { BaseModel } from '../../models/base.model';
import { Type } from 'class-transformer';

export class Mailbox extends BaseModel {

  @Type(() => Date)
  sentAt : Date = new Date();
  subject : string = null;
  message : string = null;
  from : string = null;
  fromName : string = null;
  read : boolean = false;
  starred : boolean = false;
  to : string = null;
  toName : string = null;

}
