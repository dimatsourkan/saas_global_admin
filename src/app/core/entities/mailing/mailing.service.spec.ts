import { TestBed } from '@angular/core/testing';

import { MailingService } from './mailing.service';

describe('MailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MailingService = TestBed.get(MailingService);
    expect(service).toBeTruthy();
  });
});
