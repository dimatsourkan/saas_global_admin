import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { MailingSend, MailingGroup } from './mailing.model';
import { DataStore } from '../../data-store/data-store.service';
import { Crud } from '../../decorators/crud.decorator';
import { MailingData } from '../../data-store/data-models/mailing-data.model';
import { Observable } from 'rxjs';
import { ResultList } from '../../models/result.model';
import { map, tap } from 'rxjs/operators';
import { HttpParams } from '@angular/common/http';
import { DataStoreHelper } from '../../data-store/data-store.helper';
import { plainToClass } from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
@Crud('mailing/group', MailingGroup, DataStore.mailing)
export class MailingService extends CRUDService<MailingGroup, MailingData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  getAll(params ? : HttpParams) : Observable<ResultList<MailingGroup>> {
    return this.httpClient
      .get(this.getUrl(), {headers : this.headers, params}).pipe(
        map((res : MailingGroup[]) : ResultList<MailingGroup> => this.wrapObjects({ items : res })),
        tap((res : ResultList<MailingGroup>) => DataStoreHelper.setDataList(this.dataStore, res))
      );
  }

  send(data : MailingSend) : Observable<MailingSend> {
    return this.httpClient
      .post(`${this.apiUrl}/mailing/send`, data, {headers : this.headers}).pipe(
        map((res : any) : MailingSend => MailingSend.plainToClass(res))
      );
  }

  subscriptions(params ? : HttpParams) : Observable<string[]> {
    return this.httpClient
      .get(`${this.apiUrl}/mailing/subscriptions`, {headers : this.headers, params}).pipe(
        tap((res : any) => DataStoreHelper.setDataCustom(this.dataStore, 'subscriptions', res))
      );
  }

}

