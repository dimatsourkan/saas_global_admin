import { BaseModel } from '../../models/base.model';

export class MailingGroup extends BaseModel {
  name : string = null;
  emails : string[] = [];
  default : boolean = false;
}

export class MailingSend extends BaseModel {
  body : string = null;
  emails : string[] = [];
  groups : number[] = [];
  subject : string = null;
}
