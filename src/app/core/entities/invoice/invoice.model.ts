import { BaseModel } from '../../models/base.model';
import { Type } from 'class-transformer';
import { Project } from '../project/project.model';
import { CURRENCIES } from '../../enums/currencies.enum';
import { ProductPlan } from '../product-plan/product-plan.model';
import { Product } from '../product/product.model';
import { Manager } from '../manager/manager.model';
import { Tenant } from '../tenant/tenant.model';
import { INVOICE_STATUS, INVOICE_TYPE } from './invoice.enum';
import { myPlainToClass } from '../../helpers/helpers';

class InvoiceExtra {
  name : string = null;
  value : number = null;
}

class Amount {
  amount : number = 0;
  currency : string = CURRENCIES.USD;
}

export class InvoiceTotal {

  static plainToClass = myPlainToClass;

  @Type(() => Amount)
  grandTotalAmount : Amount = new Amount();

  @Type(() => Amount)
  totalAmount : Amount = new Amount();

  @Type(() => Amount)
  tax : Amount = new Amount();
}

export class InvoiceItem {
  @Type(() => ProductPlan)
  productPlan : ProductPlan = new ProductPlan();

  @Type(() => Product)
  product : Product = new Product();

  @Type(() => InvoiceExtra)
  extraLimits : InvoiceExtra[] = [];

  @Type(() => Amount)
  price : Amount = new Amount();

  id : string;
}

export class Invoice extends BaseModel {
  @Type(() => Project)
  project : Project = new Project();

  @Type(() => Manager)
  manager : Manager = new Manager();

  @Type(() => Tenant)
  tenant : Tenant = new Tenant();

  @Type(() => Amount)
  discount : Amount = new Amount();

  @Type(() => InvoiceItem)
  items : InvoiceItem[] = [];

  @Type(() => Amount)
  grandTotalAmount : Amount = new Amount();

  @Type(() => Amount)
  totalAmount : Amount = new Amount();

  @Type(() => Amount)
  tax : Amount = new Amount();


  payer : string = null;
  status : number = INVOICE_STATUS.NEW;
  type : number = INVOICE_TYPE.REGULAR;

  get isRegular() {
    return this.type === INVOICE_TYPE.REGULAR;
  }

  get isOneTime() {
    return this.type === INVOICE_TYPE.ONE_TIME;
  }

  get isNew() {
    return this.status === INVOICE_STATUS.NEW;
  }

  get isUnpaid() {
    return this.status === INVOICE_STATUS.UNPAID;
  }

  get isPaid() {
    return this.status === INVOICE_STATUS.PAID;
  }

  get isPending() {
    return this.status === INVOICE_STATUS.PENDING;
  }

  get isCanceled() {
    return this.status === INVOICE_STATUS.CANCELED;
  }
}
