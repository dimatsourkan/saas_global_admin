export enum INVOICE_TYPE {
  ONE_TIME = 1,
  REGULAR = 2,
}

export enum INVOICE_STATUS {
  NEW = 1,
  PENDING = 2,
  PAID = 3,
  UNPAID = 4,
  CANCELED = 5
}
