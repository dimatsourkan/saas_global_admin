import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { Invoice, InvoiceTotal } from './invoice.model';
import { DataStore } from '../../data-store/data-store.service';
import { Crud } from '../../decorators/crud.decorator';
import { InvoiceData } from '../../data-store/data-models/invoice-data.model';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { DataStoreHelper } from '../../data-store/data-store.helper';
import { plainToClass } from 'class-transformer';
import { ProjectRequest } from '../project-request/project-request.model';

@Injectable({
  providedIn: 'root'
})
@Crud('invoice', Invoice, DataStore.invoice)
export class InvoiceService extends CRUDService<Invoice, InvoiceData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  sendToPayer(id : string) : Observable<Invoice> {
    return this.httpClient
      .post(this.getUrl(`${id}/send`), {}, {headers : this.headers}).pipe(
        map((res : any) : Invoice => this.wrapObject(res))
      );
  }

  getTotal(invoice : Invoice|ProjectRequest) : Observable<InvoiceTotal> {
    return this.httpClient
      .post(this.getUrl(`totals`), invoice, {headers : this.headers}).pipe(
        map(total => InvoiceTotal.plainToClass(total))
      )
  }

  changeStatus(data : Invoice, status : number) : Observable<Invoice> {
    return this.httpClient
      .post(this.getUrl(`${data.id}/status`), { status }, {headers : this.headers}).pipe(
        map((res : any) : Invoice => this.wrapObject(res)),
        tap((res : Invoice) => DataStoreHelper.updateItemInList(data, this.dataStore))
      );
  }

}

