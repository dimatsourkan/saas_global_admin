import { FilterService, Params } from '../../services/filter.service';

class InvoiceParams extends Params {
  payer : string = null;
  managerId : string = null;
  tenantId : string = null;
  projectId : string = null;
  invoiceId : string = '';
  amountFrom : number = null;
  amountTo : number = null;
  'statuses[]' : string[] = null;
  type : number = null;
}

export class InvoiceFilter extends FilterService {

  protected params = new InvoiceParams();

  /**
   * Payer filter
   * @param payer
   */
  set payer(payer : string) {
    this.params.payer = payer;
    this.emitChange();
  }

  get payer() {
    return this.params.payer;
  }

  /**
   * Manager filter
   * @param managerId
   */
  set managerId(managerId : string) {
    this.params.managerId = managerId;
    this.emitChange();
  }

  get managerId() {
    return this.params.managerId;
  }

  /**
   * Tenant filter
   * @param tenantId
   */
  set tenantId(tenantId : string) {
    this.params.tenantId = tenantId;
    this.emitChange();
  }

  get tenantId() {
    return this.params.tenantId;
  }

  /**
   * Project filter
   * @param projectId
   */
  set projectId(projectId : string) {
    this.params.projectId = projectId;
    this.emitChange();
  }

  get projectId() {
    return this.params.projectId;
  }

  /**
   * Invoice id filter
   * @param invoiceId
   */
  set invoiceId(invoiceId : string) {
    this.params.invoiceId = invoiceId;
    this.emitChange();
  }

  get invoiceId() {
    return this.params.invoiceId;
  }

  /**
   * Amount from filter
   * @param amountFrom
   */
  set amountFrom(amountFrom : number) {
    this.params.amountFrom = amountFrom;
    this.emitChange();
  }

  get amountFrom() {
    return this.params.amountFrom;
  }

  /**
   * Amount to filter
   * @param amountTo
   */
  set amountTo(amountTo : number) {
    this.params.amountTo = amountTo;
    this.emitChange();
  }

  get amountTo() {
    return this.params.amountTo;
  }

  /**
   * Statuses filter
   * @param statuses
   */
  set statuses(statuses : string[]) {
    this.params['statuses[]'] = statuses;
    this.emitChange();
  }

  get statuses() {
    return this.params['statuses[]'];
  }

  /**
   * Type filter
   * @param type
   */
  set type(type : number) {
    this.params.type = type;
    this.emitChange();
  }

  get type() {
    return this.params.type;
  }

}
