import { BaseModel } from '../../models/base.model';
import { Type } from 'class-transformer';
import { managerStatus } from './manager.type';
import { MANAGER_STATUS } from './manager.enum';

class Person {
  firstName : string;
  lastName : string;
}

export class Manager extends BaseModel {

  @Type(() => Person)
  person : Person = new Person();

  status : managerStatus = MANAGER_STATUS.ACTIVE;
  skype : string = null;
  email : string = null;
  role : number = null;

  get fullName() {
    return `${this.person.firstName} ${this.person.lastName}`;
  }

  get isActiveStatus() {
    return this.status === MANAGER_STATUS.ACTIVE;
  }

  get isInactiveStatus() {
    return this.status === MANAGER_STATUS.INACTIVE;
  }

  get showName() {
    if(!this.person.firstName) {
      return this.email;
    } else {
      return this.fullName;
    }
  }

}
