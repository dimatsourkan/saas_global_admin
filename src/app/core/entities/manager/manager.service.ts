import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { DataStore } from '../../data-store/data-store.service';
import { Manager } from './manager.model';
import { ManagerData } from '../../data-store/data-models/manager-data.model';
import { Crud } from '../../decorators/crud.decorator';

@Injectable({
  providedIn : 'root'
})
@Crud('manager/manager', Manager, DataStore.manager)
export class ManagerService extends CRUDService<Manager, ManagerData> {

}
