import { MANAGER_STATUS } from './manager.enum';

export type managerStatus = MANAGER_STATUS.ACTIVE | MANAGER_STATUS.INACTIVE;
