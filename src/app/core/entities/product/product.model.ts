import { BaseModel } from '../../models/base.model';
import { ProductPlan } from '../product-plan/product-plan.model';
import { ProductLimit } from './product-limit.model';
import { Type } from 'class-transformer';
import { PRODUCT_STATUS, PRODUCT_TYPE } from './product.enum';
import { productStatus, productType } from './product.type';

class AdditionalProduct {
  id : string = null;
}

export class Product extends BaseModel {

  @Type(() => ProductPlan)
  productPlans : ProductPlan[] = [];
  @Type(() => ProductLimit)
  productLimits : ProductLimit[] = [];
  @Type(() => AdditionalProduct)
  additionalProducts : AdditionalProduct[] = [];

  name : string = null;
  description : string = null;
  shortDescription : string = null;
  new : boolean = false;
  comingSoon : boolean = false;
  landingLink : boolean = null;
  status : productStatus = PRODUCT_STATUS.INACTIVE;
  type : productType = PRODUCT_TYPE.SINGLE;

  get isActive() {
    return this.status === PRODUCT_STATUS.ACTIVE;
  }

  get isInactive() {
    return this.status === PRODUCT_STATUS.INACTIVE;
  }

  get isSingle() {
    return this.type === PRODUCT_TYPE.SINGLE;
  }

  get isModule() {
    return this.type === PRODUCT_TYPE.MODULE;
  }

}
