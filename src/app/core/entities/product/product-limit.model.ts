import { BaseModel } from '../../models/base.model';
import { Type } from 'class-transformer';

export enum LIMIT_TYPES {
  MULTIPLE = 1,
  UNLIMITED = 2
}

export class ProductLimit extends BaseModel {

  @Type(() => Number)
  extras : number[] = [];

  name : string = null;
  type : number = null;

  get isMultiple() {
    return this.type === LIMIT_TYPES.MULTIPLE;
  }

  get isUnlimited() {
    return this.type === LIMIT_TYPES.UNLIMITED;
  }

  get extrasString() {
    return this.extras.join(', ');
  }
}
