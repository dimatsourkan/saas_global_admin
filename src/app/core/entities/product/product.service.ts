import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { Product } from './product.model';
import { DataStore } from '../../data-store/data-store.service';
import { ProductData } from '../../data-store/data-models/product-data.model';
import { Crud } from '../../decorators/crud.decorator';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, shareReplay, tap } from 'rxjs/operators';
import { DataStoreHelper } from '../../data-store/data-store.helper';
import { ResultList } from '../../models/result.model';
import { ProductLimit } from './product-limit.model';
import { plainToClass, plainToClassFromExist } from 'class-transformer';

@Injectable({
  providedIn: 'root'
})
@Crud('product', Product, DataStore.product)
export class ProductService extends CRUDService<Product, ProductData> {

  private queryCache$ : Observable<ResultList<ProductLimit>>;

  private _limits(params ? : HttpParams) : Observable<ResultList<ProductLimit>> {
    return this.httpClient
      .get(`${this.apiUrl}/product/limits`, {headers : this.headers, params}).pipe(
        map((res : ProductLimit[]) => {
          return plainToClassFromExist(new ResultList<ProductLimit>(ProductLimit), { items : res as Object });
        }),
        tap((res : any) => DataStoreHelper.setDataCustom(this.dataStore, 'limits', res))
      );
  }

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

  limits(params ? : HttpParams) {
    if(!this.queryCache$ || !DataStore.product.limits.getValue().items.length) {
      this.queryCache$ = this._limits(params).pipe(shareReplay(1));
    }
    return this.queryCache$;
  }

}

