export enum PRODUCT_STATUS {
  ACTIVE = 1,
  INACTIVE = 2
}

export enum PRODUCT_TYPE {
  SINGLE = 1,
  MODULE = 2
}
