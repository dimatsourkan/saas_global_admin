import { PRODUCT_STATUS, PRODUCT_TYPE } from './product.enum';

export type productStatus = PRODUCT_STATUS.ACTIVE | PRODUCT_STATUS.INACTIVE;
export type productType = PRODUCT_TYPE.SINGLE | PRODUCT_TYPE.MODULE;
