import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BaseItemResolver } from '../../services/resolve.item.service';
import { Product } from './product.model';
import { ProductService } from './product.service';

@Injectable({
  providedIn : 'root'
})
export class ProductResolver extends BaseItemResolver<Product> {

  constructor(
    protected productService : ProductService,
    protected router : Router
  ) {
    super(productService, router)
  }

  protected onError(error : any) {
    // this.router.navigate(['/']);
    this.hideLoader();
  }

}
