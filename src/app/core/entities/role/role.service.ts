import { Injectable } from '@angular/core';
import { CRUDService } from '../../services/crud.service';
import { Role } from './role.model';
import { DataStore } from '../../data-store/data-store.service';
import { Crud } from '../../decorators/crud.decorator';
import { RoleData } from '../../data-store/data-models/role-data.model';

@Injectable({
  providedIn: 'root'
})
@Crud('role', Role, DataStore.role)
export class RoleService extends CRUDService<Role, RoleData> {

  get apiUrl() {
    return this.apiUrlService.apiUrlWithRole();
  }

}

