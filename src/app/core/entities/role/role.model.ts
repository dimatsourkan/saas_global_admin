import { BaseModel } from '../../models/base.model';
import { Permission } from '../permission/permission.model';
import { Type } from 'class-transformer';

export class Role extends BaseModel {

  @Type(() => Permission)
  permissions : Permission[];

  name : string = null;
  weight : number = 1;

}
