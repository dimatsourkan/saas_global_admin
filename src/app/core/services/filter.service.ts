import { BaseFilterParams, BaseFilterService } from './base-filter.service';
import { Moment } from 'moment';
import { moment } from '../helpers/moment';

export class Params extends BaseFilterParams {
  limit : number = 10;
  page : number = 1;
  search : string = '';
  status : number = null;
  dateFrom : string = null;
  dateTo : string = null;
}

export class FilterService extends BaseFilterService {

  protected params = new Params();

  public dateFormat : string = 'YYYY-MM-DD';

  /**
   * Page filter
   * @param page
   */
  set page(page : number) {
    this.params.page = page;
    this.emitChange();
  }

  get page() {
    return this.params.page;
  }

  /**
   * Limit filter
   * @param limit
   */
  set limit(limit : number) {
    if(limit > 100) {
      limit = 100;
    }
    this.params.limit = limit;
    this.params.page = 1;
    // this.emitChange();
  }

  get limit() {
    return this.params.limit;
  }

  /**
   * Search filter
   * @param search
   */
  set search(search : string) {
    this.params.search = search;
    this.params.page = 1;
    this.emitChange();
  }

  get search() {
    return this.params.search;
  }

  set status(status : number) {
    this.params.status = status;
    this.params.page = 1;
    this.emitChange();
  }

  get status() {
    return this.params.status;
  }

  /**
   * From date filter
   * @param dateFrom
   */
  set dateFrom(dateFrom : Moment) {
    if(typeof dateFrom === 'string') {
      dateFrom = new moment(dateFrom);
    }
    this.params.dateFrom = dateFrom ? dateFrom.format(this.dateFormat) : '';
    this.params.page = 1;
    this.emitChange();
  }

  get dateFrom() {
    if(this.params.dateFrom) {
      return new moment(this.params.dateFrom, this.dateFormat);
    } else {
      return null;
    }
  }

  /**
   * To date filter
   * @param dateTo
   */
  set dateTo(dateTo : Moment) {
    if(typeof dateTo === 'string') {
      dateTo = new moment(dateTo);
    }
    this.params.dateTo = dateTo ? dateTo.format(this.dateFormat) : '';
    this.params.page = 1;
    this.emitChange();
  }

  get dateTo() {
    if(this.params.dateTo) {
      return new moment(this.params.dateTo, this.dateFormat);
    } else {
      return null;
    }
  }

}
