import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { CRUDService } from './crud.service';
import { BaseModel } from '../models/base.model';
import { Observable } from 'rxjs';
import { HOME_ROUTE } from '../../app.constants';

export abstract class BaseItemResolver<T extends BaseModel> implements Resolve<T> {

  constructor(protected service : CRUDService<T, any>,
              protected router : Router) {
  }

  resolve(route : ActivatedRouteSnapshot, state : RouterStateSnapshot) : Observable<any> {

    let id : any = this.getParam(route);

    this.onStart();

    return this.query(id).pipe(
      map<any, any>(res => {
        return this.confirmData(res);
      }),
      catchError<any, any>((err : any, caught : Observable<any>) : never => {
        this.onError(err);
        throw caught;
      })
    );
  }

  protected getParam(route : ActivatedRouteSnapshot) {
    return route.paramMap.get('id');
  }

  protected query(id : string) {
    return this.service.get(id);
  }

  protected confirmData(res : T) {
    if(res) {
      this.onSuccess();
      return res;
    }

    this.onError(res);
    return null;
  }

  protected onStart() {
    this.showLoader();
  }

  protected onSuccess() {
    this.hideLoader();
  }

  protected onError(error : any) {
    this.router.navigate([HOME_ROUTE]);
    this.hideLoader();
  }

  protected showLoader() {
    document.getElementById('resolver-loader').style.display = 'block';
  }

  protected hideLoader() {
    document.getElementById('resolver-loader').style.display = 'none';
  }
}
