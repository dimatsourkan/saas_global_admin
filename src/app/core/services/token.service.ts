import { Injectable } from '@angular/core';


class TokenData {
  roles : string[];
  role : string;
  username : string;
  iat : number;
  exp : number;

  constructor(data : any = {}) {
    if(!data) {
      data = {}
    }

    this.roles = data.roles || null;
    this.role = data.roles ? data.roles[0] : null;
    this.iat = data.iat || null;
    this.exp = data.exp || null;
    this.username = data.username || null;
  }
}

const TOKEN_KEY = 'token';

@Injectable({
  providedIn : 'root'
})
export class TokenService {

  private _token : string;
  private _tokenData : TokenData;

  constructor() {
    this.initToken();
  }

  private getToken() {
    return localStorage.getItem(TOKEN_KEY);
  }

  private getTokenData(token : string) : TokenData {
    try {
      let json = atob(token.split('.')[1]);
      return new TokenData(JSON.parse(json));
    } catch (e) {
      return new TokenData();
    }
  }

  private initToken() {
    this._token = this.getToken();
    this._tokenData = this.getTokenData(this._token);
  }

  private removeToken() {
    this._token = null;
    this._tokenData = new TokenData();
  }

  private get tokenExpirationDate() : Date {

    const tokenData: TokenData = this.tokenData;

    if (!tokenData || !tokenData.exp) {
      return null;
    }

    const date = new Date(0);
    date.setUTCSeconds(tokenData.exp);

    return date;
  }

  /**
   * Удаление токена
   */
  public resetToken() {
    localStorage.removeItem(TOKEN_KEY);
    this.removeToken();
  }

  /**
   * Установка токена
   * @param token - Строка с токеном
   */
  public setToken(token : string) {
    localStorage.setItem(TOKEN_KEY, token);
    this.initToken();
  }

  public get tokenHeader() {
    return `Bearer ${this.token}`;
  }

  public get token() {
    return this._token;
  }

  public get tokenData() : TokenData {
    return this._tokenData;
  }

  public get isTokenExpired() : boolean {
    const date = this.tokenExpirationDate;

    if (date == null) {
      return false;
    }

    return date.valueOf() < (new Date().valueOf() + 1000);
  }

}
