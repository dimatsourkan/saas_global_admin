import { Injectable } from '@angular/core';
import { ENVIRONMENT } from '../../../environments/environment';
import { TokenService } from './token.service';
import { ROLES } from '../entities/user/user.enum';

@Injectable({
  providedIn : 'root'
})
export class ApiUrlService {

  private BASE_URL = ENVIRONMENT.BASE_URL;

  constructor(private token : TokenService) {

  }

  linkPathFromRole(role ? : string) {
    switch(role || this.token.tokenData.role) {
      case ROLES.TENANT : return 'tenant';
      case ROLES.MANAGER : return 'manager';
      default : return 'manager';
    }
  }

  apiUrl(version : number = ENVIRONMENT.API_VERSION) {
    return `${this.BASE_URL}/v${version}`;
  }

  apiUrlWithRole(role ? : string) {
    return `${this.apiUrl()}/${this.linkPathFromRole(role)}`;
  }

}
