import { Injectable } from '@angular/core';
import { BaseModel, modelId } from '../models/base.model';
import { ResultList } from '../models/result.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map, tap } from 'rxjs/internal/operators';
import { ApiUrlService } from './api-url.service';
import { Observable } from 'rxjs';
import { DataModel } from '../data-store/data-models/base-data.model';
import { DataStoreHelper } from '../data-store/data-store.helper';
import { Crud } from '../decorators/crud.decorator';
import { plainToClassFromExist } from 'class-transformer';

/**
 * Абстрактный CRUD Сервис для работы с сервером
 * Его должны унаследовать все crud сервисы
 */
@Injectable()
@Crud('', BaseModel, <any>DataModel)
export abstract class CRUDService<T extends BaseModel, B extends DataModel<T>> {

  /**
   * Модель с которой работает сервис (УСТАРАВЛИВАЕТСЯ В ДЕКОРАТОРЕ CRUD)
   */
  private Entity : any;

  /**
   * Путь для круд операций (УСТАРАВЛИВАЕТСЯ В ДЕКОРАТОРЕ CRUD)
   */
  protected path : string;

  /**
   * Модель для хранения данных (УСТАРАВЛИВАЕТСЯ В ДЕКОРАТОРЕ CRUD)
   */
  protected dataStore : B;

  /**
   * Хедеры которые будут оправляться со всеми запросами
   */
  protected headers : HttpHeaders = new HttpHeaders();

  /**
   * Базовая ссылка на апи, возможно с учетом роли
   */
  protected get apiUrl() {
    return this.apiUrlService.apiUrl();
  }

  /**
   * Базовый урл по которому отправляются запросы
   */
  protected get baseUrl() {
    if(this.path) {
      return `${this.apiUrl}/${this.path}`;
    } else {
      return this.apiUrl;
    }
  }

  /**
   * Метод для установки apiUrl
   * @param path - Строка (вида 'custom-request' или пустая) по которому должен работать запрос
   */
  protected getUrl(path : modelId|string|number = null) : string {
    if(path) {
      return `${this.baseUrl}/${path}`;
    } else {
      return this.baseUrl;
    }
  }

  /**
   * @ignore
   */
  constructor(
    protected httpClient : HttpClient,
    protected apiUrlService : ApiUrlService
  ) {
  }

  /**
   * Функция которая оборачивает данные с сервера в объект с которым работает сервис
   * @param data - данные для обертки
   */
  protected createEntity(data : T) : T {
    return this.Entity.plainToClass(data);
  }

  /**
   * Оборачивает данные с помощью функции createEntity для возврящяемых данных ResultList
   * @param res - Ответ сервера
   * @returns {ResultList<T>}
   */
  protected wrapObjects(res : any) : ResultList<T> {
    return plainToClassFromExist(new ResultList<T>(this.Entity), res as Object);
  }

  /**
   * Оборачивает данные с помощью функции createEntity для возврящяемых данных Result
   * @param res - Ответ сервера
   * @returns {Result<T>}
   */
  protected wrapObject(res : T) : T {
    return this.createEntity(res);
  }

  /**
   * GET запрос для получения списка моделей
   * @param params - Объект с гет параметрами
   * @returns {Observable<Object>}
   */
  getAll(params ? : HttpParams) : Observable<ResultList<T>> {
    return this.httpClient
      .get(this.getUrl(), {headers : this.headers, params}).pipe(
        map((res : ResultList<T>) : ResultList<T> => this.wrapObjects(res)),
        tap((res : ResultList<T>) => DataStoreHelper.setDataList(this.dataStore, res))
      );
  }

  /**
   * Получение модели по id
   * @param id - ID модели
   * @param params - Объект с гет параметрами
   * @returns {Observable<Object>}
   */
  get(id : modelId, params ? : HttpParams) : Observable<T> {
    return this.httpClient
      .get(this.getUrl(id), {headers : this.headers, params}).pipe(
        map((res : any) : T => this.wrapObject(res)),
        tap((res : T) => DataStoreHelper.setDataItem(this.dataStore, res))
      );
  }

  /**
   * Сохранение новой модели
   * @param data - Данные модели
   * @returns {Observable<Object>}
   */
  save(data : T) : Observable<T> {
    return this.httpClient
      .put(this.getUrl(), data.toJson(), {headers : this.headers}).pipe(
        map((res : any) : T => this.wrapObject(res)),
        // tap((res : T) => DataStoreHelper.pushItemToList(this.dataStore, data))
      );
  }

  /**
   * Изменение существующей модели
   * @param data - Данные модели
   * @returns {Observable<Object>}
   */
  update(data : T) : Observable<T> {
    return this.httpClient
      .post(this.getUrl(data.id), data.toJson(), {headers : this.headers}).pipe(
        map((res : any) : T => this.wrapObject(res)),
        tap((res : T) => DataStoreHelper.updateItemInList(data, this.dataStore))
      );
  }

  /**
   * Удаление существующей модели
   * @param id - Id модели
   * @returns {Observable<Object>}
   */
  delete(id : modelId) : Observable<T> {
    return this.httpClient
      .delete(this.getUrl(id), {headers : this.headers}).pipe(
        map((res : any) : T => this.wrapObject(res)),
        tap((res : T) => DataStoreHelper.removeItemFromList(this.dataStore, id))
      );
  }
}
