import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiUrlService } from './api-url.service';
import { ROLES } from '../entities/user/user.enum';
import { map, tap } from 'rxjs/operators';
import { TokenService } from './token.service';
import { DataStore } from '../data-store/data-store.service';
import { Observable } from 'rxjs';
import includes  from 'lodash-es/includes';
import { LOGIN_ROUTE, MANAGER_LOGIN_ROUTE } from '../../app.constants';
import { DataStoreHelper } from '../data-store/data-store.helper';

export class AuthDataModel {

  _username : string;
  _password : string;
  _recaptcha_response : string;

  constructor(data : any = {}) {
    if(!data) {
      data = {}
    }

    this._username = data._username || '';
    this._password = data._password || '';
    this._recaptcha_response = data._recaptcha_response || '';
  }
}

@Injectable({
  providedIn : 'root'
})
export class AuthService {

  constructor(private router : Router,
              private httpClient : HttpClient,
              private tokenService : TokenService,
              private apiUrlService : ApiUrlService) {

  }

  get isAuthenticated() : boolean {

    if(!this.tokenService.token) {
      return false;
    }

    if(this.tokenService.isTokenExpired) {
      this.logout();
      return false;
    }

    return true;
  }

  get userRole() {
    return this.tokenService.tokenData.role;
  }

  get isManager() {
    return this.userRole === ROLES.MANAGER;
  }

  get isTenant() {
    return this.userRole === ROLES.TENANT;
  }

  tenantLogin(data : AuthDataModel) : Observable<any> {
    return this.httpClient.post(`${this.apiUrlService.apiUrl()}/tenant/login_check`, data)
      .pipe(map((res : { token : string }) => this.onLogin(res)));

  }

  managerLogin(data : AuthDataModel) : Observable<any> {
    return this.httpClient.post(`${this.apiUrlService.apiUrl()}/manager/login_check`, data)
      .pipe(map((res : { token : string }) => this.onLogin(res)));

  }

  signUp(data : AuthDataModel) : Observable<Object> {
    return this.httpClient.post(`${this.apiUrlService.apiUrl()}/register/tenant`, data)
      .pipe(map((res : { token : string }) => this.onLogin(res)));
  }

  resetPassManager(email : string) : Observable<any> {
    return this.httpClient.post(`${this.apiUrlService.apiUrl()}/request_password_reset/manager`, {
      email
    });
  }

  resetPassConfirmManager(password : string, confirmPassword : string, token : string) : Observable<any> {
    return this.httpClient.post(`${this.apiUrlService.apiUrl()}/reset_password/manager`, {
      password, confirmPassword, token
    });
  }


  resetPassTenant(email : string) : Observable<any> {
    return this.httpClient.post(`${this.apiUrlService.apiUrl()}/request_password_reset/tenant`, {email});
  }

  resetPassConfirmTenant(password : string, confirmPassword : string, token : string) : Observable<any> {
    return this.httpClient.post(`${this.apiUrlService.apiUrl()}/reset_password/tenant`, {
      password, confirmPassword, token
    });
  }

  confirmEmailTenant(token : string) : Observable<any> {
    return this.httpClient.post(`${this.apiUrlService.apiUrl()}/confirm_email/tenant/${token}`, {})
      .pipe(tap(() => {
        let user = DataStore.user.current.getValue();
            user.level.isEmailConfirmed = true;
        DataStoreHelper.setDataCustom(DataStore.user, 'current', user);
      }));
  }

  fromRoles(roles : string[]) {
    return includes(roles, '*') || includes(roles, this.tokenService.tokenData.role);
  }

  hasPermissions(permissions : any[]) {
    let list = DataStore.permission.permissionArray.getValue();
    return !!permissions.find((perm) => {
      if(list.indexOf(perm) >= 0) {
        return true;
      }
    });
  }

  logout() {
    let role = this.userRole;
    DataStore.clearData();
    this.tokenService.resetToken();
    if(role === ROLES.MANAGER) {
      this.router.navigate([MANAGER_LOGIN_ROUTE]);
    } else {
      this.router.navigate([LOGIN_ROUTE]);
    }
  }

  private onLogin(res : { token : string }) {
    this.tokenService.setToken(res.token);
    return res;
  }

}
