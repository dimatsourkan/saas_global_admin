import { Injectable, Injector } from '@angular/core';
import { UserService } from '../entities/user/user.service';
import { AuthService } from './auth.service';
import { PermissionService } from '../entities/permission/permission.service';
import { forkJoin, Subject } from 'rxjs';

@Injectable({
  providedIn : 'root'
})
export class AppInitializer {

  constructor(
    private injector : Injector,
    private userService : UserService,
    private permissionService : PermissionService
  ) {
  }

  private getPermissions(canPermissions : boolean) {
    if(canPermissions) {
      return this.permissionService.getAll();
    } else {
      const subj = new Subject();
      setTimeout(() => {
        subj.next();
        subj.complete();
      }, 10);
      return subj;
    }
  }

  private getProfile() {
    return this.userService.profile();
  }

  initialize() : Promise<any> {

    return new Promise<boolean>((resolve) => {

      let authService = this.injector.get(AuthService);

      if(authService.isAuthenticated) {
        forkJoin(
          this.getPermissions(authService.isManager),
          this.getProfile()
        ).subscribe(
          () => {
            return resolve(true);
          },
          (err) => {
            authService.logout();
            return resolve(true);
          });

      } else {
        return resolve(true);
      }

    });
  }
}

export function init_app(appLoadService : AppInitializer) {
  return () => appLoadService.initialize();
}
