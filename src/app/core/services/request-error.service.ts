import { Injectable } from '@angular/core';
import { NotificationsService } from '../components/notifications/notifications.service';
import { NOTIFICATION_TYPE } from '../components/notifications/notifications.const';
import { Notification } from '../components/notifications/notifications.model';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';
import { parseValidationKeys } from '../helpers/helpers';

@Injectable({
  providedIn: 'root'
})
export class RequestErrorService {
  constructor(
    private router: Router,
    private authService: AuthService,
    private notificationsService: NotificationsService
  ) {
  }

  showError( error: HttpErrorResponse ) {
    switch (error.status) {
      case 401 :
        this.authService.logout();
        this.pushNotification({
          title: 401,
          statusText: `Authorization error "${error.statusText}"`
        });
        break;
      case 404 :
        this.pushNotification({
          title: 404,
          statusText: 'Request address not found'
        });
        break;
      case 400 :
        this.pushNotification({
          title: 400,
          statusText: this.parseError(error.error)
        });
        break;
      case 409 :
        this.pushNotification({
          title: 409,
          statusText: error.statusText
        });
        break;
      case 403 :
        this.pushNotification({
          title: 403,
          statusText: error.error.errors
        });
        break;
      case 405 :
        this.pushNotification({
          title: 405,
          statusText: error.error.message + ' ' + error.error.path
        });
        break;
      case 422 :
        this.pushNotification({
          title: 422,
          statusText: this.parseError(error.error)
        });
        break;
      case 500 :
        this.pushNotification({
          title: error.status,
          statusText: error.message
        });
        break;
      case 502  :
        this.pushNotification({
          title: 502,
          statusText: 'The request timeout has expired, the server is unavailable'
        });
        break;
      case -1  :
        this.pushNotification({
          title: 408,
          statusText: 'The request timeout has expired, the server is unavailable'
        });
        break;
    }
  }

  private parseError( error: any ) {
    if (error.errors) {
      if (typeof error.errors === 'string') {
        return error.errors;
      } else if (typeof error.errors === 'object') {
        return this.parseErrorObject(error.errors);
      }
    }
    return 'Bad request';
  }

  private parseErrorObject( errors: Object ) {
    let errorsString = '';
    for (const key of Object.keys(errors)) {
      let keyArray = parseValidationKeys(key);
      let currentKey = keyArray[ keyArray.length - 1 ];
      currentKey = currentKey.charAt(0).toUpperCase() + currentKey.substr(1);
      errorsString += `${currentKey}: ${errors[ key ]} \n`
    }
    // ---------------------------------------------------------------
    return errorsString;
  }

  private pushNotification( error: any ) {
    let message: string = error.statusText ? error.statusText : 'Server error';
    this.notificationsService.push(
      Notification.plainToClass({
        message: message,
        title: 'Error',
        time: 3000,
        type: NOTIFICATION_TYPE.ERROR
      })
    );
  }
}
