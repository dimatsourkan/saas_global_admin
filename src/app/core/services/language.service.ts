import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

export enum LANG {
  EN = 'en',
  RU = 'ru'
}

type langType = LANG.EN | LANG.RU;

/**
 * Сервис для работы с языками
 */
@Injectable({
  providedIn : 'root'
})
export class LanguageService {

  private lang : langType = LANG.EN;

  get currentLang() {
    return this.lang;
  }

  constructor(private translateService : TranslateService) {

  }

  /**
   * Задает приложению переданный язык
   * @param lang
   */
  use(lang : langType) {
    this.lang = lang;
    this.changeLang();
    this.setCurrentLang();
  }

  /**
   * Получает из localStorage текущий язык и использует его
   */
  useCurrent() {
    this.use(this.getCurrentLang());
  }

  /**
   * Сохраняет в localStorage текущий язык
   */
  setCurrentLang() {
    localStorage.setItem('lang', this.lang);
  }

  /**
   * Получает язык из localStorage если его нет то отдает язык по умолчанию
   */
  private getCurrentLang() {
    let lang = localStorage.getItem('lang');
    if(!lang) {
      return this.lang;
    } else {
      lang = lang.toUpperCase();
      if(LANG[lang]) {
        return LANG[lang];
      } else {
        return this.lang;
      }
    }
  }

  /**
   * Изменяет язык для приложения
   */
  private changeLang() {
    this.translateService.setDefaultLang(this.lang);
    this.translateService.use(this.lang);
  }

}
