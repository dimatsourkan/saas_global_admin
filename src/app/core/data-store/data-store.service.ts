import { UserData } from './data-models/user-data.model';
import { ProjectData } from './data-models/project-data.model';
import { ProductData } from './data-models/product-data.model';
import { BlockedMailData } from './data-models/blocked-mail-data.model';
import { TenantData } from './data-models/tenant-data.model';
import { CountryData } from './data-models/country-data.model';
import { ManagerData } from './data-models/manager-data.model';
import { ProductPlanData } from './data-models/product-plan-data.model';
import { MailboxData } from './data-models/mailbox-data.model';
import { RoleData } from './data-models/role-data.model';
import { PermissionData } from './data-models/permission-data.model';
import { ProjectRequestData } from './data-models/project-request-data.model';
import { MailingData } from './data-models/mailing-data.model';
import { InvoiceData } from './data-models/invoice-data.model';
import { BaseModel } from '../models/base.model';

/**
 * Сервис для хранения данных
 */
export class DataStore<T extends BaseModel> {

  static user : UserData = new UserData();
  static role : RoleData = new RoleData();
  static tenant : TenantData = new TenantData();
  static mailbox : MailboxData = new MailboxData();
  static invoice : InvoiceData = new InvoiceData();
  static mailing : MailingData = new MailingData();
  static manager : ManagerData = new ManagerData();
  static country : CountryData = new CountryData();
  static project : ProjectData = new ProjectData();
  static product : ProductData = new ProductData();
  static permission : PermissionData = new PermissionData();
  static blockedMail : BlockedMailData = new BlockedMailData();
  static productPlan : ProductPlanData = new ProductPlanData();
  static projectRequest : ProjectRequestData = new ProjectRequestData();

  static clearData() {
    DataStore.user.clearData();
    DataStore.role.clearData();
    DataStore.tenant.clearData();
    DataStore.mailbox.clearData();
    DataStore.invoice.clearData();
    DataStore.mailing.clearData();
    DataStore.manager.clearData();
    DataStore.country.clearData();
    DataStore.project.clearData();
    DataStore.product.clearData();
    DataStore.permission.clearData();
    DataStore.blockedMail.clearData();
    DataStore.productPlan.clearData();
    DataStore.projectRequest.clearData();
  }

  static isMine(id : string) {

    let user = DataStore.user.current.getValue();

    if(!user) {
      return false;
    }

    if(!user.id || !id) {
      return false;
    }

    return id === user.id;

  }
}
