import { DataModel, StoreItem } from './base-data.model';
import { Permission } from '../../entities/permission/permission.model';

/**
 * Модель хранения данных permissions
 */
export class PermissionData extends DataModel<Permission> {

  permissionArray : StoreItem<string[]> = new StoreItem([]);

  constructor() {
    super();
    this.list.asObservable$.subscribe((res) => {
      if(res) {
        this.permissionArray.setValue(res.items.map(p => p.id));
      }
    });
  }
}
