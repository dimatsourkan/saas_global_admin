import { DataModel } from './base-data.model';
import { ProductPlan } from '../../entities/product-plan/product-plan.model';

/**
 * Модель хранения данных product-plan
 */
export class ProductPlanData extends DataModel<ProductPlan> {}
