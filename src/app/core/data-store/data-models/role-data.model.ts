import { DataModel, StoreItem } from './base-data.model';
import { Role } from '../../entities/role/role.model';
import { ResultList } from '../../models/result.model';
import { Permission } from '../../entities/permission/permission.model';

/**
 * Модель хранения данных roles
 */
export class RoleData extends DataModel<Role> {

  permissions : StoreItem<ResultList<Permission>> = new StoreItem(new ResultList<Permission>());

  clearData() {
    super.clearData();
    this.permissions.setValue(new ResultList<Permission>())
  }
}
