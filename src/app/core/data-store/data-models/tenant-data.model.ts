import { DataModel } from './base-data.model';
import { Tenant } from '../../entities/tenant/tenant.model';

/**
 * Модель хранения данных tenants
 */
export class TenantData extends DataModel<Tenant> {

}
