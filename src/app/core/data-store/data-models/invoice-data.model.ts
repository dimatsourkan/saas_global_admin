import { DataModel } from './base-data.model';
import { Invoice } from '../../entities/invoice/invoice.model';

/**
 * Модель хранения данных invoice
 */
export class InvoiceData extends DataModel<Invoice> {

}
