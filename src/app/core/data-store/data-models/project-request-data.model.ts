import { DataModel, StoreItem } from './base-data.model';
import { ProjectRequest } from '../../entities/project-request/project-request.model';

/**
 * Модель хранения данных projects
 */
export class ProjectRequestData extends DataModel<ProjectRequest> {

  count = new StoreItem<number>(0);

  clearData() {
    this.count.setValue(0);
    super.clearData();
  }

}
