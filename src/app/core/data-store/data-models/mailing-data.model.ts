import { DataModel, StoreItem } from './base-data.model';
import { MailingGroup } from '../../entities/mailing/mailing.model';

/**
 * Модель хранения данных mailing
 */
export class MailingData extends DataModel<MailingGroup> {

  subscriptions : StoreItem<string[]> = new StoreItem();

  clearData() {
    super.clearData();
    this.subscriptions.setValue([]);
  }
}
