import { DataModel } from './base-data.model';
import { Manager } from '../../entities/manager/manager.model';

/**
 * Модель хранения данных managers
 */
export class ManagerData extends DataModel<Manager> {

}
