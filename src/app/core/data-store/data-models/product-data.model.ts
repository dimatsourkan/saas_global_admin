import { DataModel, StoreItem } from './base-data.model';
import { Product } from '../../entities/product/product.model';
import { ResultList } from '../../models/result.model';
import { ProductLimit } from '../../entities/product/product-limit.model';

/**
 * Модель хранения данных products
 */
export class ProductData extends DataModel<Product> {

  public limits : StoreItem<ResultList<ProductLimit>> = new StoreItem(new ResultList<ProductLimit>());

  clearData() {
    super.clearData();
    this.limits.setValue(new ResultList<ProductLimit>());
  }
}
