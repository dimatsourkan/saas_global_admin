import { DataModel } from './base-data.model';
import { BlockedMail } from '../../entities/blocked-mail/blocked-mail.model';

/**
 * Модель хранения данных blocked-mails
 */
export class BlockedMailData extends DataModel<BlockedMail> {}
