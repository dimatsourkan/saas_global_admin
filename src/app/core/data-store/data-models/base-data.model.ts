import { Observable, ReplaySubject } from 'rxjs';
import { BaseModel } from '../../models/base.model';
import { EventEmitter } from '@angular/core';
import { ResultList } from '../../models/result.model';
import { map, publishReplay, refCount } from 'rxjs/operators';
import { classToClass } from 'class-transformer';

export class StoreItem<T> {

  private value : T = null;
  private valueActions$ : ReplaySubject<T> = new ReplaySubject<T>();

  public asObservable$ : Observable<T>;

  constructor(initialValue? : T) {
    this.value = initialValue;
    this.valueActions$.next(initialValue);
    this.asObservable$ = this.valueActions$.pipe(
      map(value => classToClass(value)),
      publishReplay(1),
      refCount()
    );
  }

  setValue(value : T) {
    this.value = value;
    this.valueActions$.next(value);
  }

  getValue() : T {
    return classToClass(this.value);
  }
}

/**
 * Базовая модель хранения данных
 */
export abstract class DataModel<T extends BaseModel> {

  /**
   * Эти события можно генерить снаружи в требуемых случаях
   */
  public onCreate : EventEmitter<T> = new EventEmitter<T>(null);
  public onUpdate : EventEmitter<T> = new EventEmitter<T>(null);

  public list : StoreItem<ResultList<T>> = new StoreItem(new ResultList<T>());
  public item : StoreItem<T> = new StoreItem();

  clearData() {
    this.list.setValue(new ResultList<T>());
    this.item.setValue(null);
  }
}
