import { DataModel, StoreItem } from './base-data.model';
import { Mailbox } from '../../entities/mailbox/mailbox.model';

/**
 * Модель хранения данных mailing
 */
export class MailboxData extends DataModel<Mailbox> {

  count = new StoreItem<number>(0);

  clearData() {
    this.count.setValue(0);
    super.clearData();
  }
}
