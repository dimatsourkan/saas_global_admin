import { DataModel } from './base-data.model';
import { Project } from '../../entities/project/project.model';

/**
 * Модель хранения данных projects
 */
export class ProjectData extends DataModel<Project> {}
