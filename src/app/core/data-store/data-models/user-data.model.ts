import { DataModel, StoreItem } from './base-data.model';
import { User } from '../../entities/user/user.model';

/**
 * Модель хранения данных users
 */
export class UserData extends DataModel<User> {

  public current : StoreItem<User> = new StoreItem<User>(new User());

  clearData() {
    this.current.setValue(new User());
    super.clearData();
  }
}
