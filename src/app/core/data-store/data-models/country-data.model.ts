import { DataModel } from './base-data.model';
import { Country } from '../../entities/country/country.model';

/**
 * Модель хранения данных country
 */
export class CountryData extends DataModel<Country> {}
