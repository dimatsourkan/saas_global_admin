import findIndex from 'lodash-es/findIndex';
import { BaseModel } from '../models/base.model';
import { DataModel, StoreItem } from './data-models/base-data.model';
import { ResultList } from '../models/result.model';

/**
 * Класс хелпер для работы с data-store
 */
export class DataStoreHelper {

  /**
   * Устанавливает список в dataStore
   * @param dataStore
   * @param res
   */
  static setDataList<T extends BaseModel, P extends DataModel<T>>(dataStore : P, res : ResultList<T>) {
    dataStore.list.setValue(res);
  }

  /**
   * Устанавливает элемент в dataStore
   * @param dataStore
   * @param res
   */
  static setDataItem<T extends BaseModel, P extends DataModel<T>>(dataStore : P, res : T) {
    dataStore.item.setValue(res);
  }

  /**
   * Устанавливает кастомный элемент в dataStore
   * @param dataStore
   * @param name
   * @param data
   */
  static setDataCustom<
    T extends BaseModel,
    P extends DataModel<T>,
    B extends keyof P
    >(dataStore : P, name : B, data : T|ResultList<T>) {

    let item : any = dataStore[name];

    if(item) {
      item.setValue(data);
    }
  }

  /**
   * Добавляет элемент в список в dataStore
   * @param dataStore
   * @param res
   */
  static pushItemToList<T extends BaseModel, P extends DataModel<T>>(dataStore : P, res : T) {
    let list = dataStore.list.getValue();
        list.items.push(res);
    dataStore.list.setValue(list);
  }

  /**
   * Изменяет элемент списка в dataStore
   * @param dataStore
   * @param item
   */
  static updateItemInList<T extends BaseModel, P extends DataModel<T>>(item : T, dataStore : P) {
    let list = dataStore.list.getValue();
    let index = this.getIndexById(list.items, item.id);

    if(index >= 0) {
      list.items[index] = item;
    }

    dataStore.list.setValue(list);
  }

  /**
   * Удаляет из списка в dataStore
   * @param dataStore
   * @param id
   */
  static removeItemFromList<T extends BaseModel, P extends DataModel<T>>(dataStore : P, id : string) {
    let list = dataStore.list.getValue();
    let index = this.getIndexById(list.items, id);

    if(index >= 0) {
      list.items.splice(index, 1);
    }

    dataStore.list.setValue(list);
  }

  /**
   * Получение индекса элемента в массиве по id
   * @param array
   * @param id
   */
  static getIndexById<T extends BaseModel>(array : T[] = [], id : string) {
    return findIndex(array, (i : T) => i.id === id);
  }

}
