import { classToClass, classToPlain, Type } from 'class-transformer';
import { myPlainToClass } from '../helpers/helpers';

export type modelId = string;

export abstract class BaseModel {

  static plainToClass = myPlainToClass;

  @Type(() => Date)
  createdAt : Date = new Date();

  @Type(() => Date)
  updatedAt : Date = new Date();

  id : modelId = null;

  toJson() {
    return classToPlain(this);
  }

  clone() {
    return classToClass(this);
  }
}
