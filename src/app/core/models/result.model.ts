import { BaseModel } from './base.model';
import { Pagination } from './pagination.model';
import { Exclude, Type } from 'class-transformer';

export class ResultList<T extends BaseModel> extends Pagination<T> {

  @Exclude()
  private type : Function;

  @Type(options => (options.newObject as ResultList<T>).type)
  items : T[] = [];

  constructor(type ? : Function) {
    super();
    this.type = type;
  }
}
