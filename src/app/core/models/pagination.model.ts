import { BaseModel } from './base.model';
import { PAGINATION_LIMIT } from '../../app.constants';
import { myPlainToClass } from '../helpers/helpers';
import { classToClass } from 'class-transformer';

export class Pagination<T extends BaseModel> {

  static plainToClass = myPlainToClass;

  public page : number = 1;
  public limit : number = PAGINATION_LIMIT;
  public totalPages : number = 1;
  public totalItems : number = 0;

  public clone() {
    return classToClass(this);
  }
}
