import { APP_INITIALIZER, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './components/notifications/notification/notification.component';
import { NotificationListComponent } from './components/notifications/notification-list/notification-list.component';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { TokenInterceptor } from './interceprors/token.interceptor';
import { RequestInterceptor } from './interceprors/request.interceptor';
import { LanguageInterceptor } from './interceprors/language.interceptor';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HeaderComponent } from './components/header/header.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { AppInitializer, init_app } from './services/app-initializer.service';
import { SharedModule } from '../shared/shared.module';

export function createTranslateLoader(http : HttpClient) {
  return new TranslateHttpLoader(http, './assets/lang/', '.json');
}

@NgModule({
  imports : [
    CommonModule,
    RouterModule,
    SharedModule,
    TranslateModule.forRoot({
      loader : {
        provide : TranslateLoader,
        useFactory : (createTranslateLoader),
        deps : [HttpClient]
      }
    })
  ],
  declarations : [
    NotificationComponent,
    NotificationListComponent,
    HeaderComponent,
    WrapperComponent,
    FooterComponent,
  ],
  exports : [
    NotificationListComponent,
    NotificationComponent,
    HeaderComponent,
    WrapperComponent,
    FooterComponent,
  ],
  providers : [
    {
      provide: APP_INITIALIZER,
      useFactory: init_app,
      deps: [AppInitializer],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LanguageInterceptor,
      multi: true
    }
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule ? : CoreModule) {
    if(parentModule) {
      throw new Error('CoreModule is already loaded. Import only in AppModule');
    }
  }
}
