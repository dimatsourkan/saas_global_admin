import { Component, OnInit } from '@angular/core';
import { DataStore } from '../../data-store/data-store.service';
import { AuthService } from '../../services/auth.service';
import { PERMISSIONS } from '../../entities/permission/permissions.list';

@Component({
  selector : 'app-header',
  templateUrl : './header.component.html',
  styleUrls : ['./header.component.scss']
})
export class HeaderComponent {

  PERMISSIONS = PERMISSIONS;
  user$ = DataStore.user.current.asObservable$;

  constructor(private authService : AuthService) {

  }

  logout() {
    this.authService.logout();
  }

  get isAuthenticated() {
    return this.authService.isAuthenticated;
  }

  get isManager() {
    return this.authService.isManager;
  }

  get isTenant() {
    return this.authService.isTenant;
  }

}
