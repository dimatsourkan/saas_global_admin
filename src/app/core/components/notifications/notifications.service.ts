import { Injectable } from '@angular/core';
import { Notification } from './notifications.model';
import { BehaviorSubject } from 'rxjs';
import { NOTIFICATION_TYPE } from './notifications.const';

@Injectable({
  providedIn : 'root'
})
export class NotificationsService {

  notificationList$ = new BehaviorSubject<Notification[]>([]);

  push(notification : Notification) {
    this.notificationList$.value.push(notification);
  }

  removeByIndex(index : number) {
    this.notificationList$.value.splice(index, 1);
  }

  pushMessage(message : string, title : string, type : string) {
    this.push(Notification.plainToClass({
      time : 5000,
      type : type,
      title : title,
      message : message
    }))
  }

  success(message : string) {
    this.pushMessage(message, 'Success', NOTIFICATION_TYPE.SUCCESS);
  }

  error(message : string) {
    this.pushMessage(message, 'Error', NOTIFICATION_TYPE.ERROR);
  }

  warning(message : string) {
    this.pushMessage(message, 'Warning', NOTIFICATION_TYPE.WARNING);
  }

  info(message : string) {
    this.pushMessage(message, 'Info', NOTIFICATION_TYPE.INFO);
  }
}
