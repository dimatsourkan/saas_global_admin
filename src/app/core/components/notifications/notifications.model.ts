import { BaseModel } from '../../models/base.model';

export class Notification extends BaseModel {
  id : string = `${(new Date()).getTime()}`;
  message : string = null;
  title : string = null;
  type : string = null;
  time   ? : number = 3000;
}
