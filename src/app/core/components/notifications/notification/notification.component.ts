import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Notification } from '../notifications.model';

@Component({
  selector : 'app-notification',
  templateUrl : './notification.component.html',
  styleUrls : ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Input() notification : Notification;
  @Output() onRemove : EventEmitter<any> = new EventEmitter<any>();

  timeout = null;

  ngOnInit() {
    this.setRemoveTimeout();
  }

  clearTimeout() {
    clearTimeout(this.timeout);
  }

  setRemoveTimeout() {
    this.timeout = setTimeout(() => this.remove(), this.notification.time);
  }

  remove() {
    this.onRemove.emit();
  }
}
