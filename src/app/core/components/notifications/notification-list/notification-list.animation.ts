import { animate, query, style, transition, trigger } from '@angular/animations';

export const notificationSlideAnimation = trigger('notificationSlideAnimation', [

  transition( '* <=> *', [

    query(':enter',
      [
        style({ opacity: 0, transform: 'translateX(20px)' }),

        animate('0.2s',
          style({
            opacity: 1,
            transform: 'translateX(0)'
          })
        )
      ],
      { optional: true }
    ),

    query(':leave',
      [
        style({ opacity: 1, transform: 'translateX(0)', 'margin-bottom' : '*' }),

        animate('0.2s',
          style({
            opacity: 0,
            transform: 'translateY(-100%)',
            // 'margin-bottom' : '0px',
            // 'margin-top' : '-91px'
          })
        ),
      ],
      { optional: true }
    )

  ])

]);
