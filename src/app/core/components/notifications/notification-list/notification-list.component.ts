import { Component } from '@angular/core';
import { NotificationsService } from '../notifications.service';
import { BehaviorSubject } from 'rxjs';
import { Notification } from '../notifications.model';
import { Helpers } from '../../../helpers/helpers';
import { notificationSlideAnimation } from './notification-list.animation';

@Component({
  selector : 'app-notification-list',
  templateUrl : './notification-list.component.html',
  styleUrls : ['./notification-list.component.scss'],
  animations : [notificationSlideAnimation]
})
export class NotificationListComponent {

  list$ : BehaviorSubject<Notification[]>;

  constructor(
    public NotificationsService : NotificationsService,
    public helpers : Helpers
  ) {
    this.list$ = NotificationsService.notificationList$;
  }

  remove(index : number) {
    this.NotificationsService.removeByIndex(index);
  }

}
