import { Component, ElementRef, ViewChild } from '@angular/core';
import { routerTransition } from '../../../shared/animation/animation.fade';

@Component({
  selector : 'app-wrapper',
  templateUrl : './wrapper.component.html',
  styleUrls : ['./wrapper.component.scss'],
  animations : [routerTransition]
})
export class WrapperComponent {

  @ViewChild('main') private main : ElementRef;

  animationInProgress = false;

  get animationData() {
    return document.location.pathname;
  }

  onStartAnimation() {
    this.animationInProgress = true;
  }

  onEndAnimation() {
    this.animationInProgress = false;
  }

}
