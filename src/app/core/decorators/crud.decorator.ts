import { DataModel } from '../data-store/data-models/base-data.model';

/**
 * Декоратор для круд сервисов
 * Устанавливает нужные параметры для его работы
 * Пример : @Crud('user', User, DataStore.user)
 * @param path   - Путь по которому сервис должен работать с сервером
 * @param Entity - Модель с которой работает сервис
 * @param dataStore   - DataModel в которую сервис будет записывать результат сервера
 * @constructor
 */
export function Crud(path : string, Entity : Function, dataStore : DataModel<any>) : Function {
  return function (target : any) {
    target.prototype.path = path;
    target.prototype.Entity = Entity;
    target.prototype.dataStore = dataStore;
    return target;
  };
}
