export const BASE_DATE_FORMAT = 'YYYY-MM-DD HH:mm:ss';
export const MANAGER_LOGIN_ROUTE = '/auth/manager/login';
export const LOGIN_ROUTE = '/auth/login';
export const HOME_ROUTE = '/home';
export const PAGINATION_LIMIT = 10;
export const REGEXPS = {
  NUMERIC : /^\d+$/,
  PHONE : /^\+?[0-9]{8,20}$/i,
  NUMERIC_FLOAT : /^[0-9]*[.]?[0-9]*?$/,
  NUMERIC_OR_LETTERS : /^[0-9a-zA-Z]+$/,
  NUMBERS_AFTER_POINT : (max : number) => {
    return new RegExp(`^[0-9]*[.]?[0-9]{0,${max}}$`);
  },
  LINK : regexp()
};

function regexp() {
  return new RegExp([
    /(?:(?:(https?|ftp):)?\/\/)/       // protocol
    , /(?:([^:\n\r]+):([^@\n\r]+)@)?/  // user:pass
    , /(?:(?:www\.)?([^\/\n\r]+))/     // domain
    , /(\/[^?\n\r]+)?/                 // request
    , /(\?[^#\n\r]*)?/                 // query
    , /(#?[^\n\r]*)?/                  // anchor
  ].map(function (r) {
    return r.source;
  }).join(''));
}
