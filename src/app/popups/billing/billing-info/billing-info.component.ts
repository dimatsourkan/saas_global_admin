import { Component } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { DataStore } from '../../../core/data-store/data-store.service';
import { Invoice } from '../../../core/entities/invoice/invoice.model';

@Component({
  selector : 'app-billing-info',
  templateUrl : './billing-info.component.html',
  styleUrls : ['./billing-info.component.scss']
})
export class BillingInfoComponent extends BasePopupComponent {
  invoice : Invoice = <Invoice>DataStore.invoice.item.getValue();
}

