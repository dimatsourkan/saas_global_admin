import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { BILLING_POPUPS_ROUTES } from './billing-popups.routing';
import { NewBillingComponent } from './billing-new-edit/billing-new.component';
import { BillingInfoComponent } from './billing-info/billing-info.component';
import { InvoicePopupResolver } from './billing-popup-resolver.service';
import { EditBillingComponent } from './billing-new-edit/billing-edit.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(BILLING_POPUPS_ROUTES)
  ],
  declarations : [
    NewBillingComponent,
    BillingInfoComponent,
    EditBillingComponent
  ],
  providers : [
    InvoicePopupResolver
  ]
})
export class BillingPopupsModule {
}
