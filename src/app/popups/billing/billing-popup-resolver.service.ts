import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BaseItemResolver } from '../../core/services/resolve.item.service';
import { PopupService } from '../popup.service';
import { InvoiceService } from '../../core/entities/invoice/invoice.service';
import { Invoice } from '../../core/entities/invoice/invoice.model';

@Injectable()
export class InvoicePopupResolver extends BaseItemResolver<Invoice> {

  constructor(
    protected popupService : PopupService,
    protected invoiceService : InvoiceService,
    protected router : Router
  ) {
    super(invoiceService, router)
  }

  protected onError(error : any) {
    this.hideLoader();
    this.popupService.closeRoutingPopup();
  }

}
