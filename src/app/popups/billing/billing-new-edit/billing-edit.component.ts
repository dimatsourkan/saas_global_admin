import { Component, OnInit } from '@angular/core';
import { plainToClassFromExist } from 'class-transformer';
import { DataStore } from '../../../core/data-store/data-store.service';
import { Invoice } from '../../../core/entities/invoice/invoice.model';
import { NewBillingComponent } from './billing-new.component';
import { patchFormValues } from '../../../core/helpers/helpers';

@Component({
  selector : 'app-new-billing',
  templateUrl : './billing-new-edit.component.html',
  styleUrls : ['./billing-new-edit.component.scss']
})
export class EditBillingComponent extends NewBillingComponent implements OnInit {

  invoice : Invoice = <Invoice>DataStore.invoice.item.getValue();

  ngOnInit() {
    this.form.get('tenant').valueChanges.subscribe((res) => {
      this.form.patchValue({ tenantSelect : res.id });
    });
    this.productBuilder.patchForm(this.invoice.items);
    patchFormValues(this.form, this.invoice);
    this.setTenant(this.invoice.tenant.id);
    this.form.get('project').enable();
    super.ngOnInit();
  }

  getSubmitMethod() {
    let request = plainToClassFromExist(this.invoice, this.form.getRawValue() as Object);
    request.items = this.productBuilderService
      .filterEmptyExtralimits(request.items)
      .filter((i) => i.product);

    return this.invoiceService.update(this.getInvoiceFullModel())
  }

  getInvoiceFullModel() : Invoice {
    let invoice = plainToClassFromExist(this.invoice, this.form.getRawValue() as Object);
    invoice.items = this.productBuilderService
      .filterEmptyExtralimits(invoice.items)
      .filter((i) => i.product);

    return invoice;
  }

  onSuccess(res : any) {
    this.modal.close();
    this.notificationsService.success('Invoice edit successfully');
    DataStore.invoice.onCreate.emit(Invoice.plainToClass(this.form.value));
  }
}
