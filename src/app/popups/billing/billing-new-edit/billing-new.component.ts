import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { PLACEHOLDER_TYPE } from '../../../shared/components/form-controls/form-controls.component';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { debounceTime, finalize, mergeMap } from 'rxjs/operators';
import { DataStore } from '../../../core/data-store/data-store.service';
import { Invoice, InvoiceTotal } from '../../../core/entities/invoice/invoice.model';
import { InvoiceService } from '../../../core/entities/invoice/invoice.service';
import { TenantService } from '../../../core/entities/tenant/tenant.service';
import { CURRENCIES } from '../../../core/enums/currencies.enum';
import { Tenant } from '../../../core/entities/tenant/tenant.model';
import { ProductBuilderComponent } from '../../../shared/components/product-builder/product-builder.component';
import { ProductBuilderService } from '../../../shared/components/product-builder/product-builder.service';
import { REGEXPS } from '../../../app.constants';
import { Subject } from 'rxjs';

@Component({
  selector : 'app-new-billing',
  templateUrl : './billing-new-edit.component.html',
  styleUrls : ['./billing-new-edit.component.scss']
})
export class NewBillingComponent extends BasePopupComponent implements OnInit, OnDestroy {

  PLACEHOLDER_TYPE = PLACEHOLDER_TYPE;

  REGEXPS = REGEXPS;
  loading : boolean = false;
  invoice : Invoice = new Invoice();
  form : FormGroup;
  formItems : FormArray;
  @ViewChild('productBuilder') protected productBuilder : ProductBuilderComponent;
  totalPrice = new InvoiceTotal();
  private totalSubscriber$ = new Subject();

  constructor(
    protected formBuilder : FormBuilder,
    protected invoiceService : InvoiceService,
    protected tenantService : TenantService,
    protected validatorService : ValidatorService,
    protected notificationsService : NotificationsService,
    protected productBuilderService : ProductBuilderService
  ) {
    super();
    this.form = this.formBuilder.group({
      tenantSelect : [null, Validators.required],
      tenant : [null, Validators.required],
      project : this.formBuilder.group({
        id : [{value : '', disabled : true}, Validators.required]
      }),
      payer : ['', Validators.required],
      discount : this.formBuilder.group({
        amount : [0, [Validators.required]],
        currency : [CURRENCIES.USD, Validators.required]
      }),
      items : this.formBuilder.array([])
    });

    this.formItems = this.form.get('items') as FormArray;

    this.form.get('tenant').valueChanges.subscribe((tenant : Tenant) => {
      this.productBuilderService.taxPercent = tenant.billingInfo.taxPercent;
    });

    this.form.valueChanges.subscribe(() => {
      this.totalSubscriber$.next();
    });

  }

  ngOnInit() {
    super.ngOnInit();
    this.totalSubscriber$
      .pipe(debounceTime(500))
      .subscribe((value : string) => {
        this.invoiceService.getTotal(this.getInvoiceFullModel()).subscribe(res => {
          this.totalPrice = res;
        });
      });
  }

  ngOnDestroy() {
    this.totalSubscriber$.unsubscribe();
  }

  setTenant(id : string) {
    this.loading = true;
    this.form.get('project').disable({onlySelf : true});
    this.tenantService.get(id)
      .pipe(finalize(() => this.loading = false))
      .subscribe(res => {
        this.form.patchValue({tenant : res});
        this.form.get('project').get('id').enable();
      });
  }

  resetProject() {
    this.form.get('project').patchValue({id : null});
  }

  getTenant() {
    return this.form.get('tenant').value;
  }

  /**
   * Записывает форму из компоненты
   * @param event
   */
  onFormChange(event : FormGroup[]) {
    this.formItems.controls = event;
    this.formItems.patchValue([]);
    this.form.patchValue({});
  }

  save() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.getSubmitMethod()
      .pipe(finalize((() => this.loading = false)))
      .subscribe(
        (res) => this.onSuccess(res),
        err => this.onError(err)
      );
  }

  saveAndSend() {
    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.getSubmitMethod()
      .pipe(mergeMap(res => {
        return this.invoiceService.sendToPayer(this.invoice.id || res.id)
      }))
      .pipe(finalize((() => this.loading = false)))
      .subscribe(
        (res) => this.onSuccess(res),
        err => this.onError(err)
      );
  }

  getSubmitMethod() {
    return this.invoiceService.save(this.getInvoiceFullModel());
  }

  getInvoiceFullModel() : Invoice {
    let invoice = Invoice.plainToClass(this.form.getRawValue());
    invoice.items = this.productBuilderService
      .filterEmptyExtralimits(invoice.items)
      .filter((i) => i.product);

    return invoice;
  }

  onSuccess(res : any) {
    this.modal.close();
    this.notificationsService.success('Invoice created successfully');
    DataStore.invoice.onCreate.emit(Invoice.plainToClass(this.form.value));
  }

  onError(err) {
    this.validatorService.addErrorToForm(this.form, err);
  }
}
