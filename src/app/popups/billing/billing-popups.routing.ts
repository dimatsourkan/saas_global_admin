import { Routes } from '@angular/router';
import { NewBillingComponent } from './billing-new-edit/billing-new.component';
import { BillingInfoComponent } from './billing-info/billing-info.component';
import { InvoicePopupResolver } from './billing-popup-resolver.service';
import { EditBillingComponent } from './billing-new-edit/billing-edit.component';
import { PopupHasPermissions } from '../popup-has-permissions';
import { ROLES } from '../../core/entities/user/user.enum';

export const BILLING_POPUPS_ROUTES : Routes = [

  {
    path : 'new',
    canActivate : [PopupHasPermissions],
    data : { roles : [ROLES.MANAGER] },
    component : NewBillingComponent
  },
  {
    path : ':id',
    resolve : {
      data : InvoicePopupResolver
    },
    canActivate : [PopupHasPermissions],
    data : { roles : [ROLES.MANAGER, ROLES.TENANT] },
    component : BillingInfoComponent
  },
  {
    path : ':id/edit',
    resolve : {
      data : InvoicePopupResolver
    },
    canActivate : [PopupHasPermissions],
    data : { roles : [ROLES.MANAGER] },
    component : EditBillingComponent
  }

];
