import { Routes } from '@angular/router';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectInfoComponent } from './project-info/project-info.component';
import { ProjectPopupResolver } from './project-popup-resolver.service';
import { PopupHasPermissions } from '../popup-has-permissions';
import { ROLES } from '../../core/entities/user/user.enum';

export const PROJECT_POPUPS_ROUTES : Routes = [

  {
    path : ':id',
    resolve : {
      data : ProjectPopupResolver
    },
    canActivate : [PopupHasPermissions],
    data : { roles : [ROLES.MANAGER, ROLES.TENANT] },
    component : ProjectInfoComponent
  },
  {
    path : ':id/edit',
    resolve : {
      data : ProjectPopupResolver
    },
    canActivate : [PopupHasPermissions],
    data : { roles : [ROLES.MANAGER] },
    component : ProjectEditComponent
  },

];
