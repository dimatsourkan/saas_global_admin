import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BaseItemResolver } from '../../core/services/resolve.item.service';
import { PopupService } from '../popup.service';
import { ProjectService } from '../../core/entities/project/project.service';
import { Project } from '../../core/entities/project/project.model';

@Injectable()
export class ProjectPopupResolver extends BaseItemResolver<Project> {

  constructor(
    protected popupService : PopupService,
    protected projectService : ProjectService,
    protected router : Router
  ) {
    super(projectService, router)
  }

  protected onError(error : any) {
    this.hideLoader();
    this.popupService.closeRoutingPopup();
  }

}
