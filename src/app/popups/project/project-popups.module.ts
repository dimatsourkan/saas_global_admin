import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { PROJECT_POPUPS_ROUTES } from './project-popups.routing';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectInfoComponent } from './project-info/project-info.component';
import { ProjectPopupResolver } from './project-popup-resolver.service';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(PROJECT_POPUPS_ROUTES)
  ],
  declarations : [
    ProjectEditComponent,
    ProjectInfoComponent
  ],
  providers : [
    ProjectPopupResolver
  ]
})
export class ProjectPopupsModule {
}
