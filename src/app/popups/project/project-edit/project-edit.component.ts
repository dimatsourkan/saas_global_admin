import { Component, OnInit, ViewChild } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { PLACEHOLDER_TYPE } from '../../../shared/components/form-controls/form-controls.component';
import { AbstractControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductBuilderComponent } from '../../../shared/components/product-builder/product-builder.component';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { finalize } from 'rxjs/operators';
import { plainToClassFromExist } from 'class-transformer';
import { DataStore } from '../../../core/data-store/data-store.service';
import { Project } from '../../../core/entities/project/project.model';
import { patchFormValues } from '../../../core/helpers/helpers';
import { ProjectService } from '../../../core/entities/project/project.service';
import { BASE_DATE_FORMAT } from '../../../app.constants';

@Component({
  selector : 'app-project-new',
  templateUrl : './project-edit.component.html',
  styleUrls : ['./project-edit.component.scss']
})
export class ProjectEditComponent extends BasePopupComponent implements OnInit {

  readonly PLACEHOLDER_TYPE = PLACEHOLDER_TYPE;
  readonly PROJECT_TRIAL_FORMAT = BASE_DATE_FORMAT;

  loading : boolean = false;
  project : Project = <Project>DataStore.project.item.getValue();
  form : FormGroup;
  formItems : FormArray;
  @ViewChild('productBuilder') protected productBuilder : ProductBuilderComponent;

  constructor(
    protected formBuilder : FormBuilder,
    protected projectService : ProjectService,
    protected validatorService : ValidatorService,
    protected notificationsService : NotificationsService
  ) {
    super();
    this.form = this.formBuilder.group({
      name : ['', Validators.required],
      domain : [''],
      items : this.formBuilder.array([]),
      trialPeriod : this.formBuilder.group({
        startDate : [null],
        endDate : [null],
        notes : [null]
      })
    });

    this.formItems = this.form.get('items') as FormArray;

    const endDate = this.form.get('trialPeriod').get('endDate');
    endDate.disable();
    this.form.get('trialPeriod').get('startDate').valueChanges.subscribe(value => {
      value ? endDate.enable() : endDate.disable();
      value ? endDate.setValidators([Validators.required]) : endDate.clearValidators();
      endDate.updateValueAndValidity();
    });

  }

  ngOnInit() {
    this.productBuilder.patchForm(this.project.items as any[]);
    patchFormValues(this.form, this.project);
    super.ngOnInit();
  }

  /**
   * Считает totalPrice по всем продуктам формы
   */
  get totalPrice() {
    return this.formItems.controls.reduce((summ : number, current : AbstractControl) => {
      return summ + Number(this.productBuilder.productPrice(current));
    }, 0).toFixed(2);
  }

  /**
   * Записывает форму из компоненты
   * @param event
   */
  onFormChange(event : FormGroup[]) {
    this.formItems.setErrors(null);
    this.formItems.controls = event;
  }

  submit() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    let request = plainToClassFromExist(this.project.clone(), this.form.getRawValue() as Object);
    request.items = request.items.filter((i) => i.product);
    this.projectService.update(request)
      .pipe(finalize((() => this.loading = false)))
      .subscribe(
        (res) => this.onSuccess(res),
        err => this.onError(err)
      );
  }

  onSuccess(res : any) {
    this.modal.close();
    this.notificationsService.success('Project created successfully');
    DataStore.project.onCreate.emit(Project.plainToClass(this.form.value));
  }

  onError(err) {
    this.validatorService.addErrorToForm(this.form, err);
    console.log(this.form);
  }

}
