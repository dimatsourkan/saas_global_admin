import { Component, OnInit } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { DataStore } from '../../../core/data-store/data-store.service';
import { AuthService } from '../../../core/services/auth.service';
import { Project } from '../../../core/entities/project/project.model';

@Component({
  selector: 'app-project-info',
  templateUrl: './project-info.component.html',
  styleUrls: ['./project-info.component.scss']
})
export class ProjectInfoComponent extends BasePopupComponent implements OnInit {
  project = <Project>DataStore.project.item.getValue();

  constructor(private authService : AuthService) {
    super();
  }

  get isManager() {
    return this.authService.isManager;
  }
}
