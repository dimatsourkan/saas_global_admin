import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { POPUP_ROUTES } from './popups.routing';
import { SharedModule } from '../shared/shared.module';
import { PopupHasPermissions } from './popup-has-permissions';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    RouterModule.forChild(POPUP_ROUTES)
  ],
  declarations : [],
  providers : [
    PopupHasPermissions
  ]
})
export class PopupsModule {
}
