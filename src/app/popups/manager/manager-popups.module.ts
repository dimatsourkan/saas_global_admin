import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MANAGER_POPUPS_ROUTES } from './manager-popups.routing';
import { ManagerPopupResolver } from './manager-popup-resolver.service';
import { ManagerNewComponent } from './manager-new-edit/manager-new.component';
import { ManagerEditComponent } from './manager-new-edit/manager-edit.component';
import { ManagerInfoComponent } from './manager-info/manager-info.component';
import { ManagerPopupEditResolver } from './manager-popup-edit-resolver.service';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(MANAGER_POPUPS_ROUTES)
  ],
  declarations : [
    ManagerNewComponent,
    ManagerEditComponent,
    ManagerInfoComponent
  ],
  providers : [
    ManagerPopupResolver,
    ManagerPopupEditResolver
  ]
})
export class ManagerPopupsModule {
}
