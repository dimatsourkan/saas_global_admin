import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantNewComponent } from './manager-new.component';

describe('TenantNewComponent', () => {
  let component: TenantNewComponent;
  let fixture: ComponentFixture<TenantNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TenantNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
