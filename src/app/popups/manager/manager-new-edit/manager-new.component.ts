import { Component } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { REGEXPS } from '../../../app.constants';
import { DataStore } from '../../../core/data-store/data-store.service';
import { patchFormValues } from '../../../core/helpers/helpers';
import { finalize } from 'rxjs/operators';
import { Manager } from '../../../core/entities/manager/manager.model';
import { ManagerService } from '../../../core/entities/manager/manager.service';
import { PERMISSIONS } from '../../../core/entities/permission/permissions.list';
import { MANAGER_STATUS } from '../../../core/entities/manager/manager.enum';

@Component({
  selector : 'app-manager-new',
  templateUrl : './manager-new-edit.component.html',
  styleUrls : ['./manager-new-edit.component.scss']
})
export class ManagerNewComponent extends BasePopupComponent {

  readonly PERMISSIONS = PERMISSIONS;
  readonly MANAGER_STATUS = MANAGER_STATUS;
  readonly REGEXPS = REGEXPS;
  loading : boolean = false;
  form : FormGroup;
  manager : Manager;

  constructor(
    protected notificationsService : NotificationsService,
    protected validatorService : ValidatorService,
    protected managerService : ManagerService,
    protected formBuilder : FormBuilder
  ) {
    super();

    this.form = this.formBuilder.group({

      skype : ['', [Validators.required]],
      email : ['', [Validators.required, Validators.email]],
      role : ['', [Validators.required]],
      status : [MANAGER_STATUS.ACTIVE, [Validators.required]],
      person : this.formBuilder.group({
        firstName : ['', [Validators.required]],
        lastName : ['', [Validators.required]]
      })
    });

    /**
     * Нужно для того что бы установить значения по умолчанию для модели
     */
    patchFormValues(this.form, new Manager());

  }

  submitMethod() {
    return this.managerService.save(Manager.plainToClass(this.form.value));
  }

  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.submitMethod()
      .pipe(finalize((() => this.loading = false)))
      .subscribe(
        (res) => this.onSuccess(res),
        err => this.validatorService.addErrorToForm(this.form, err)
      );

  }

  onSuccess(res : any) {
    this.modal.close();
    this.notificationsService.success('Manager created successfully');
    DataStore.manager.onCreate.emit(Manager.plainToClass(this.form.value));
  }
}
