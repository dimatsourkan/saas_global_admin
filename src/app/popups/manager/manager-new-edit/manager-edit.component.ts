import { Component, OnInit } from '@angular/core';
import { ManagerNewComponent } from './manager-new.component';
import { DataStore } from '../../../core/data-store/data-store.service';
import { patchFormValues } from '../../../core/helpers/helpers';
import { Manager } from '../../../core/entities/manager/manager.model';
import { classToClass, plainToClassFromExist } from 'class-transformer';

@Component({
  selector : 'app-manager-edit',
  templateUrl : './manager-new-edit.component.html',
  styleUrls : ['./manager-new-edit.component.scss']
})
export class ManagerEditComponent extends ManagerNewComponent implements OnInit {

  manager : Manager = <Manager>DataStore.manager.item.getValue();

  ngOnInit() {
    patchFormValues(this.form, this.manager);
    super.ngOnInit();
  }

  submitMethod() {
    return this.managerService.update(plainToClassFromExist(classToClass(this.manager), this.form.value as Object));
  }

  onSuccess(res : any) {
    this.modal.close();
    this.notificationsService.success('Manager updated successfully');
    DataStore.manager.onUpdate.emit(plainToClassFromExist(classToClass(this.manager), this.form.value as Object));
  }
}
