import { Component, OnInit } from '@angular/core';
import { BasePopupComponent } from '../../popup.component'
import { DataStore } from '../../../core/data-store/data-store.service';
import { Manager } from '../../../core/entities/manager/manager.model';

@Component({
  selector: 'app-manager-info',
  templateUrl: './manager-info.component.html',
  styleUrls: ['./manager-info.component.scss']
})
export class ManagerInfoComponent extends BasePopupComponent implements OnInit {
  manager = <Manager>DataStore.manager.item.getValue();
}
