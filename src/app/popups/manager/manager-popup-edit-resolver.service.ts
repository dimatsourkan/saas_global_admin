import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { BaseItemResolver } from '../../core/services/resolve.item.service';
import { PopupService } from '../popup.service';
import { Manager } from '../../core/entities/manager/manager.model';
import { ManagerService } from '../../core/entities/manager/manager.service';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { DataStore } from '../../core/data-store/data-store.service';

@Injectable()
export class ManagerPopupEditResolver extends BaseItemResolver<Manager> {

  constructor(
    protected popupService : PopupService,
    protected managerService : ManagerService,
    protected router : Router
  ) {
    super(managerService, router)
  }

  resolve(route : ActivatedRouteSnapshot, state : RouterStateSnapshot) : Observable<any> {

    let id : any = this.getParam(route);

    if(DataStore.isMine(id)) {
      this.onError(null);
      return of(null);
    }

    return super.resolve(route, state);
  }

  protected onError(error : any) {
    this.hideLoader();
    this.popupService.closeRoutingPopup();
  }

}
