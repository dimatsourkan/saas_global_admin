import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BaseItemResolver } from '../../core/services/resolve.item.service';
import { PopupService } from '../popup.service';
import { Manager } from '../../core/entities/manager/manager.model';
import { ManagerService } from '../../core/entities/manager/manager.service';

@Injectable()
export class ManagerPopupResolver extends BaseItemResolver<Manager> {

  constructor(
    protected popupService : PopupService,
    protected managerService : ManagerService,
    protected router : Router
  ) {
    super(managerService, router)
  }

  protected onError(error : any) {
    this.hideLoader();
    this.popupService.closeRoutingPopup();
  }

}
