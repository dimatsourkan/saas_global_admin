import { Routes } from '@angular/router';
import { ManagerNewComponent } from './manager-new-edit/manager-new.component';
import { ManagerEditComponent } from './manager-new-edit/manager-edit.component';
import { ManagerPopupResolver } from './manager-popup-resolver.service';
import { ManagerInfoComponent } from './manager-info/manager-info.component';
import { ManagerPopupEditResolver } from './manager-popup-edit-resolver.service';
import { HasPermission } from '../../core/guards/has-permission.service';
import { PERMISSIONS } from '../../core/entities/permission/permissions.list';

export const MANAGER_POPUPS_ROUTES : Routes = [

  {
    path : 'new',
    canActivate : [HasPermission],
    data : { permissions : [PERMISSIONS.MANAGE_MANAGERS] },
    component : ManagerNewComponent
  },
  {
    path : ':id',
    resolve : {
      data : ManagerPopupResolver
    },
    canActivate : [HasPermission],
    data : { permissions : [PERMISSIONS.VIEW_MANAGERS] },
    component : ManagerInfoComponent
  },
  {
    path : ':id/edit',
    resolve : {
      data : ManagerPopupEditResolver
    },
    canActivate : [HasPermission],
    data : { permissions : [PERMISSIONS.MANAGE_MANAGERS] },
    component : ManagerEditComponent
  }

];
