import { Routes } from '@angular/router';
import { MailingNewComponent } from './mailing-new-edit/mailing-new.component';
import { MailingEditComponent } from './mailing-new-edit/mailing-edit.component';
import { MailingPopupResolver } from './mailing-popup-resolver.service';

export const TENANT_POPUPS_ROUTES : Routes = [

  {
    path : 'new',
    component : MailingNewComponent
  },
  // {
  //   path : ':id',
  //   resolve : {
  //     data : TenantPopupResolver
  //   },
  //   component : TenantInfoComponent
  // },
  {
    path : ':id/edit',
    resolve : {
      data : MailingPopupResolver
    },
    component : MailingEditComponent
  }

];
