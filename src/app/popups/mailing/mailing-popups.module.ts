import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { MailingNewComponent } from './mailing-new-edit/mailing-new.component';
import { MailingEditComponent } from './mailing-new-edit/mailing-edit.component';
import { MailingPopupResolver } from './mailing-popup-resolver.service';
import { TENANT_POPUPS_ROUTES } from './mailing-popups.routing';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(TENANT_POPUPS_ROUTES)
  ],
  declarations : [
    MailingNewComponent,
    MailingEditComponent
  ],
  providers : [
    MailingPopupResolver
  ]
})
export class MailingPopupsModule {
}
