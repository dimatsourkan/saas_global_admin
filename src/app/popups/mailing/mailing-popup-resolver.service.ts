import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BaseItemResolver } from '../../core/services/resolve.item.service';
import { PopupService } from '../popup.service';
import { MailingService } from '../../core/entities/mailing/mailing.service';
import { MailingGroup } from '../../core/entities/mailing/mailing.model';

@Injectable()
export class MailingPopupResolver extends BaseItemResolver<MailingGroup> {

  constructor(
    protected popupService : PopupService,
    protected mailingService : MailingService,
    protected router : Router
  ) {
    super(mailingService, router)
  }

  protected onError(error : any) {
    this.hideLoader();
    this.popupService.closeRoutingPopup();
  }

}
