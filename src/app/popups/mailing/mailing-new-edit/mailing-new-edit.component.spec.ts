import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailingNewComponent } from './mailing-new.component';

describe('TenantNewComponent', () => {
  let component: MailingNewComponent;
  let fixture: ComponentFixture<MailingNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailingNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailingNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
