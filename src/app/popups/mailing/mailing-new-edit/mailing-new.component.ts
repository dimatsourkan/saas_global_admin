import { Component } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { DataStore } from '../../../core/data-store/data-store.service';
import { patchFormValues } from '../../../core/helpers/helpers';
import { finalize } from 'rxjs/operators';
import { MailingGroup } from '../../../core/entities/mailing/mailing.model';
import { MailingService } from '../../../core/entities/mailing/mailing.service';

@Component({
  selector : 'app-tenant-new',
  templateUrl : './mailing-new-edit.component.html',
  styleUrls : ['./mailing-new-edit.component.scss']
})
export class MailingNewComponent extends BasePopupComponent {

  loading : boolean = false;
  form : FormGroup;
  group : MailingGroup;

  constructor(
    protected notificationsService : NotificationsService,
    protected validatorService : ValidatorService,
    protected mailingService : MailingService,
    protected formBuilder : FormBuilder
  ) {
    super();

    this.form = this.formBuilder.group({
      name : ['', [Validators.required]],
      emails : [[], [Validators.required]],
      default : [false],
    });

    const emailControl = this.form.get('emails');
    this.form.get('default').valueChanges.subscribe(value => {
      if(!value) {
        emailControl.setValidators([Validators.required]);
      } else {
        emailControl.clearValidators();
      }
      emailControl.updateValueAndValidity();
    });

    /**
     * Нужно для того что бы установить значения по умолчанию для модели
     */
    patchFormValues(this.form, new MailingGroup());

  }

  submitMethod() {
    return this.mailingService.save(MailingGroup.plainToClass(this.form.value));
  }

  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.submitMethod()
      .pipe(finalize((() => this.loading = false)))
      .subscribe(
        (res) => this.onSuccess(res),
        err => this.validatorService.addErrorToForm(this.form, err)
      );

  }

  onSuccess(res : any) {
    this.modal.close();
    DataStore.mailing.onCreate.emit(MailingGroup.plainToClass(this.form.value));
    this.notificationsService.success('Group created successfully');
  }
}
