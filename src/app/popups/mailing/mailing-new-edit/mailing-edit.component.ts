import { Component, OnInit } from '@angular/core';
import { MailingNewComponent } from './mailing-new.component';
import { DataStore } from '../../../core/data-store/data-store.service';
import { patchFormValues } from '../../../core/helpers/helpers';
import { MailingGroup } from '../../../core/entities/mailing/mailing.model';
import { classToClass, plainToClassFromExist } from 'class-transformer';

@Component({
  selector : 'app-tenant-edit',
  templateUrl : './mailing-new-edit.component.html',
  styleUrls : ['./mailing-new-edit.component.scss']
})
export class MailingEditComponent extends MailingNewComponent implements OnInit {

  group : MailingGroup = <MailingGroup>DataStore.mailing.item.getValue();

  ngOnInit() {
    patchFormValues(this.form, this.group);
    this.form.get('default').disable({ onlySelf : true });
    super.ngOnInit();
  }

  submitMethod() {
    return this.mailingService.update(plainToClassFromExist(this.group.clone(), this.form.value as Object));
  }

  onSuccess(res : any) {
    this.modal.close();
    DataStore.mailing.onUpdate.emit(MailingGroup.plainToClass(this.form.value));
    this.notificationsService.success('Group edit successfully');
  }
}
