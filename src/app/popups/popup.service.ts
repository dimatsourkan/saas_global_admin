import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

const POPUP = 'popup';

@Injectable({
  providedIn : 'root'
})
export class PopupService {

  constructor(
    private router : Router
  ) {

  }

  private removaBodyStyles() {
    let body : HTMLElement = document.querySelector('body');
    body.classList.remove('open-modal');
    body.style['padding-right'] = null;
  }

  private get queryParams() {
    return {
      queryParams : this.router.parseUrl(document.location.search).queryParams
    }
  }

  closeRoutingPopup() {
    this.removaBodyStyles();
    return this.router.navigate([{
      outlets : {popup : null}
    }], this.queryParams);
  }

  /**
   * Tenant popups
   * @param id
   */
  showTenantInfo(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'tenant', id]}
    }], this.queryParams);
  }

  showTenantNew() {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'tenant', 'new']}
    }], this.queryParams);
  }

  showTenantEdit(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'tenant', id, 'edit']}
    }], this.queryParams);
  }

  /**
   * Billing popups
   */
  showInvoiceNew() {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'invoice', 'new']}
    }], this.queryParams);
  }

  showInvoiceEdit(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'invoice', id, 'edit']}
    }], this.queryParams);
  }

  showInvoiceInfo(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'invoice', id]}
    }], this.queryParams);
  }

  /**
   * Product popups
   */
  showProductNew() {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'product', 'new']}
    }], this.queryParams);
  }

  showProductInfo(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'product', id]}
    }], this.queryParams);
  }

  showProductEdit(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'product', id, 'edit']}
    }], this.queryParams);
  }

  showProductPlanNew(productId : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'product', productId, 'plan', 'new']}
    }], this.queryParams);
  }

  showProductPlanInfo(productId : string, id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'product', productId, 'plan', id]}
    }], this.queryParams);
  }

  showProductPlanEdit(productId : string, id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'product', productId, 'plan', id, 'edit']}
    }], this.queryParams);
  }

  /**
   * Manager popups
   * @param id
   */
  showManagerInfo(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'manager', id]}
    }], this.queryParams);
  }

  showManagerNew() {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'manager', 'new']}
    }], this.queryParams);
  }

  showManagerEdit(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'manager', id, 'edit']}
    }], this.queryParams);
  }

  /**
   * Project popups
   * @param id
   */
  showProjectInfo(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'project', id]}
    }], this.queryParams);
  }

  showProjectEdit(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'project', id, 'edit']}
    }], this.queryParams);
  }

  /**
   * Role popups
   * @param id
   */
  showRoleInfo(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'role', id]}
    }], this.queryParams);
  }

  showRoleNew() {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'role', 'new']}
    }], this.queryParams);
  }

  showRoleEdit(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'role', id, 'edit']}
    }], this.queryParams);
  }

  /**
   * Project request popups
   * @param id
   */
  showProjectRequestInfo(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'project-request', id]}
    }], this.queryParams);
  }

  /**
   * MailingSend popups
   */
  showMailingGroupNew() {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'mailing', 'new']}
    }], this.queryParams);
  }

  showMailingGroupEdit(id : string) {
    return this.router.navigate([{
      outlets : {popup : [POPUP, 'mailing', id, 'edit']}
    }], this.queryParams);
  }

}
