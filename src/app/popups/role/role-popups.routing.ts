import { Routes } from '@angular/router';
import { RolePopupResolver } from './role-popup-resolver.service';
import { RoleInfoComponent } from './role-info/role-info.component';
import { RoleNewComponent } from './role-new-edit/role-new.component';
import { RoleEditComponent } from './role-new-edit/role-edit.component';

export const ROLES_POPUPS_ROUTES : Routes = [

  {
    path : 'new',
    component : RoleNewComponent
  },
  {
    path : ':id',
    resolve : {
      data : RolePopupResolver
    },
    component : RoleInfoComponent
  },
  {
    path : ':id/edit',
    resolve : {
      data : RolePopupResolver
    },
    component : RoleEditComponent
  }

];
