import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BaseItemResolver } from '../../core/services/resolve.item.service';
import { PopupService } from '../popup.service';
import { RoleService } from '../../core/entities/role/role.service';
import { Role } from '../../core/entities/role/role.model';

@Injectable()
export class RolePopupResolver extends BaseItemResolver<Role> {

  constructor(
    protected popupService : PopupService,
    protected roleService : RoleService,
    protected router : Router
  ) {
    super(roleService, router)
  }

  protected onError(error : any) {
    this.hideLoader();
    this.popupService.closeRoutingPopup();
  }

}
