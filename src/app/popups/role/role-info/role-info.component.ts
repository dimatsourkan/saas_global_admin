import { Component, OnInit } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { DataStore } from '../../../core/data-store/data-store.service';
import { Role } from '../../../core/entities/role/role.model';

@Component({
  selector: 'app-product-info',
  templateUrl: './role-info.component.html',
  styleUrls: ['./role-info.component.scss']
})
export class RoleInfoComponent extends BasePopupComponent implements OnInit {
  role = <Role>DataStore.role.item.getValue();
}
