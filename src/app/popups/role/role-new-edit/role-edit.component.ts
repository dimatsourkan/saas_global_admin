import { Component } from '@angular/core';
import { DataStore } from '../../../core/data-store/data-store.service';
import { RoleNewComponent } from './role-new.component';
import { patchFormValues } from '../../../core/helpers/helpers';
import { Role } from '../../../core/entities/role/role.model';
import { classToClass, plainToClass, plainToClassFromExist } from 'class-transformer';

@Component({
  selector : 'app-product-new-edit',
  templateUrl : './role-new-edit.component.html',
  styleUrls : ['./role-new-edit.component.scss']
})
export class RoleEditComponent extends RoleNewComponent {

  role : Role = <Role>DataStore.role.item.getValue();

  ngOnInit() {
    patchFormValues(this.form, this.role);
    super.ngOnInit();
  }

  onSuccess(res : any) {
    this.modal.close();
    DataStore.role.onUpdate.emit(Role.plainToClass(this.form.value));
    this.notificationsService.success('Role edit successfully');
  }

  submitMethod() {
    return this.roleService.update(plainToClassFromExist(classToClass(this.role), this.form.value));
  }

}
