import { Component } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { DataStore } from '../../../core/data-store/data-store.service';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { patchFormValues } from '../../../core/helpers/helpers';
import { finalize } from 'rxjs/operators';
import { Role } from '../../../core/entities/role/role.model';
import { RoleService } from '../../../core/entities/role/role.service';
import { plainToClass } from 'class-transformer';

@Component({
  selector : 'app-product-new-edit',
  templateUrl : './role-new-edit.component.html',
  styleUrls : ['./role-new-edit.component.scss']
})
export class RoleNewComponent extends BasePopupComponent {

  loading : boolean = false;
  role : Role;
  form : FormGroup;

  constructor(
    protected formBuilder : FormBuilder,
    protected roleService : RoleService,
    protected validatorService : ValidatorService,
    protected notificationsService : NotificationsService
  ) {
    super();
    this.form = this.formBuilder.group({
      name : ['', Validators.required],
      weight : ['', Validators.required],
      permissions : [[], Validators.required]
    });

    patchFormValues(this.form, new Role());

  }

  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.submitMethod()
      .pipe(finalize(() => this.loading = false))
      .subscribe(
      (res) => this.onSuccess(res),
      err => this.validatorService.addErrorToForm(this.form, err)
    );

  }


  submitMethod() {
    return this.roleService.save(Role.plainToClass(this.form.value));
  }

  onSuccess(res : any) {
    this.modal.close();
    this.notificationsService.success('Role created successfully');
    DataStore.role.onCreate.emit(Role.plainToClass(this.form.value));
  }


}
