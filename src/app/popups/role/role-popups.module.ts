import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { ROLES_POPUPS_ROUTES } from './role-popups.routing';
import { RolePopupResolver } from './role-popup-resolver.service';
import { RoleInfoComponent } from './role-info/role-info.component';
import { RoleNewComponent } from './role-new-edit/role-new.component';
import { RoleEditComponent } from './role-new-edit/role-edit.component';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(ROLES_POPUPS_ROUTES)
  ],
  declarations : [
    RoleInfoComponent,
    RoleNewComponent,
    RoleEditComponent
  ],
  providers : [
    RolePopupResolver
  ]
})
export class RolePopupsModule {
}
