import { Injectable } from '@angular/core';
import { FromRoles } from '../core/guards/from-roles.service';
import { PopupService } from './popup.service';
import { AuthService } from '../core/services/auth.service';
import { Router } from '@angular/router';

@Injectable()
export class PopupHasPermissions extends FromRoles {

  constructor(
    private popupService : PopupService,
    protected authService : AuthService,
    protected router : Router,
  ) {
    super(authService, router);
  }

  onError() {
    this.popupService.closeRoutingPopup();
  }

}
