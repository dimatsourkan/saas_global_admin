import { Component } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ProductService } from '../../../core/entities/product/product.service';
import { Product } from '../../../core/entities/product/product.model';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { DataStore } from '../../../core/data-store/data-store.service';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { patchFormValues } from '../../../core/helpers/helpers';
import { finalize } from 'rxjs/operators';
import { ProductPlan } from '../../../core/entities/product-plan/product-plan.model';
import { LIMIT_TYPES } from '../../../core/entities/product/product-limit.model';
import { REGEXPS } from '../../../app.constants';
import { ProductPopupsService } from '../product-popups.service';
import { classToClass, plainToClassFromExist } from 'class-transformer';
import { PRODUCT_STATUS, PRODUCT_TYPE } from '../../../core/entities/product/product.enum';

@Component({
  selector : 'app-product-edit',
  templateUrl : './product-edit.component.html',
  styleUrls : ['./product-edit.component.scss']
})
export class ProductEditComponent extends BasePopupComponent {

  product : Product = <Product>DataStore.product.item.getValue();
  readonly PRODUCT_STATUS = PRODUCT_STATUS;
  readonly PRODUCT_TYPE = PRODUCT_TYPE;
  readonly LIMIT_TYPES = LIMIT_TYPES;
  readonly REGEXPS = REGEXPS;
  loading : boolean = false;
  form : FormGroup;
  productLimits : FormArray;

  constructor(
    private formBuilder : FormBuilder,
    private productService : ProductService,
    private validatorService : ValidatorService,
    private productPopupsService : ProductPopupsService,
    private notificationsService : NotificationsService
  ) {
    super();

    this.form = this.productPopupsService.productForm;
    this.productLimits = this.form.get('productLimits') as FormArray;
    patchFormValues(this.form, new Product());

  }

  ngOnInit() {
    patchFormValues(this.form, this.product);
    this.form.get('type').disable();
    super.ngOnInit();
  }

  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    return this.productService.update(plainToClassFromExist(classToClass(this.product), this.form.value))
      .pipe(finalize(() => this.loading = false))
      .subscribe(
      (res) => this.onSuccess(res),
      err => this.validatorService.addErrorToForm(this.form, err)
    );

  }

  onSuccess(res : any) {
    this.modal.close();
    this.notificationsService.success('Product edit successfully');
    DataStore.product.onUpdate.emit(plainToClassFromExist(classToClass(this.product), this.form.value));
  }


}
