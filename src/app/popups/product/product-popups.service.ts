import { Injectable } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PRODUCT_STATUS, PRODUCT_TYPE } from '../../core/entities/product/product.enum';
import { CustomValidators } from '../../shared/validation/validators/custom.validator';
import { LIMIT_TYPES } from '../../core/entities/product/product-limit.model';

@Injectable()
export class ProductPopupsService {

  constructor(private formBuilder : FormBuilder) {

  }

  get productForm() {
    return this.formBuilder.group({
      name : ['', Validators.required],
      description : ['', Validators.required],
      shortDescription : ['', Validators.required],
      new : [true, Validators.required],
      status : [PRODUCT_STATUS.INACTIVE, Validators.required],
      type : [PRODUCT_TYPE.SINGLE, Validators.required],
      comingSoon : [false, Validators.required],
      landingLink : [null, [Validators.required, CustomValidators.link]],
      additionalProducts : [[]],
      productLimits : this.formBuilder.array([
        this.productLimitForm
      ])
    })
  }

  get productLimitForm() {
    return this.formBuilder.group({
      name : [null, [Validators.required]],
      type : [LIMIT_TYPES.MULTIPLE, [Validators.required]],
      extras : [[]]
    });

  }

}
