import { Component, OnInit } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { DataStore } from '../../../core/data-store/data-store.service';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.scss']
})
export class ProductInfoComponent extends BasePopupComponent implements OnInit {
  product = DataStore.product.item.getValue();
}
