import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { BaseItemResolver } from '../../core/services/resolve.item.service';
import { PopupService } from '../popup.service';
import { ProductPlanService } from '../../core/entities/product-plan/product-plan.service';
import { ProductPlan } from '../../core/entities/product-plan/product-plan.model';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class ProductPlanPopupResolver extends BaseItemResolver<ProductPlan> {

  constructor(
    protected popupService : PopupService,
    protected productPlanService : ProductPlanService,
    protected router : Router
  ) {
    super(productPlanService, router)
  }

  resolve(route : ActivatedRouteSnapshot, state : RouterStateSnapshot) : Observable<any> {

    this.productPlanService.productId = route.paramMap.get('id');
    let id : string = route.paramMap.get('planId');

    this.onStart();

    return this.query(id).pipe(
      map<any, any>(res => {
        return this.confirmData(res);
      }),
      catchError<any, any>((err : any, caught : Observable<any>) => {
        this.onError(err);
        throw caught;
      })
    );
  }

  protected getParam(route : ActivatedRouteSnapshot) {
    return route.paramMap.get('id');
  }

  protected query(id : string) {
    return this.service.get(id);
  }

  protected onError(error : any) {
    this.hideLoader();
    this.popupService.closeRoutingPopup();
  }

}
