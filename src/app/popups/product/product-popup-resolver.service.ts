import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BaseItemResolver } from '../../core/services/resolve.item.service';
import { ProductService } from '../../core/entities/product/product.service';
import { Product } from '../../core/entities/product/product.model';
import { PopupService } from '../popup.service';

@Injectable()
export class ProductPopupResolver extends BaseItemResolver<Product> {

  constructor(
    protected popupService : PopupService,
    protected productService : ProductService,
    protected router : Router
  ) {
    super(productService, router)
  }

  protected onError(error : any) {
    this.hideLoader();
    this.popupService.closeRoutingPopup();
  }

}
