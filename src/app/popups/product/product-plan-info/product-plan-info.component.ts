import { Component, OnInit } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { DataStore } from '../../../core/data-store/data-store.service';
import { ProductPlan } from '../../../core/entities/product-plan/product-plan.model';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'app-product-plan-info',
  templateUrl: './product-plan-info.component.html',
  styleUrls: ['./product-plan-info.component.scss']
})
export class ProductPlanInfoComponent extends BasePopupComponent implements OnInit {
  productPlan = <ProductPlan>DataStore.productPlan.item.getValue();
}
