import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductPlanInfoComponent } from './product-plan-info.component';

describe('ProductInfoComponent', () => {
  let component: ProductPlanInfoComponent;
  let fixture: ComponentFixture<ProductPlanInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductPlanInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductPlanInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
