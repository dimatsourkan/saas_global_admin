import { Routes } from '@angular/router';
import { ProductNewComponent } from './product-new/product-new.component';
import { ProductPopupResolver } from './product-popup-resolver.service';
import { ProductInfoComponent } from './product-info/product-info.component';
import { ProductPlanNewComponent } from './product-plan-new-edit/product-plan-new.component';
import { ProductPlanPopupResolver } from './product-plan-popup-resolver.service';
import { ProductPlanInfoComponent } from './product-plan-info/product-plan-info.component';
import { ProductPlanEditComponent } from './product-plan-new-edit/product-plan-edit.component';
import { HasPermission } from '../../core/guards/has-permission.service';
import { PERMISSIONS } from '../../core/entities/permission/permissions.list';
import { ProductEditComponent } from './product-edit/product-edit.component';

export const PRODUCT_POPUPS_ROUTES : Routes = [

  {
    path : 'new',
    component : ProductNewComponent
  },
  {
    path : ':id',
    resolve : {
      data : ProductPopupResolver
    },
    component : ProductInfoComponent
  },
  {
    path : ':id/edit',
    resolve : {
      data : ProductPopupResolver
    },
    component : ProductEditComponent
  },
  {
    path : ':id/plan',
    canLoad : [HasPermission],
    data : { permissions : [PERMISSIONS.MANAGE_PRODUCT_PLANS] },
    children : [
      {
        path : 'new',
        resolve : {
          data : ProductPopupResolver
        },
        component : ProductPlanNewComponent
      },
      {
        path : ':planId',
        resolve : {
          data : ProductPlanPopupResolver
        },
        component : ProductPlanInfoComponent
      },
      {
        path : ':planId/edit',
        resolve : {
          data : ProductPlanPopupResolver,
          product : ProductPopupResolver
        },
        component : ProductPlanEditComponent
      }
    ]
  }

];
