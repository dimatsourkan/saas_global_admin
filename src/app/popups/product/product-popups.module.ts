import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { PRODUCT_POPUPS_ROUTES } from './product-popups.routing';
import { ProductNewComponent } from './product-new/product-new.component';
import { ProductInfoComponent } from './product-info/product-info.component';
import { ProductPlanNewComponent } from './product-plan-new-edit/product-plan-new.component';
import { ProductPlanEditComponent } from './product-plan-new-edit/product-plan-edit.component';
import { ProductPlanInfoComponent } from './product-plan-info/product-plan-info.component';
import { ProductPopupResolver } from './product-popup-resolver.service';
import { ProductPlanPopupResolver } from './product-plan-popup-resolver.service';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductPopupsService } from './product-popups.service';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(PRODUCT_POPUPS_ROUTES)
  ],
  declarations : [
    ProductNewComponent,
    ProductInfoComponent,
    ProductEditComponent,
    ProductPlanNewComponent,
    ProductPlanEditComponent,
    ProductPlanInfoComponent
  ],
  providers : [
    ProductPopupsService,
    ProductPopupResolver,
    ProductPlanPopupResolver,
  ]
})
export class ProductPopupsModule {
}
