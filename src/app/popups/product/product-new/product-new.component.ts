import { Component } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ProductService } from '../../../core/entities/product/product.service';
import { Product } from '../../../core/entities/product/product.model';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { DataStore } from '../../../core/data-store/data-store.service';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { patchFormValues } from '../../../core/helpers/helpers';
import { finalize } from 'rxjs/operators';
import { LIMIT_TYPES } from '../../../core/entities/product/product-limit.model';
import { REGEXPS } from '../../../app.constants';
import { ProductPopupsService } from '../product-popups.service';
import { PRODUCT_STATUS, PRODUCT_TYPE } from '../../../core/entities/product/product.enum';

@Component({
  selector : 'app-product-new',
  templateUrl : './product-new.component.html',
  styleUrls : ['./product-new.component.scss']
})
export class ProductNewComponent extends BasePopupComponent {

  readonly LIMIT_TYPES = LIMIT_TYPES;
  readonly PRODUCT_STATUS = PRODUCT_STATUS;
  readonly PRODUCT_TYPE = PRODUCT_TYPE;
  readonly REGEXPS = REGEXPS;
  productLimits : FormArray;
  loading : boolean = false;
  form : FormGroup;

  constructor(
    private formBuilder : FormBuilder,
    private productService : ProductService,
    private validatorService : ValidatorService,
    private productPopupsService : ProductPopupsService,
    private notificationsService : NotificationsService
  ) {
    super();

    this.form = this.productPopupsService.productForm;
    this.productLimits = this.form.get('productLimits') as FormArray;
    patchFormValues(this.form, new Product());

  }

  get isSingle() {
    return this.form.value.type === PRODUCT_TYPE.SINGLE;
  }

  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.productService.save(Product.plainToClass(this.form.value))
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        (res) => this.onSuccess(res),
        err => this.validatorService.addErrorToForm(this.form, err)
      );

  }

  onSuccess(res : any) {
    this.modal.close();
    this.notificationsService.success('Product created successfully');
    DataStore.product.onCreate.emit(Product.plainToClass(this.form.value));
  }

}
