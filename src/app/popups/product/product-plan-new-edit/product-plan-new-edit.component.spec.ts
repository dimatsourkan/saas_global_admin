import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductPlanNewComponent } from './product-plan-new.component';

describe('ProductPlanNewEditComponent', () => {
  let component: ProductPlanNewComponent;
  let fixture: ComponentFixture<ProductPlanNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductPlanNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductPlanNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
