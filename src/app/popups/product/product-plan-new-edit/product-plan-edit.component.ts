import { Component } from '@angular/core';
import { Helpers, patchFormValues } from '../../../core/helpers/helpers';
import { DataStore } from '../../../core/data-store/data-store.service';
import { ProductPlan } from '../../../core/entities/product-plan/product-plan.model';
import { ProductPlanNewComponent } from './product-plan-new.component';
import { classToClass, plainToClassFromExist } from 'class-transformer';

@Component({
  selector: 'app-product-plan-new-edit',
  templateUrl: './product-plan-new-edit.component.html',
  styleUrls: ['./product-plan-new-edit.component.scss']
})
export class ProductPlanEditComponent extends ProductPlanNewComponent {

  productPlan : ProductPlan = <ProductPlan>DataStore.productPlan.item.getValue();

  ngOnInit() {

    // создание форм для лимитов
    this.removeLimit(this.productPlan.limits.length);
    this.productPlan.limits.forEach(() => {
      this.addLimit();
    });

    patchFormValues(this.form, this.productPlan);
    super.ngOnInit();
  }

  submitMethod() {
    this.productPlanService.productId = this.route.snapshot.paramMap.get('id');
    return this.productPlanService.update(plainToClassFromExist(
      classToClass(this.productPlan), this.form.value as Object)
    );
  }

  onSuccess(res : any) {
    this.modal.close();
    this.notificationsService.success('Product plan edit successfully');
    DataStore.productPlan.onUpdate.emit(plainToClassFromExist(classToClass(this.productPlan), this.form.value));
  }

}
