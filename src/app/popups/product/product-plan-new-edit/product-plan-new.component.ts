import { Component } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { patchFormValues } from '../../../core/helpers/helpers';
import { finalize } from 'rxjs/operators';
import { DataStore } from '../../../core/data-store/data-store.service';
import { REGEXPS } from '../../../app.constants';
import { ProductPlan, ProductPlanLimit } from '../../../core/entities/product-plan/product-plan.model';
import { ProductPlanService } from '../../../core/entities/product-plan/product-plan.service';
import { ActivatedRoute } from '@angular/router';
import { LIMIT_TYPES, ProductLimit } from '../../../core/entities/product/product-limit.model';
import { Product } from '../../../core/entities/product/product.model';
import { ProductService } from '../../../core/entities/product/product.service';
import { CURRENCIES } from '../../../core/enums/currencies.enum';

@Component({
  selector : 'app-product-plan-new-edit',
  templateUrl : './product-plan-new-edit.component.html',
  styleUrls : ['./product-plan-new-edit.component.scss']
})
export class ProductPlanNewComponent extends BasePopupComponent {

  product : Product = DataStore.product.item.getValue();
  productLimits : ProductLimit[];
  readonly LIMIT_TYPES = LIMIT_TYPES;
  readonly REGEXPS = REGEXPS;
  loading : boolean = false;
  productPlan : ProductPlan;
  limits : FormArray;
  form : FormGroup;

  constructor(
    protected route : ActivatedRoute,
    protected formBuilder : FormBuilder,
    protected productService : ProductService,
    protected productPlanService : ProductPlanService,
    protected validatorService : ValidatorService,
    protected notificationsService : NotificationsService
  ) {
    super();

    this.productService.limits().subscribe(() => this.setFilteredLimits());
    this.productPlanService.productId = route.snapshot.paramMap.get('id');

    this.form = this.formBuilder.group({
      shortDescription : ['', Validators.required],
      description : ['', Validators.required],
      custom : [false, Validators.required],
      limits : this.formBuilder.array([
        this.planLimitForm
      ]),
      name : ['', Validators.required],
      price : this.formBuilder.group({
        amount : [''],
        currency : [CURRENCIES.USD]
      })
    });

    this.limits = this.form.get('limits') as FormArray;

    patchFormValues(this.form, new ProductPlan());

  }

  setFilteredLimits() {
    this.productLimits = DataStore.product.limits.getValue().items.filter(item => {
      return this.currentLimits.indexOf(item.id) >= 0;
    });
  }

  get planLimitForm() {
    return this.formBuilder.group({
      name : ['', Validators.required],
      value : ['', Validators.required],
      extraUnitPrice : this.formBuilder.group({
        amount : [''],
        currency : [CURRENCIES.USD]
      })
    });
  }

  get currentLimits() {
    return this.product.productLimits.map(l => l.name);
  }

  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.submitMethod()
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        (res) => this.onSuccess(res),
        err => this.validatorService.addErrorToForm(this.form, err)
      );

  }

  submitMethod() {
    return this.productPlanService.save(ProductPlan.plainToClass(this.form.value));
  }

  onSuccess(res : any) {
    this.modal.close();
    DataStore.productPlan.onCreate.emit(ProductPlan.plainToClass(this.form.value));
    this.notificationsService.success('Product plan created successfully');
  }

  addLimit() {
    let limit = this.planLimitForm;
    patchFormValues(limit, new ProductPlanLimit());
    this.limits.insert(0, limit);
  }

  removeLimit(index : number) {

    /**
     * TODO - setTimeout это костыль, без него по при удалении закрывается попап
     * Связано это с тем сто в компоненте ModalComponent есть метод clickOverlay
     * Он отслеживает клики по попапу и если кликнули на область которая находится за рамками попапа
     * то закрывает его, а все потому что элемент формы удаляется быстрее чем то событие регистрирует клик
     * и получается что на момент регистрации элемента уже не существует
     */
    setTimeout(() => {
      this.limits.removeAt(index);
    }, 10);
  }

}
