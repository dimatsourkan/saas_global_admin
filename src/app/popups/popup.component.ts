import { OnInit, ViewChild } from '@angular/core';
import { RouterModalComponent } from '../shared/components/modals/router-modal/router-modal.component';

export class BasePopupComponent implements OnInit {

  @ViewChild('modal') protected modal : RouterModalComponent;

  ngOnInit() {
    this.modal.open();
  }

}
