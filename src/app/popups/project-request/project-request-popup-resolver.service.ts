import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BaseItemResolver } from '../../core/services/resolve.item.service';
import { PopupService } from '../popup.service';
import { ProjectRequestService } from '../../core/entities/project-request/project-request.service';
import { ProjectRequest } from '../../core/entities/project-request/project-request.model';

@Injectable()
export class ProjectRequestPopupResolver extends BaseItemResolver<ProjectRequest> {

  constructor(
    protected popupService : PopupService,
    protected projectRequestService : ProjectRequestService,
    protected router : Router
  ) {
    super(projectRequestService, router)
  }

  protected onError(error : any) {
    this.hideLoader();
    this.popupService.closeRoutingPopup();
  }

}
