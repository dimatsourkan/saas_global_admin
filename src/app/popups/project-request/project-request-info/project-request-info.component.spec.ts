import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectRequestInfoComponent } from './project-request-info.component';

describe('ProjectInfoComponent', () => {
  let component: ProjectRequestInfoComponent;
  let fixture: ComponentFixture<ProjectRequestInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectRequestInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectRequestInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
