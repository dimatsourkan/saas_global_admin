import { Component, OnInit } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { DataStore } from '../../../core/data-store/data-store.service';
import { ProjectRequest } from '../../../core/entities/project-request/project-request.model';

@Component({
  selector: 'app-project-request-info',
  templateUrl: './project-request-info.component.html',
  styleUrls: ['./project-request-info.component.scss']
})
export class ProjectRequestInfoComponent extends BasePopupComponent implements OnInit {
  request = <ProjectRequest>DataStore.projectRequest.item.getValue();
}
