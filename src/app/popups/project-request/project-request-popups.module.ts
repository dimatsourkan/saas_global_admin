import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { PROJECT_POPUPS_ROUTES } from './project-request-popups.routing';
import { TranslateModule } from '@ngx-translate/core';
import { ProjectRequestPopupResolver } from './project-request-popup-resolver.service';
import { ProjectRequestInfoComponent } from './project-request-info/project-request-info.component';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(PROJECT_POPUPS_ROUTES)
  ],
  declarations : [
    ProjectRequestInfoComponent
  ],
  providers : [
    ProjectRequestPopupResolver
  ]
})
export class ProjectRequestPopupsModule {
}
