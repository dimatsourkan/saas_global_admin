import { Routes } from '@angular/router';
import { ProjectRequestPopupResolver } from './project-request-popup-resolver.service';
import { ProjectRequestInfoComponent } from './project-request-info/project-request-info.component';

export const PROJECT_POPUPS_ROUTES : Routes = [

  {
    path : ':id',
    resolve : {
      data : ProjectRequestPopupResolver
    },
    component : ProjectRequestInfoComponent
  },

];
