import { Routes } from '@angular/router';
import { PopupHasPermissions } from './popup-has-permissions';
import { ROLES } from '../core/entities/user/user.enum';
import { HasPermission } from '../core/guards/has-permission.service';
import { PERMISSIONS } from '../core/entities/permission/permissions.list';

export const POPUP_ROUTES : Routes = [

  {
    path : 'invoice',
    canLoad : [PopupHasPermissions],
    data : { roles : [ROLES.MANAGER, ROLES.TENANT] },
    loadChildren : './billing/billing-popups.module#BillingPopupsModule'
  },
  {
    path : 'project',
    canLoad : [PopupHasPermissions],
    data : { roles : [ROLES.MANAGER, ROLES.TENANT] },
    loadChildren : './project/project-popups.module#ProjectPopupsModule'
  },
  {
    path : 'project-request',
    canLoad : [PopupHasPermissions],
    data : { roles : [ROLES.MANAGER] },
    loadChildren : './project-request/project-request-popups.module#ProjectRequestPopupsModule'
  },
  {
    path : 'tenant',
    canLoad : [PopupHasPermissions, HasPermission],
    data : { roles : [ROLES.MANAGER], permissions : [PERMISSIONS.MANAGE_TENANTS, PERMISSIONS.VIEW_TENANTS] },
    loadChildren : './tenant/tenant-popups.module#TenantPopupsModule'
  },
  {
    path : 'manager',
    canLoad : [PopupHasPermissions, HasPermission],
    data : { roles : [ROLES.MANAGER], permissions : [PERMISSIONS.MANAGE_MANAGERS, PERMISSIONS.VIEW_MANAGERS] },
    loadChildren : './manager/manager-popups.module#ManagerPopupsModule'
  },
  {
    path : 'product',
    canLoad : [PopupHasPermissions, HasPermission],
    data : { roles : [ROLES.MANAGER], permissions : [PERMISSIONS.MANAGE_PRODUCTS] },
    loadChildren : './product/product-popups.module#ProductPopupsModule'
  },
  {
    path : 'role',
    canLoad : [PopupHasPermissions, HasPermission],
    data : { roles : [ROLES.MANAGER], permissions : [PERMISSIONS.MANAGE_ROLES] },
    loadChildren : './role/role-popups.module#RolePopupsModule'
  },
  {
    path : 'mailing',
    canLoad : [PopupHasPermissions, HasPermission],
    data : { roles : [ROLES.MANAGER], permissions : [PERMISSIONS.MASS_MAILING] },
    loadChildren : './mailing/mailing-popups.module#MailingPopupsModule'
  }

];
