import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BaseItemResolver } from '../../core/services/resolve.item.service';
import { TenantService } from '../../core/entities/tenant/tenant.service';
import { PopupService } from '../popup.service';
import { Tenant } from '../../core/entities/tenant/tenant.model';

@Injectable()
export class TenantPopupResolver extends BaseItemResolver<Tenant> {

  constructor(
    protected popupService : PopupService,
    protected tenantService : TenantService,
    protected router : Router
  ) {
    super(tenantService, router)
  }

  protected onError(error : any) {
    this.hideLoader();
    this.popupService.closeRoutingPopup();
  }

}
