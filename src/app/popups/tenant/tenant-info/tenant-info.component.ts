import { Component, OnInit } from '@angular/core';
import { BasePopupComponent } from '../../popup.component'
import { DataStore } from '../../../core/data-store/data-store.service';
import { Tenant } from '../../../core/entities/tenant/tenant.model';

@Component({
  selector: 'app-tenant-info',
  templateUrl: './tenant-info.component.html',
  styleUrls: ['./tenant-info.component.scss']
})
export class TenantInfoComponent extends BasePopupComponent implements OnInit {
  tenant = <Tenant>DataStore.tenant.item.getValue();
}
