import { Component, OnInit } from '@angular/core';
import { TenantNewComponent } from './tenant-new.component';
import { DataStore } from '../../../core/data-store/data-store.service';
import { patchFormValues } from '../../../core/helpers/helpers';
import { Tenant } from '../../../core/entities/tenant/tenant.model';
import { classToClass, plainToClassFromExist } from 'class-transformer';

@Component({
  selector : 'app-tenant-edit',
  templateUrl : './tenant-new-edit.component.html',
  styleUrls : ['./tenant-new-edit.component.scss']
})
export class TenantEditComponent extends TenantNewComponent implements OnInit {

  tenant : Tenant = <Tenant>DataStore.tenant.item.getValue();

  ngOnInit() {
    patchFormValues(this.form, this.tenant);
    super.ngOnInit();
  }

  submitMethod() {
    return this.tenantService.update(plainToClassFromExist(classToClass(this.tenant), this.form.value));
  }

  onSuccess(res : any) {
    this.modal.close();
    DataStore.tenant.onUpdate.emit(plainToClassFromExist(classToClass(this.tenant), this.form.value));
    this.notificationsService.success('Tenant updated successfully');
  }
}
