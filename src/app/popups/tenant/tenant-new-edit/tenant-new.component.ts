import { Component } from '@angular/core';
import { BasePopupComponent } from '../../popup.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { TenantService } from '../../../core/entities/tenant/tenant.service';
import { REGEXPS } from '../../../app.constants';
import { CustomValidators } from '../../../shared/validation/validators/custom.validator';
import { DataStore } from '../../../core/data-store/data-store.service';
import { Tenant } from '../../../core/entities/tenant/tenant.model';
import { TENANT_STATUS, TENANT_TYPE } from '../../../core/entities/tenant/tenant.enum';
import { patchFormValues } from '../../../core/helpers/helpers';
import { finalize } from 'rxjs/operators';
import { FilterService } from '../../../core/services/filter.service';
import { MANAGER_STATUS } from '../../../core/entities/manager/manager.enum';
import { PERMISSIONS } from '../../../core/entities/permission/permissions.list';

@Component({
  selector : 'app-tenant-new',
  templateUrl : './tenant-new-edit.component.html',
  styleUrls : ['./tenant-new-edit.component.scss']
})
export class TenantNewComponent extends BasePopupComponent {

  readonly TENANT_STATUS = TENANT_STATUS;
  readonly TENANT_TYPE = TENANT_TYPE;
  readonly PERMISSIONS = PERMISSIONS;
  readonly REGEXPS = REGEXPS;
  loading : boolean = false;
  form : FormGroup;
  tenant : Tenant;
  managersFilter : FilterService;

  get showFromCompany() {
    return parseInt(this.form.value.type) === TENANT_TYPE.COMPANY;
  }

  constructor(
    protected notificationsService : NotificationsService,
    protected validatorService : ValidatorService,
    protected tenantService : TenantService,
    protected formBuilder : FormBuilder
  ) {
    super();

    this.managersFilter = (new FilterService()).init();
    this.managersFilter.status = MANAGER_STATUS.ACTIVE;
    this.managersFilter.cancelCurrentChangeEvent();

    this.form = this.formBuilder.group({

      notes : [''],
      managerId : [''],
      status : ['', [Validators.required]],
      type : ['', Validators.required],
      phone : ['', [Validators.required, CustomValidators.phone]],
      email : ['', [Validators.required, Validators.email]],
      billingAddress : [''],
      person : this.formBuilder.group({
        firstName : ['', [Validators.required]],
        lastName : ['', [Validators.required]]
      }),

      address : this.formBuilder.group({
        countryCode : ['', [Validators.required]],
        address : ['', [Validators.required]],
        zipCode : ['', [CustomValidators.numbersOrLetters]]
      }),

      billingInfo : this.formBuilder.group({
        company : [''],
        invoiceEmail : ['', [Validators.required, Validators.email]],
        taxPercent : ['', CustomValidators.intOrFloat],
        vat : ['', CustomValidators.numeric]
      })
    });

    /**
     * TODO - Можно сказать что костыль
     * Пока что не нашел решения как вынести это в CustomValidators
     * Валидация для поля company
     * Оно должно быть обязательным если поле type === TENANT_TYPE.COMPANY
     */
    const companyControl = this.form.get('billingInfo').get('company');
    this.form.get('type').valueChanges.subscribe((type : string) => {

      if(parseInt(type) === TENANT_TYPE.COMPANY) {
        companyControl.setValidators([Validators.required]);
      } else {
        companyControl.clearValidators();
      }
      companyControl.updateValueAndValidity();

    });

    /**
     * Нужно для того что бы установить значения по умолчанию для модели
     */
    patchFormValues(this.form, new Tenant());

  }

  submitMethod() {
    return this.tenantService.save(Tenant.plainToClass(this.form.value));
  }

  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.submitMethod()
      .pipe(finalize((() => this.loading = false)))
      .subscribe(
        (res) => this.onSuccess(res),
        err => this.validatorService.addErrorToForm(this.form, err)
      );

  }

  onSuccess(res : any) {
    this.modal.close();
    DataStore.tenant.onCreate.emit(Tenant.plainToClass(this.form.value));
    this.notificationsService.success('Manager created successfully');
  }
}
