import { Routes } from '@angular/router';
import { TenantNewComponent } from './tenant-new-edit/tenant-new.component';
import { TenantPopupResolver } from './tenant-popup-resolver.service';
import { TenantInfoComponent } from './tenant-info/tenant-info.component';
import { TenantEditComponent } from './tenant-new-edit/tenant-edit.component';
import { HasPermission } from '../../core/guards/has-permission.service';
import { PERMISSIONS } from '../../core/entities/permission/permissions.list';

export const TENANT_POPUPS_ROUTES : Routes = [

  {
    path : 'new',
    canActivate : [HasPermission],
    data : { permissions : [PERMISSIONS.MANAGE_TENANTS] },
    component : TenantNewComponent
  },
  {
    path : ':id',
    resolve : {
      data : TenantPopupResolver
    },
    canActivate : [HasPermission],
    data : { permissions : [PERMISSIONS.VIEW_TENANTS] },
    component : TenantInfoComponent
  },
  {
    path : ':id/edit',
    resolve : {
      data : TenantPopupResolver
    },
    canActivate : [HasPermission],
    data : { permissions : [PERMISSIONS.MANAGE_TENANTS] },
    component : TenantEditComponent
  }

];
