import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { TenantInfoComponent } from './tenant-info/tenant-info.component';
import { TenantNewComponent } from './tenant-new-edit/tenant-new.component';
import { TenantEditComponent } from './tenant-new-edit/tenant-edit.component';
import { TenantPopupResolver } from './tenant-popup-resolver.service';
import { TENANT_POPUPS_ROUTES } from './tenant-popups.routing';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(TENANT_POPUPS_ROUTES)
  ],
  declarations : [
    TenantInfoComponent,
    TenantNewComponent,
    TenantEditComponent
  ],
  providers : [
    TenantPopupResolver
  ]
})
export class TenantPopupsModule {
}
