import { Routes } from '@angular/router';
import { WrapperComponent } from './core/components/wrapper/wrapper.component';
import { HomeComponent } from './pages/main/home/home.component';
import { ROLES } from './core/entities/user/user.enum';
import { IsAuthenticated } from './core/guards/is-authenticated';
import { NotAuthenticated } from './core/guards/not-authenticated';
import { FromRoles } from './core/guards/from-roles.service';
import { UiComponent } from './pages/main/ui/ui.component';
import { ConfirmEmailComponent } from './pages/main/confirm-email/confirm-email.component';
import { UnsubscribeComponent } from './pages/main/unsubscribe/unsubscribe.component';

export const APP_ROUTES : Routes = [

  {
    path : '',
    component : WrapperComponent,
    children : [

      {
        path : '',
        pathMatch : 'full',
        redirectTo : 'home'
      },
      {
        path : 'home',
        canActivate : [NotAuthenticated],
        component : HomeComponent
      },
      {
        path : 'confirm-email/:token',
        component : ConfirmEmailComponent
      },
      {
        path : 'unsubscribe/:token',
        component : UnsubscribeComponent
      },
      {
        path : 'unsubscribe/:token/:groupId',
        component : UnsubscribeComponent
      },
      {
        path : 'ui',
        component : UiComponent
      },
      {
        path : 'auth',
        canLoad : [NotAuthenticated],
        loadChildren : './pages/authorization/authorization.module#AuthorizationModule'
      },
      {
        path : 'tenant',
        data : { roles : [ROLES.TENANT] },
        canLoad : [IsAuthenticated, FromRoles],
        loadChildren : './pages/tenant/tenant.module#TenantModule'
      },
      {
        path : 'manager',
        data : { roles : [ROLES.MANAGER] },
        canLoad : [IsAuthenticated, FromRoles],
        loadChildren : './pages/manager/manager.module#ManagerModule'
      },
      {
        path : '**',
        redirectTo : '/home'
      }
    ]
  },

  /**
   * Именованый роут для работы с попапами
   */
  {
    path : 'popup',
    outlet : 'popup',
    canLoad : [IsAuthenticated],
    loadChildren : './popups/popups.module#PopupsModule'
  }

];
