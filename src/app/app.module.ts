import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { MainModule } from './pages/main/main.module';
import { RouterModule } from '@angular/router';
import { APP_ROUTES } from './app.routing';
import { CoreModule } from './core/core.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LanguageService } from './core/services/language.service';



@NgModule({
  imports : [
    BrowserModule,
    BrowserAnimationsModule,
    MainModule,
    CoreModule,
    SharedModule,
    HttpClientModule,

    RouterModule.forRoot(APP_ROUTES, {
      enableTracing: false,
      anchorScrolling: 'enabled'
    })
  ],
  declarations : [
    AppComponent
  ],
  providers : [

  ],
  bootstrap : [AppComponent]
})
export class AppModule {

  constructor(private languageService : LanguageService) {
    this.languageService.useCurrent();
  }

}
