import { Directive, ElementRef, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../../core/services/auth.service';
import { Subscription } from 'rxjs';
import { DataStore } from '../../../core/data-store/data-store.service';

@Directive({
  selector : '[hasPermission]'
})
export class HasPermissionDirective implements OnInit, OnDestroy {

  private subscription$ : Subscription;
  private permissions = [];

  @Input()
  set hasPermission(val) {
    this.permissions = val;
    this.updateView();
  }

  constructor(
    private element : ElementRef,
    private authService : AuthService,
    private templateRef : TemplateRef<any>,
    private viewContainer : ViewContainerRef
  ) {
  }

  private updateView() {

    this.viewContainer.clear();

    if(this.checkPermission()) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }

  }

  private checkPermission() {
    return this.authService.hasPermissions(this.permissions);
  }

  ngOnInit() {
    this.subscription$ = DataStore.permission.permissionArray.asObservable$.subscribe(() => {
      this.updateView();
    })
  }

  ngOnDestroy() {
    if(this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }
}
