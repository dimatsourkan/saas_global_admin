import { AbstractControl, ValidatorFn } from '@angular/forms';
import { REGEXPS } from '../../../app.constants';

export class CustomValidators {

  static get phone() : ValidatorFn {

    return (control : AbstractControl) : { [key : string] : any } | null => {

      const success = control.value ? REGEXPS.PHONE.test(control.value) : true;
      return success ? null : { phone : true };


    };
  }

  static get numeric() : ValidatorFn {

    return (control : AbstractControl) : { [key : string] : any } | null => {

      const success = control.value ? REGEXPS.NUMERIC.test(control.value) : true;
      return success ? null : { numeric : true };

    };
  }

  static get numbersOrLetters() : ValidatorFn {

    return (control : AbstractControl) : { [key : string] : any } | null => {

      const success = control.value ? REGEXPS.NUMERIC_OR_LETTERS.test(control.value) : true;
      return success ? null : { numbersOrLetters : true };

    };
  }

  static get intOrFloat() : ValidatorFn {

    return (control : AbstractControl) : { [key : string] : any } | null => {

      const success = control.value ? REGEXPS.NUMERIC_FLOAT.test(control.value) : true;
      return success ? null : { intOrFloat : true };

    };
  }

  static get link() : ValidatorFn {

    return (control : AbstractControl) : { [key : string] : any } | null => {

      const success = control.value ? REGEXPS.LINK.test(control.value) : true;
      return success ? null : { link : true };

    };
  }

}
