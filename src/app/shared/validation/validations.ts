export default {
  required : 'Required field.',
  email : 'This value is not valid email address.',
  phone : 'This value is not valid phone number.',
  numeric : 'This value must be a number.',
  intOrFloat : 'This value must be a integer or float.',
  link : 'This value must be a link.',
  minlength : 'Field has to be at least ${requiredLength} symbols',
  numbersOrLetters : 'This value must be a number or letters.',
};
