import { Injectable } from '@angular/core';
import { AbstractControl, FormArray, FormGroup } from '@angular/forms';
import forEach  from 'lodash-es/forEach';
import { HttpErrorResponse } from '@angular/common/http';
import { parseValidationKeys } from '../../core/helpers/helpers';

@Injectable({
  providedIn : 'root'
})
export class ValidatorService {

  private addErrorToControl(control : AbstractControl, error) {
    if(control) {
      control.setErrors({server : error});
    }
  }

  addErrorToForm(form : FormGroup, err : HttpErrorResponse) {

    if(!form.controls || !err.error.errors) {
      return;
    }

    if(typeof err.error.errors === 'string') {
      setTimeout(() => {
        form.setErrors(null);
      }, 3000);
      return this.addErrorToControl(form, err.error.errors);
    }

    forEach(err.error.errors, (error, key) => {

      let fields = parseValidationKeys(key);

      let control : any = form;

      forEach(fields, (field) => {
        if(control.controls && control.controls[field]) {
          control = control.controls[field];
        }
      });

      this.addErrorToControl(control, error);

    });

  }

  setTouchToControls(form : FormGroup) {

    Object.keys(form.controls).forEach(key => {

      let control : AbstractControl = form.controls[key];

      if(control instanceof FormGroup) {
        this.setTouchToControls(control);
      } else if(control instanceof FormArray) {
        control.controls.forEach((c : FormGroup) => {
          this.setTouchToControls(c);
        });
      } else {
        form.controls[key].markAsTouched({onlySelf : true});
      }

    });

  }

}
