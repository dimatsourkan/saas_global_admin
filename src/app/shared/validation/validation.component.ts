import { Component, Input } from '@angular/core';
import ValidationText from './validations';
import { FormControl } from '@angular/forms';

interface ErrorObject {
  key : string;
  value : any;
}

@Component({
  selector : 'validation-message',
  templateUrl : './validation.component.html',
  styleUrls : ['./validation.component.scss']
})
export class ValidationComponent {

  @Input() control : FormControl;

  validationText = ValidationText;

  validationErrors() : ErrorObject[] {

    if(!this.control || !this.control.errors) {
      return null;
    }

    return Object.keys(this.control.errors).map((key) => {
      return { key : key, value : this.control.errors[key] };
    });
  }

  getErrorText(error : ErrorObject) {

    const value = error.value;
    let errorText = this.validationText[error.key];

    if(!errorText) {
      errorText = 'Field error';
    }

    if(typeof value === 'object') {
      if(value.requiredLength) {
        errorText = errorText.replace('${requiredLength}', value.requiredLength);
      }
    }

    return errorText;
  }

}
