import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from '../../core/entities/country/country.model';
import { CountryService } from '../../core/entities/country/country.service';
import { DataStore } from '../../core/data-store/data-store.service';
import { map } from 'rxjs/operators';

@Pipe({
  name : 'countryByCode'
})
export class CountryByCodePipe implements PipeTransform {

  private countries$ = DataStore.country.list.asObservable$;
  private country$ = DataStore.country.item.asObservable$;

  constructor(
    private countryService : CountryService
  ) {}

  getCountry(code : string) {
    return this.countryService.get(code).subscribe();
  }

  getCountryFromList(code : string) {
    return this.countries$
      .pipe(
        map((result) => result.items),
        map((countries) => {
          return countries.find(country => country.code === code);
        })
      );
  }

  transform(value : any) : Observable<Country> {

    if(!value) {
      return null;
    }
    /**
     * Если есть список стран, то страну будет искать в этом списке
     * Если нет, то запрашивает страну с сервера
     */
    if(DataStore.country.list.getValue().items.length) {
      return this.getCountryFromList(value);
    } else {
      this.getCountry(value);
      return this.country$;
    }
  }

}
