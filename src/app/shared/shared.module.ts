import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ValidationComponent } from './validation/validation.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ModalComponent } from './components/modals/modal/modal.component';
import { RouterModalComponent } from './components/modals/router-modal/router-modal.component';
import { ClickOutsideDirective } from './directives/click-outside/click-outside.directive';
import { AccordionComponent } from './components/accordion/accordion.component';
import { AccordionPanelComponent } from './components/accordion/accordion-panel/accordion-panel.component';
import { BadgeComponent } from './components/badge/badge.component';
import { TranslateModule } from '@ngx-translate/core';
import { SearchComponent } from './components/search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutosizeDirective } from './directives/autosize/autosize.directive';
import { ConfirmComponent } from './components/modals/confirm/confirm.component';
import { CountryByCodePipe } from './pipes/country-pipe.pipe';
import { PaginationComponent } from './components/pagination/pagination.component';
import { HoverClassDirective } from './directives/hover-class/hover-class.directive';
import { HasPermissionDirective } from './directives/has-permission/has-permission.directive';
import { BaseLiveSelectComponent } from './components/form-controls/live-selects/base-live-select.component';
import { FormControlsComponent } from './components/form-controls/form-controls.component';
import { InputTextComponent } from './components/form-controls/input-text/input-text.component';
import { PasswordComponent } from './components/form-controls/password/password.component';
import { CheckboxComponent } from './components/form-controls/checkbox/checkbox.component';
import { SelectComponent } from './components/form-controls/select/select.component';
import { TextareaComponent } from './components/form-controls/textarea/textarea.component';
import { SwitcherComponent } from './components/form-controls/switcher/switcher.component';
import { SwitchBtnComponent } from './components/form-controls/switcher/switch-btn/switch-btn.component';
import {
  CountrySelectComponent } from './components/form-controls/live-selects/country-select/country-select.component';
import { RoleSelectComponent } from './components/form-controls/live-selects/role-select/role-select.component';
import {
  PermissionSelectComponent
} from './components/form-controls/live-selects/permission-select/permission-select.component';
import {
  ProductSelectComponent
} from './components/form-controls/live-selects/product-select/product-select.component';
import { TenantSelectComponent } from './components/form-controls/live-selects/tenant-select/tenant-select.component';
import { TagsInputComponent } from './components/form-controls/tags-input/tags-input.component';
import {
  SubscribersSelectComponent
} from './components/form-controls/live-selects/subscribers-select/subscribers-select.component';
import {
  ProductLimitsSelectComponent
} from './components/form-controls/live-selects/product-limits-select/product-limits-select.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { OwlDateTimeIntl, OwlDateTimeModule } from 'ng-pick-datetime';
import { NouiSliderComponent } from './components/form-controls/noui-slider/noui-slider.component';
import {
  ProjectSelectComponent
} from './components/form-controls/live-selects/project-select/project-select.component';
import {
  ManagerSelectComponent
} from './components/form-controls/live-selects/manager-select/manager-select.component';
import { ProductBuilderComponent } from './components/product-builder/product-builder.component';
import {
  TermsAndConditionsComponent
} from './components/user-levels/terms-and-conditions/terms-and-conditions.component';
import { ProductBuilderService } from './components/product-builder/product-builder.service';
import { EmailConfirmationComponent } from './components/user-levels/email-confirmation/email-confirmation.component';
import { UserLevelsComponent } from './components/user-levels/user-levels.component';
import { ProfileFilledComponent } from './components/user-levels/profile-filled/profile-filled.component';
import {
  MultipleProductsSelectComponent
} from './components/form-controls/live-selects/multiple-products-select/multiple-products-select.component';
import { ProductItemComponent } from './components/product-builder/product-item/product-item.component';
import { DefaultIntl } from './components/form-controls/datepicker/datepicker.defaults';
import {
  SingleDatepickerComponent
} from './components/form-controls/datepicker/single-datepicker/single-datepicker.component';
import {
  RangeDatepickerComponent
} from './components/form-controls/datepicker/range-datepicker/range-datepicker.component';

const FORM_CONTROLS = [
  BaseLiveSelectComponent,
  FormControlsComponent,
  InputTextComponent,
  PasswordComponent,
  CheckboxComponent,
  SelectComponent,
  TextareaComponent,
  SwitcherComponent,
  SwitchBtnComponent,
  CountrySelectComponent,
  SingleDatepickerComponent,
  RangeDatepickerComponent,
  RoleSelectComponent,
  PermissionSelectComponent,
  ProductSelectComponent,
  ProjectSelectComponent,
  TenantSelectComponent,
  ManagerSelectComponent,
  TagsInputComponent,
  SubscribersSelectComponent,
  ProductLimitsSelectComponent,
  MultipleProductsSelectComponent,
];

const DIRECTIVES = [
  ClickOutsideDirective,
  HoverClassDirective,
  HasPermissionDirective,
  AutosizeDirective,
];

const PIPES = [
  CountryByCodePipe,
];

const PRIVATE_COMPONENTS = [
  EmailConfirmationComponent,
  ProfileFilledComponent,
  TermsAndConditionsComponent,
  ProductItemComponent,
];

// @ts-ignore
@NgModule({
  imports : [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    NgSelectModule,
    OwlDateTimeModule
  ],
  providers : [
    ProductBuilderService,
    {provide : OwlDateTimeIntl, useClass : DefaultIntl}
  ],
  declarations : [
    ProductBuilderComponent,
    LoaderComponent,
    ModalComponent,
    RouterModalComponent,
    AccordionComponent,
    AccordionPanelComponent,
    BadgeComponent,
    PaginationComponent,
    SearchComponent,
    ConfirmComponent,
    ValidationComponent,
    UserLevelsComponent,
    NouiSliderComponent,

    ...PRIVATE_COMPONENTS,

    ...PIPES,

    ...DIRECTIVES,

    // Form-controls
    ...FORM_CONTROLS
  ],
  exports : [
    NgSelectModule,

    ProductBuilderComponent,
    LoaderComponent,
    ModalComponent,
    RouterModalComponent,
    AccordionComponent,
    AccordionPanelComponent,
    BadgeComponent,
    PaginationComponent,
    SearchComponent,
    ConfirmComponent,
    ValidationComponent,
    UserLevelsComponent,
    NouiSliderComponent,

    ...PIPES,

    ...DIRECTIVES,

    // Form-controls
    ...FORM_CONTROLS
  ]
})
export class SharedModule {
}
