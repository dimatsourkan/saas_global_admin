import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { PLACEHOLDER_TYPE } from '../form-controls/form-controls.component';
import { FilterService } from '../../../core/services/filter.service';

@Component({
  selector : 'app-search',
  templateUrl : './search.component.html',
  styleUrls : ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  private _filter : FilterService;
  private subscriber$ = new Subject();
  @Output() private onChange : EventEmitter<string> = new EventEmitter<string>();
  @Input() public placeholderType : string = PLACEHOLDER_TYPE.MOVE;
  @Input() public placeholder : string;
  @Input() public set filter(filter : FilterService) {
    this._filter = filter;
    this.value = filter.search
  };
  value : string = '';

  ngOnInit() {
    this.subscriber$
      .pipe(debounceTime(500))
      .subscribe((value : string) => {
        this.onChange.emit(value);
        if(this._filter) {
          this._filter.search = value;
        }
      });
  }

  ngOnDestroy() {
    this.subscriber$.unsubscribe();
  }

  searchChange(value) {
    this.subscriber$.next(value);
  }
}
