import { Injectable } from '@angular/core';

/**
 * Сервис для работы пагинации
 */
@Injectable()
export class PaginationService {

  /**
   * Получает массив чисел обозначающих номер страницы для отображения
   *
   * Текущая страница находится по середине данных страниц
   * @returns {number[]}
   */
  public getPaginationPages(current : number, total : number, showed : number) : number[] {

    let pagesArray : number[] = [];
    let firstPage : number = this.getFirstPage(current, showed);
    let lastPage : number = this.getLastPage(firstPage, current, total, showed);

    for (let i = firstPage; i <= lastPage; i++) {
      pagesArray.push(i);
    }

    if(pagesArray.length > showed) {
      pagesArray = pagesArray.slice(0, showed);
    }

    if(pagesArray.length <= 0) {
      pagesArray.push(current);
    }

    pagesArray = this.complementPages(pagesArray, showed);

    return pagesArray;
  }

  /**
   * Дополняет массив страниц до указанного параметра showed если длина массива меньше чем showed
   * @param pagesArray
   * @param showed
   */
  private complementPages(pagesArray : number[], showed : number) {

    if(showed > pagesArray.length && pagesArray[0] > 1) {

      let firstPage = pagesArray[0];
      for (let i = 1; i <= showed - pagesArray.length; i++) {
        firstPage--;
        pagesArray.unshift(firstPage);
      }

    }

    return pagesArray;
  }

  /**
   * Получает номер первой страницы в списке
   * @returns {number}
   */
  private getFirstPage(current : number, showed : number) : number {

    let firstPage : number = Math.ceil(current - showed / 2);

    if(firstPage < 1) {
      firstPage = 1;
    }

    return firstPage;
  }

  /**
   * Получает последнюю страницу в списке
   * @param firstPage
   * @param current
   * @param total
   * @param showed
   */
  private getLastPage(firstPage : number, current : number, total : number, showed : number) : number {

    let lastPage : number = Math.floor(current + showed / 2);

    if(firstPage === 1) {
      lastPage = current + showed;
    }

    if(lastPage > total) {
      lastPage = total;
    }

    return lastPage;
  }

}
