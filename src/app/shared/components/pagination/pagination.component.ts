import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { Pagination } from '../../../core/models/pagination.model';
import { BaseModel } from '../../../core/models/base.model';
import { PaginationService } from './pagination.service';
import { FilterService } from '../../../core/services/filter.service';

@Component({
  selector : 'app-pagination',
  templateUrl : './pagination.component.html',
  styleUrls : ['./pagination.component.scss'],
  providers : [PaginationService]
})
export class PaginationComponent implements OnChanges {

  _filter : FilterService;
  @Output() onChange : EventEmitter<Pagination<BaseModel>> = new EventEmitter();
  @Input() pagination = new Pagination();
  @Input() showed = 5;
  @Input() set filter(filter : FilterService) {
    this._filter = filter;
  }

  _pagination = new Pagination();
  _showedPages : number[] = [];

  constructor(private paginationService : PaginationService) {}

  ngOnChanges() {
    this._pagination = this.pagination;
    this._showedPages = this.paginationService.getPaginationPages(
      this._pagination.page,
      this._pagination.totalPages,
      this.showed
    );
  }

  goToPage(page : number) {
    if(this._pagination.page !== page) {
      this._pagination.page = page;
      this.onChange.emit(this._pagination);
      if(this._filter) {
        this._filter.page = page;
      }
    }
  }

  goToPrev() {
    if(!this.isFirstPage) {
      this.goToPage(this._pagination.page - 1);
    }
  }

  goToNext() {
    if(!this.isLastPage) {
      this.goToPage(this._pagination.page + 1);
    }
  }

  goToPrevHidden() {
    this.goToPage(this.prevHidden);
  }

  goToNextHidden() {
    this.goToPage(this.nextHidden);
  }

  get isFirstPage() {
    return this._pagination.page === 1;
  }

  get isLastPage() {
    return this._pagination.page === this._pagination.totalPages;
  }

  get hasHiddenPrevPages() {
    return this.prevHidden > 0;
  }

  get hasHiddenNextPages() {
    return this.nextHidden < this._pagination.totalPages;
  }

  get prevHidden() {
    return this._showedPages[0] - 1;
  }

  get nextHidden() {
    return this._showedPages[this._showedPages.length - 1] + 1;
  }

}
