import { animate, state, style, transition, trigger } from '@angular/animations';

export const fadeTransition = trigger('fadeTransition', [
  state('void', style({
    opacity: 0
  })),
  transition('void <=> *', animate(300)),
]);
