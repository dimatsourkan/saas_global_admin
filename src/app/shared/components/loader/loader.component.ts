import { Component, Input } from '@angular/core';
import { fadeTransition } from './loader.animation';

@Component({
  selector : 'loader',
  templateUrl : 'loader.component.html',
  styleUrls : ['loader.component.scss'],
  animations : [fadeTransition]
})

export class LoaderComponent {

  @Input() loading = false;

}
