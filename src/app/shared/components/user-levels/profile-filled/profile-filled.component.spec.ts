import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileFilledComponent } from './profile-filled.component';

describe('TermsAndConditionsComponent', () => {
  let component: ProfileFilledComponent;
  let fixture: ComponentFixture<ProfileFilledComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileFilledComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileFilledComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
