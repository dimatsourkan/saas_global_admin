import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ModalComponent } from '../../modals/modal/modal.component';

@Component({
  selector: 'app-profile-filled',
  templateUrl: './profile-filled.component.html',
  styleUrls: ['./profile-filled.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class ProfileFilledComponent implements OnInit {

  @ViewChild('modal') private modal : ModalComponent;

  ngOnInit() {
    this.modal.open();
  }

}
