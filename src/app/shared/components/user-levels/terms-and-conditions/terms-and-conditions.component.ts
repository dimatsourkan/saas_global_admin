import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '../../modals/modal/modal.component';
import { TenantService } from '../../../../core/entities/tenant/tenant.service';
import { UserService } from '../../../../core/entities/user/user.service';
import { finalize } from 'rxjs/operators';
import { NotificationsService } from '../../../../core/components/notifications/notifications.service';

@Component({
  selector: 'app-terms-and-conditions',
  templateUrl: './terms-and-conditions.component.html',
  styleUrls: ['./terms-and-conditions.component.scss']
})
export class TermsAndConditionsComponent implements OnInit {

  @ViewChild('modal') private modal : ModalComponent;

  termsAccepted = false;
  loading = false;

  constructor(
    private notificationsService : NotificationsService,
    private tenantService : TenantService,
    private userService : UserService,
  ) {

  }

  ngOnInit() {
    this.modal.open();
  }

  scrollTo(id : string) {
    let element = document.querySelector(`#${id}`);
    element.scrollIntoView({
      block : 'start',
      inline : 'start'
    });
  }

  acceptTerms() {
    this.loading = true;
    this.tenantService.acceptTerms().subscribe(() => {
      this.userService.profile()
        .pipe(finalize(() => this.loading = false))
        .subscribe(() => {
          this.notificationsService.success('Terms and conditions accepted successfully');
        });
    });
  }

}
