import { Component } from '@angular/core';
import { User } from '../../../core/entities/user/user.model';
import { DataStore } from '../../../core/data-store/data-store.service';

@Component({
  selector: 'app-user-level',
  templateUrl: './user-levels.component.html',
  styleUrls: ['./user-levels.component.scss']
})
export class UserLevelsComponent {

  user$ = DataStore.user.current.asObservable$;

}
