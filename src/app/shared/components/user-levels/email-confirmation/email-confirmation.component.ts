import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ModalComponent } from '../../modals/modal/modal.component';
import { TenantService } from '../../../../core/entities/tenant/tenant.service';
import { NotificationsService } from '../../../../core/components/notifications/notifications.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-email-confirmation',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class EmailConfirmationComponent implements OnInit {

  @ViewChild('modal') private modal : ModalComponent;
  loading = false;

  constructor(
    private notificationsService : NotificationsService,
    private tenantService : TenantService
  ) {

  }

  ngOnInit() {
    this.modal.open();
  }

  requestEmailConfirmation() {
    this.loading = true;
    this.tenantService.requestEmailConfirmation()
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => {
        this.notificationsService.success('Email sent, check your email');
        this.modal.close();
      });
  }

}
