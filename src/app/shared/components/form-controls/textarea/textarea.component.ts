import { Component, forwardRef, ViewChild } from '@angular/core';
import { FormControlsComponent } from '../form-controls.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector : 'app-textarea',
  templateUrl : './textarea.component.html',
  styleUrls : ['./textarea.component.scss'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => TextareaComponent),
      multi : true
    }
  ]
})
export class TextareaComponent extends FormControlsComponent {

  @ViewChild('area') private area : any;

  writeValue(value : any) {
    super.writeValue(value);
    this.area.nativeElement.value = value;
  }

}
