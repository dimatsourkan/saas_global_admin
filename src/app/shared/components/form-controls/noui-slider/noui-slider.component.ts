import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, Output, ViewChild } from '@angular/core';
import { noUiSlider, Options } from 'nouislider';
import * as NoUiSlider from 'nouislider';

@Component({
  selector : 'app-noui-slider',
  templateUrl : './noui-slider.component.html',
  styleUrls : ['./noui-slider.component.scss']
})
export class NouiSliderComponent implements OnChanges {

  @ViewChild('rangeSlider') private rangeSlider : ElementRef;

  @Input() private start : 0;
  @Input() private end : 10;
  @Input() private min : 0;
  @Input() private max : 10;

  @Output() onChange = new EventEmitter<[string, string]>();

  private changesCount = 0;
  private noUiSlider : noUiSlider;
  private get options() : Options {
    return {
      start : [this.start, this.end],
      step : 0.01,
      connect : true,
      range : {
        min : [this.min],
        max : [this.max]
      }
    }
  };

  ngOnChanges() {
    this.initSlider();
  }

  initSlider() {

    if(this.noUiSlider) {
      this.noUiSlider.destroy();
      this.noUiSlider = null;
    }

    NoUiSlider.create(this.rangeSlider.nativeElement, this.options);
    this.noUiSlider = this.rangeSlider.nativeElement.noUiSlider;
    this.noUiSlider.on('update', (e : [string, string]) => {
      this.changesCount++;
      if(this.changesCount > 2) {
        this.onChange.emit(e);
      }
    });
  }

}
