import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NouiSliderComponent } from './noui-slider.component';

describe('NouiSliderComponent', () => {
  let component: NouiSliderComponent;
  let fixture: ComponentFixture<NouiSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NouiSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NouiSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
