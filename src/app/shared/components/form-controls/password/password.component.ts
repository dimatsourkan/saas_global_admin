import { Component, forwardRef, ViewChild } from '@angular/core';
import { FormControlsComponent } from '../form-controls.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector : 'app-password',
  templateUrl : './password.component.html',
  styleUrls : ['./password.component.scss'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => PasswordComponent),
      multi : true
    }
  ]
})
export class PasswordComponent extends FormControlsComponent {

  showPass : boolean = false;

  @ViewChild('input') private input : any;

  writeValue(value : any) {
    super.writeValue(value);
    this.input.nativeElement.value = value;
  }

}
