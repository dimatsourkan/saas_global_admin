import { FormControlsComponent } from '../form-controls.component';
import { Subscription } from 'rxjs';
import { BaseModel } from '../../../../core/models/base.model';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { debounceTime, finalize, tap } from 'rxjs/operators';
import { CRUDService } from '../../../../core/services/crud.service';
import { DataModel } from '../../../../core/data-store/data-models/base-data.model';
import { FilterService } from '../../../../core/services/filter.service';
import findIndex from 'lodash-es/findIndex';
import { ResultList } from '../../../../core/models/result.model';

/**
 * Прямое использование этой компоненты недопустимо, она используется только для наследования
 */
@Component({
  selector : 'base-live-select-component',
  template : `
    <ng-select [loading]="loading"
               [formControl]="control"
               [items]="items"
               (change)="onChange()"
               (open)="onOpen()"
               [typeahead]="typeahead"
               [virtualScroll]="true"
               bindValue="id"
               bindLabel="name">
    </ng-select>
    <validation-message [control]="control"></validation-message>
  `
})
export class BaseLiveSelectComponent<T extends BaseModel> extends FormControlsComponent implements OnInit, OnDestroy {

  loading = false;
  typeahead = new EventEmitter<string>();
  items : T[] = [];
  subcription$ : Subscription;
  dataService : CRUDService<T, DataModel<T>>;
  totalPages = 1;

  @Input() limit = 10;
  @Input() initOnStart = false;
  @Input() filter = (new FilterService()).init();
  @Output() change = new EventEmitter();

  ngOnInit() {
    this.initTypehead();
    this.initFilterListener();
    if(this.initOnStart) {
      this.getData().subscribe();
    }
  }

  ngOnDestroy() {
    if(this.subcription$) {
      this.subcription$.unsubscribe();
    }
  }

  getData() {
    this.loading = true;
    return this.dataService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .pipe(tap(res => this.pushItems(res)))
  }

  getOne(id : string) {
    this.loading = true;
    return this.dataService.get(id)
      .pipe(finalize(() => this.loading = false))
  }

  pushItems(result : ResultList<T>) {
    this.totalPages = result.totalPages;
    if(this.filter.page === 1) {
      this.items = result.items;
    } else {
      this.items = [...this.items, ...result.items];
    }
  }

  onChange() {
    this.value = this.control.value;
    this.propagateChange(this.value);
    this.change.emit(this.value);
  }

  onOpen() {
    this.filter.limit = this.limit;
    this.filter.search = '';
    this.filter.page = 1;
    this.items = [];
  }

  initFilterListener() {
    this.filter.onChangeFilter$.subscribe(() => {
      this.getData().subscribe();
    });
  }

  initTypehead() {
    this.typeahead
      .pipe(debounceTime(200))
      .subscribe(term => {
        this.filter.search = term;
        this.filter.page = 1;
      });
  }

  writeValue(value : any) {
    if(value && value !== this.value) {
      if(this.getItemIndex(value) < 0) {
        this.getOne(value).subscribe(res => this.items = [...this.items, res]);
      }
    }
    super.writeValue(value);
  }

  getItemIndex(id : string) {
    return findIndex(this.items, (i : BaseModel) => i.id === id);
  }

  onScroll() {
    if(this.totalPages > this.filter.page) {
      this.filter.page = this.filter.page + 1;
    }
  }
}
