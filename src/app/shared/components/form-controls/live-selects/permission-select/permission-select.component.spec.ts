import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionSelectComponent } from './permission-select.component';

describe('SelectComponent', () => {
  let component : PermissionSelectComponent;
  let fixture : ComponentFixture<PermissionSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations : [PermissionSelectComponent]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
