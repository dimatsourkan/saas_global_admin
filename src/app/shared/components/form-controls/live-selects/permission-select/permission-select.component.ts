import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { PermissionService } from '../../../../../core/entities/permission/permission.service';
import { Permission } from '../../../../../core/entities/permission/permission.model';

@Component({
  selector : 'app-permission-select',
  template : `
    <div class="form-detail">
      <span class="form-detail__subtitle">
        <ng-content></ng-content>
      </span>
      <ng-select [loading]="loading"
                 [formControl]="control"
                 [multiple]="true"
                 [items]="items"
                 [placeholder]="placeholder || ''"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [virtualScroll]="true"
                 [clearable]="false"
                 (scrollToEnd)="onScroll()"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control"></validation-message>
    </div>
  `,
  styles : [
    `
      .form-detail__subtitle:empty {
        padding-bottom: 0;
      }
    `
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => PermissionSelectComponent),
      multi : true
    }
  ]
})
export class PermissionSelectComponent extends BaseLiveSelectComponent<Permission> {

  dataService = this.injector.get(PermissionService, null);

  writeValue(value : any) {
    if(value && value !== this.value) {
      this.getData().subscribe();
    }
    this.control.setValue(value);
  }

}
