import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { ProductService } from '../../../../../core/entities/product/product.service';
import { finalize, tap } from 'rxjs/operators';

@Component({
  selector : 'app-product-limits-select',
  template : `
    <div class="form-detail">
      <span class="form-detail__subtitle">
        <ng-content></ng-content>
      </span>
      <ng-select [loading]="loading"
                 [formControl]="control"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [placeholder]="placeholder || ''"
                 [typeahead]="typeahead"
                 [virtualScroll]="true"
                 [clearable]="false"
                 (scrollToEnd)="onScroll()"
                 bindValue="id"
                 [disabled]="disabled"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control"></validation-message>
    </div>
  `,
  styles : [
    `
      .form-detail__subtitle:empty {
        padding-bottom: 0;
      }
    `
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => ProductLimitsSelectComponent),
      multi : true
    }
  ]
})
export class ProductLimitsSelectComponent extends BaseLiveSelectComponent<any> {

  dataService = this.injector.get(ProductService, null);

  initFilterListener() {
    this.filter.onChangeFilter$.subscribe(() => {
      this.getLimits().subscribe();
    });
  }

  getLimits() {
    this.loading = true;
    return this.dataService.limits(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .pipe(tap(res => this.pushItems(res)))
  }

  ngOnInit() {
    this.initTypehead();
    this.initFilterListener();
    if(this.initOnStart) {
      this.getLimits().subscribe();
    }
  }

  writeValue(value : any) {
    this.control.setValue(value || null)
    // this.value = value || null;
  }

}
