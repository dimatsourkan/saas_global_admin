import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { ProjectService } from '../../../../../core/entities/project/project.service';
import { Project } from '../../../../../core/entities/project/project.model';

@Component({
  selector : 'app-project-select',
  template : `
    <div class="form-detail">
      <span class="form-detail__subtitle">
        <ng-content></ng-content>
      </span>
      <ng-select [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [typeahead]="typeahead"
                 [clearable]="false"
                 [virtualScroll]="true"
                 (scrollToEnd)="onScroll()"
                 bindValue="id"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control"></validation-message>
    </div>
  `,
  styles : [
    `
      .form-detail__subtitle:empty {
        padding-bottom: 0;
      }
    `
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => ProjectSelectComponent),
      multi : true
    }
  ]
})
export class ProjectSelectComponent extends BaseLiveSelectComponent<Project> {

  dataService = this.injector.get(ProjectService, null);

}
