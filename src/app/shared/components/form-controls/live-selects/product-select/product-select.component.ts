import { Component, forwardRef, Injector, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { ProductService } from '../../../../../core/entities/product/product.service';
import { AuthService } from '../../../../../core/services/auth.service';
import { Product } from '../../../../../core/entities/product/product.model';
import { ResultList } from '../../../../../core/models/result.model';

@Component({
  selector : 'app-product-select',
  template : `
    <div class="form-detail">
      <span class="form-detail__subtitle">
        <ng-content></ng-content>
      </span>
      <ng-select [loading]="loading"
                 [formControl]="control"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [placeholder]="placeholder || ''"
                 [typeahead]="typeahead"
                 [virtualScroll]="true"
                 [clearable]="false"
                 (scrollToEnd)="onScroll()"
                 bindValue="id"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control"></validation-message>
    </div>
  `,
  styles : [
    `
      .form-detail__subtitle:empty {
        padding-bottom: 0;
      }
    `
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => ProductSelectComponent),
      multi : true
    }
  ]
})
export class ProductSelectComponent extends BaseLiveSelectComponent<Product> {

  @Input() excludeProducts : string[] = [];

  dataService = this.injector.get(ProductService, null);

  constructor(
    private authService : AuthService,
    protected injector : Injector
  ) {
    super(injector);
  }

  pushItems(result : ResultList<Product>) {
    this.totalPages = result.totalPages;

    let items = result.items.filter((product : Product) => {

      if(this.excludeProducts.indexOf(product.id) < 0) {

        if(this.authService.isManager) {
          return product;
        }

        if(!product.comingSoon) {
          return product;
        }

      }

    });

    if(this.filter.page === 1) {
      this.items = items;
    } else {
      this.items = [...this.items, ...items];
    }
  }

}
