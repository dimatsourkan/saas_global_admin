import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { RoleService } from '../../../../../core/entities/role/role.service';
import { Role } from '../../../../../core/entities/role/role.model';

@Component({
  selector : 'app-role-select',
  template : `
    <div class="form-detail">
      <span class="form-detail__subtitle">
        <ng-content></ng-content>
      </span>
      <ng-select [loading]="loading"
                 [formControl]="control"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [placeholder]="placeholder || ''"
                 [clearable]="false"
                 [virtualScroll]="true"
                 (scrollToEnd)="onScroll()"
                 bindValue="id"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control"></validation-message>
    </div>
  `,
  styles : [
    `
      .form-detail__subtitle:empty {
        padding-bottom: 0;
      }
    `
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => RoleSelectComponent),
      multi : true
    }
  ]
})
export class RoleSelectComponent extends BaseLiveSelectComponent<Role> {

  dataService = this.injector.get(RoleService, null);

}
