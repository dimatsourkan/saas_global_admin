import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TenantSelectComponent } from './tenant-select.component';

describe('SelectComponent', () => {
  let component : TenantSelectComponent;
  let fixture : ComponentFixture<TenantSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations : [TenantSelectComponent]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
