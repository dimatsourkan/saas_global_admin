import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { TenantService } from '../../../../../core/entities/tenant/tenant.service';
import { Tenant } from '../../../../../core/entities/tenant/tenant.model';

@Component({
  selector : 'app-tenant-select',
  template : `
    <div class="form-detail">
      <span class="form-detail__subtitle">
        <ng-content></ng-content>
      </span>
      <ng-select [loading]="loading"
                 [placeholder]="placeholder || ''"
                 [formControl]="control"
                 [items]="items"
                 [typeahead]="typeahead"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [clearable]="false"
                 [virtualScroll]="true"
                 (scrollToEnd)="onScroll()"
                 bindValue="id"
                 bindLabel="showName">
      </ng-select>
      <validation-message [control]="control"></validation-message>
    </div>
  `,
  styles : [
    `
      .form-detail__subtitle:empty {
        padding-bottom: 0;
      }
    `
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => TenantSelectComponent),
      multi : true
    }
  ]
})
export class TenantSelectComponent extends BaseLiveSelectComponent<Tenant> {

  dataService = this.injector.get(TenantService, null);

}
