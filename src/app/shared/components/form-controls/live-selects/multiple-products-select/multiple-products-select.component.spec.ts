import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MultipleProductsSelectComponent } from './multiple-products-select.component';

describe('SelectComponent', () => {
  let component : MultipleProductsSelectComponent;
  let fixture : ComponentFixture<MultipleProductsSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations : [MultipleProductsSelectComponent]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleProductsSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
