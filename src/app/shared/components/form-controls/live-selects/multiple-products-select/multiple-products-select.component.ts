import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { ProductService } from '../../../../../core/entities/product/product.service';
import { Product } from '../../../../../core/entities/product/product.model';
import { ResultList } from '../../../../../core/models/result.model';

@Component({
  selector : 'app-multiple-products-select',
  template : `
    <div class="form-detail">
      <span class="form-detail__subtitle">
        <ng-content></ng-content>
      </span>
      <ng-select [loading]="loading"
                 [formControl]="control"
                 [items]="items"
                 [placeholder]="placeholder || ''"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [typeahead]="typeahead"
                 [virtualScroll]="true"
                 [clearable]="false"
                 (scrollToEnd)="onScroll()"
                 [multiple]="true"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control"></validation-message>
    </div>
  `,
  styles : [
    `
      .form-detail__subtitle:empty {
        padding-bottom: 0;
      }
    `
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => MultipleProductsSelectComponent),
      multi : true
    }
  ]
})
export class MultipleProductsSelectComponent extends BaseLiveSelectComponent<Product> {

  @Input() excludeId = null;

  dataService = this.injector.get(ProductService, null);

  pushItems(result : ResultList<Product>) {
    this.totalPages = result.totalPages;

    let items = result.items.filter((i) => {
      if(i.id !== this.excludeId) {
        return i;
      }
    });

    if(this.filter.page === 1) {
      this.items = items;
    } else {
      this.items = [...this.items, ...items];
    }
  }

  writeValue(value : any) {
    this.value = value || null;
  }

  ngOnInit() {
    this.items = null;
    super.ngOnInit();
  }

}
