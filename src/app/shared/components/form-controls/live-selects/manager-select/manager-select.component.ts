import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { ManagerService } from '../../../../../core/entities/manager/manager.service';
import { Manager } from '../../../../../core/entities/manager/manager.model';

@Component({
  selector : 'app-manager-select',
  template : `
    <div class="form-detail">
      <span class="form-detail__subtitle">
        <ng-content></ng-content>
      </span>
      <ng-select [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 [typeahead]="typeahead"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [virtualScroll]="true"
                 [clearable]="false"
                 (scrollToEnd)="onScroll()"
                 bindValue="id"
                 bindLabel="showName">
      </ng-select>
      <validation-message [control]="control"></validation-message>
    </div>
  `,
  styles : [
    `
      .form-detail__subtitle:empty {
        padding-bottom: 0;
      }
    `
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => ManagerSelectComponent),
      multi : true
    }
  ]
})
export class ManagerSelectComponent extends BaseLiveSelectComponent<Manager> {

  dataService = this.injector.get(ManagerService, null);

}
