import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscribersSelectComponent } from './subscribers-select.component';

describe('SelectComponent', () => {
  let component : SubscribersSelectComponent;
  let fixture : ComponentFixture<SubscribersSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations : [SubscribersSelectComponent]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscribersSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
