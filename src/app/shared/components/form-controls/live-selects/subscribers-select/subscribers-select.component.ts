import { Component, forwardRef, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import { MailingService } from '../../../../../core/entities/mailing/mailing.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector : 'app-subscribers-select',
  template : `
    <div class="form-detail">
      <span class="form-detail__subtitle">
        <ng-content></ng-content>
      </span>
      <ng-select [loading]="loading"
                 [formControl]="control"
                 [items]="anyItems"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [clearable]="false"
                 [multiple]="multiple"
                 [placeholder]="placeholder || ''"
                 [virtualScroll]="true"
                 (scrollToEnd)="onScroll()">
      </ng-select>
      <validation-message [control]="control"></validation-message>
    </div>
  `,
  styles : [
    `
      .form-detail__subtitle:empty {
        padding-bottom: 0;
      }
    `
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => SubscribersSelectComponent),
      multi : true
    }
  ]
})
export class SubscribersSelectComponent extends BaseLiveSelectComponent<any> {

  @Input() multiple = false;

  anyItems : string[] = [];

  dataService = this.injector.get(MailingService, null);

  initFilterListener() {
    this.filter.onChangeFilter$.subscribe(() => {
      this.getSubscribers().subscribe(res => {
        this.anyItems = res;
      });
    });
  }

  getSubscribers() {
    this.loading = true;
    return this.dataService.subscriptions(this.filter.filter)
      .pipe(finalize(() => this.loading = false));
  }

  writeValue(value : any) {
    this.value = value || null;
  }

}
