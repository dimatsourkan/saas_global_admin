import { Component, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { CountryService } from '../../../../../core/entities/country/country.service';
import { BaseLiveSelectComponent } from '../base-live-select.component';
import findIndex from 'lodash-es/findIndex';
import { Country } from '../../../../../core/entities/country/country.model';

@Component({
  selector : 'app-country-select',
  template : `
    <div class="form-detail">
      <span class="form-detail__subtitle" 
            [ngClass]="{ required : required }">
        <ng-content></ng-content>
      </span>
      <ng-select [loading]="loading"
                 [formControl]="control"
                 [placeholder]="placeholder || ''"
                 [items]="items"
                 (change)="onChange()"
                 (open)="onOpen()"
                 [virtualScroll]="true"
                 [clearable]="false"
                 (scrollToEnd)="onScroll()"
                 bindValue="code"
                 bindLabel="name">
      </ng-select>
      <validation-message [control]="control"></validation-message>
    </div>
  `,
  styles : [
    `
      .form-detail__subtitle:empty {
        padding-bottom: 0;
      }
    `
  ],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => CountrySelectComponent),
      multi : true
    }
  ]
})
export class CountrySelectComponent extends BaseLiveSelectComponent<Country> {

  dataService = this.injector.get(CountryService, null);

  getItemIndex(id : string) {
    return findIndex(this.items, (i : Country) => i.code === id);
  }

}
