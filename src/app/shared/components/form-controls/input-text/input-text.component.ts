import { Component, forwardRef, Input, ViewChild } from '@angular/core';
import { FormControlsComponent } from '../form-controls.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as Inputmask from 'inputmask/dist/inputmask/inputmask';
import 'inputmask/dist/inputmask/inputmask.extensions';

@Component({
  selector : 'app-input-text',
  templateUrl : './input-text.component.html',
  styleUrls : ['./input-text.component.scss'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => InputTextComponent),
      multi : true
    }
  ]
})
export class InputTextComponent extends FormControlsComponent {

  @Input() mask : string = null;

  @Input() regexp : RegExp|RegExp[] = null;

  @Input() options : any = {};

  @ViewChild('input') private input : any;

  ngAfterViewInit() {

    super.ngAfterViewInit();

    if(this.mask) {
      Inputmask(this.mask, this.options).mask(this.input.nativeElement);
    }

    if(this.options.alias) {
      Inputmask(this.options).mask(this.input.nativeElement);
    }

  }

  onKeyPress(event : any) {

    if(!this.regexp || event.key.length > 1) {
      return true;
    }

    let position = caretPosition(event.target);
    let value : string    = getFinalString(event.target.value, position.start, position.end, event.key);

    if(this.regexp instanceof RegExp) {
      if(!this.regexp.test(value)) {
        return false;
      }
    } else {

      return this.regexp.filter(regexp => {
        return regexp.test(value);
      }).length === this.regexp.length;

    }

  }

}

export function getFinalString(string : string, start : number, end : number, value : string) {
  let beforeSubStr = string.substring(0, start);
  let afterSubStr = string.substring(end, string.length);
  return beforeSubStr + value + afterSubStr;

}

export function caretPosition(input) {
  let start = input.selectionStart;
  let end = input.selectionEnd;

  return {
    start, end
  }
}
