import {
  AfterContentInit,
  Component,
  forwardRef,
  Input,
  ViewChild
} from '@angular/core';
import { FormControlsComponent } from '../form-controls.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector : 'app-select',
  templateUrl : './select.component.html',
  styleUrls : ['./select.component.scss'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => SelectComponent),
      multi : true
    }
  ]
})
export class SelectComponent extends FormControlsComponent implements AfterContentInit {

  @ViewChild('select') private select : any;
  @Input() label : string;
  @Input() parseInt : boolean = false;

  writeValue(value : any) {
    super.writeValue(value);
    this.select.nativeElement.value = value;
    this.setSelect();
  }

  ngAfterContentInit() {
    super.ngAfterViewInit();
    this.setSelect();
  }

  setSelect() {
    let option = this.select.nativeElement.querySelector(`option[value="${this.value}"]`);
    if(option) {
      option.selected = true;
    }
  }

  onChange(value : any) {
    if(this.parseInt) {
      value = parseInt(value);
    }
    this.propagateChange(value);
  }
}
