import { AfterViewInit, Component, forwardRef, Input } from '@angular/core';
import { FormControlsComponent } from '../form-controls.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { caretPosition, getFinalString } from '../input-text/input-text.component';

@Component({
  selector : 'app-tags-input',
  templateUrl : './tags-input.component.html',
  styleUrls : ['./tags-input.component.scss'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => TagsInputComponent),
      multi : true
    }
  ]
})
export class TagsInputComponent extends FormControlsComponent implements AfterViewInit {

  value : string[] = [];
  current : string = '';
  invalid = false;

  @Input() regexp : RegExp|RegExp[] = null;
  @Input() number : boolean = false;

  addTag() {
    if(this.current) {
      let current : any = this.current;
      if(this.number) {
        current = parseInt(this.current);
      }
      this.value.push(current);
      this.current = '';
      this.propagateChange(this.value);
    }
  }

  removeLastTag() {
    if(!this.current) {
      this.value.splice(this.value.length - 1, 1);
      this.propagateChange(this.value);
    }
  }

  removeByIndex(index : number) {
    this.value.splice(index, 1);
    this.propagateChange(this.value);
  }

  onKeyPress(event : any) {

    if(!this.regexp || event.key.length > 1) {
      return true;
    }

    let position = caretPosition(event.target);
    let value : string    = getFinalString(event.target.value, position.start, position.end, event.key);

    if(this.regexp instanceof RegExp) {
      if(!this.regexp.test(value)) {
        return false;
      }
    } else {

      return this.regexp.filter(regexp => {
        return regexp.test(value);
      }).length === this.regexp.length;

    }

  }

  markAsTouched() {
    this.control.markAsTouched({onlySelf : true});
    this.invalid = this.control.invalid;
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.control.valueChanges.subscribe(() => {
      this.invalid = this.control.invalid;
    });
  }

}
