import { AfterViewInit, Component, Injector, Input } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms';

export enum PLACEHOLDER_TYPE {
  MOVE = 'move-placholder',
  STATIC = 'text-indent-placholder',
}

/**
 * Прямое использование этой компоненты недопустимо, она используется только для наследования
 */
@Component({
  selector : '',
  template : ''
})
export class FormControlsComponent implements ControlValueAccessor, AfterViewInit {

  @Input()
  placeholderType = PLACEHOLDER_TYPE.STATIC;

  @Input()
  customClass = '';

  @Input()
  name = '';

  @Input()
  formControlName : string;

  @Input()
  maxlength : number;

  @Input()
  placeholder : string = '';

  @Input()
  value : any = '';

  @Input()
  disabled : boolean = false;

  @Input()
  required : boolean = false;

  control : FormControl = new FormControl('');

  get isMovedType() {
    return this.placeholderType === PLACEHOLDER_TYPE.MOVE;
  }

  get isStaticType() {
    return this.placeholderType === PLACEHOLDER_TYPE.STATIC;
  }

  constructor(protected injector : Injector) {}

  markAsTouched() {
    this.control.markAsTouched({onlySelf : true});
  }

  writeValue(value : any) {
    this.value = value || null;
  }

  setDisabledState(state : boolean = false) {
    this.disabled = state;
  }

  propagateChange = (_ : any) => {
    this.value = _;
  };

  registerOnChange(fn : any) {
    this.propagateChange = fn;
  }

  registerOnTouched() {
  }

  ngAfterViewInit() {

    const ngControl: NgControl = this.injector.get(NgControl, null);
    if (ngControl) {
      this.control = ngControl.control as FormControl;
    }

  }

  clearValue() {
    this.propagateChange(null);
  }
}
