import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {
  DateTimeAdapter,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE,
  OwlDateTimeInlineComponent
} from 'ng-pick-datetime';
import {
  MomentDateTimeAdapter
} from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';
import { moment } from 'src/app/core/helpers/moment';
import { BASE_DATE_FORMAT } from '../../../../../app.constants';
import { fromEvent, Subscription } from 'rxjs';
import { Moment } from 'moment';
import { FilterService } from '../../../../../core/services/filter.service';

const CUSTOM_FORMATS = {
  parseInput: BASE_DATE_FORMAT,
  fullPickerInput: BASE_DATE_FORMAT,
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: BASE_DATE_FORMAT,
  monthYearLabel: 'MMMM YYYY',
  dateA11yLabel: BASE_DATE_FORMAT,
  monthYearA11yLabel: BASE_DATE_FORMAT
};

@Component({
  selector : 'app-range-datepicker',
  templateUrl : './range-datepicker.component.html',
  styleUrls : ['./range-datepicker.component.scss'],
  providers : [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide : OWL_DATE_TIME_FORMATS, useValue : CUSTOM_FORMATS}
  ]
})
export class RangeDatepickerComponent implements OnChanges, OnInit, OnDestroy {

  _filter : FilterService;
  _dateFrom : Moment;
  _dateTo : Moment;
  values : Moment[];
  isOpen = false;
  subscription$ : Subscription;

  @ViewChild('picker') private picker : OwlDateTimeInlineComponent<Moment>;
  @ViewChild('container') private container : ElementRef;
  @Output() change = new EventEmitter<Moment[]>();

  @Input() public set filter(filter : FilterService) {
    this._filter = filter;
    this.dateFrom = filter.dateFrom;
    this.dateTo = filter.dateTo;
  };

  @Input()
  set dateFrom(dateFrom : string|Moment) {
    if(dateFrom) {
      this._dateFrom = new moment(dateFrom, CUSTOM_FORMATS.datePickerInput);
    } else {
      this._dateFrom = null;
    }
  }

  @Input()
  set dateTo(dateTo : string|Moment) {
    if(dateTo) {
      this._dateTo = new moment(dateTo, CUSTOM_FORMATS.datePickerInput);
    } else {
      this._dateTo = null;
    }
  }

  get dateFrom() {
    if(this._dateFrom) {
      return this._dateFrom.format(CUSTOM_FORMATS.datePickerInput);
    } else {
      return null;
    }
  }

  get dateTo() {
    if(this._dateTo) {
      return this._dateTo.format(CUSTOM_FORMATS.datePickerInput);
    } else {
      return null;
    }
  }

  onChange(date : Moment[]) {
    this._dateFrom = date[0];
    this._dateTo = date[1];
    if(this._filter) {
      this._filter.dateFrom = date[0];
      this._filter.dateTo = date[1];
    }
    this.change.emit(date);
  }

  applyDates() {
    this.onChange(this.picker.values);
    this.hidePicker();
  }

  togglePicker() {
    this.isOpen = !this.isOpen;
  }

  hidePicker() {
    this.isOpen = false;
  }

  clickOverlay(target : any) {
    if(!this.container.nativeElement.contains(target) && document.contains(target)) {
      this.hidePicker();
    }
  }

  ngOnInit() {
    this.subscription$ = fromEvent(document, 'click').subscribe((e) => {
      this.clickOverlay(e.target)
    })
  }

  ngOnChanges() {
    this.values = [this._dateFrom, this._dateTo];
  }

  ngOnDestroy() {
    if(this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }
}
