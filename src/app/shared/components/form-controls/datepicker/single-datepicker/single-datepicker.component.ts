import { AfterViewInit, Component, forwardRef, Input } from '@angular/core';
import { FormControlsComponent } from '../../form-controls.component';
import { FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import {
  MomentDateTimeAdapter
} from 'ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class';
import { BASE_DATE_FORMAT } from '../../../../../app.constants';
import { moment } from 'src/app/core/helpers/moment';
import { Moment } from 'moment';

const CUSTOM_FORMATS = {
  parseInput: BASE_DATE_FORMAT,
  fullPickerInput: BASE_DATE_FORMAT,
  datePickerInput: 'DD-MM-YYYY',
  timePickerInput: BASE_DATE_FORMAT,
  monthYearLabel: 'MMMM YYYY',
  dateA11yLabel: BASE_DATE_FORMAT,
  monthYearA11yLabel: BASE_DATE_FORMAT
};

@Component({
  selector : 'app-single-datepicker',
  templateUrl : './single-datepicker.component.html',
  styleUrls : ['./single-datepicker.component.scss'],
  providers : [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide : OWL_DATE_TIME_FORMATS, useValue : CUSTOM_FORMATS},
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => SingleDatepickerComponent),
      multi : true
    }
  ]
})
export class SingleDatepickerComponent extends FormControlsComponent implements AfterViewInit {

  @Input() format : string = BASE_DATE_FORMAT;

  privateFormControl = new FormControl();

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.privateFormControl.valueChanges.subscribe((v : Moment) => {
      this.updateWithFormat(v);
    });
  }

  updateWithFormat(v : Moment) {
    this.propagateChange(v ? v.format(this.format) : null);
  }

  writeValue(value : any) {
    this.value = value || null;
    if(this.value) {
      this.privateFormControl.patchValue(moment(value, this.format));
    }
  }
}
