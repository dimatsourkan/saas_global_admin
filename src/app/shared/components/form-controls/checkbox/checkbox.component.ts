import { Component, forwardRef, Input } from '@angular/core';
import { FormControlsComponent } from '../form-controls.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector : 'app-checkbox',
  templateUrl : './checkbox.component.html',
  styleUrls : ['./checkbox.component.scss'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => CheckboxComponent),
      multi : true
    }
  ]
})
export class CheckboxComponent extends FormControlsComponent {

  @Input() checked : false;

}
