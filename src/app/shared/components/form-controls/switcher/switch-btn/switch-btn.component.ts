import { Component, Input } from '@angular/core';

@Component({
  selector : 'app-switch-btn',
  templateUrl : './switch-btn.component.html',
  styleUrls : ['./switch-btn.component.scss']
})
export class SwitchBtnComponent {
  @Input() value : string = '';
  @Input() name : string = '';
  @Input() checked : boolean = false;
}
