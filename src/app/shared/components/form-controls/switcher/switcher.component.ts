import { AfterViewInit, Component, ElementRef, forwardRef, Input, ViewChild } from '@angular/core';
import { FormControlsComponent } from '../form-controls.component';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector : 'app-switcher',
  templateUrl : './switcher.component.html',
  styleUrls : ['./switcher.component.scss'],
  providers : [
    {
      provide : NG_VALUE_ACCESSOR,
      useExisting : forwardRef(() => SwitcherComponent),
      multi : true
    }
  ]
})
export class SwitcherComponent extends FormControlsComponent implements AfterViewInit {

  @Input() label : string = '';
  @Input() parseInt : boolean = false;
  @ViewChild('switchContainer') private switchContainer : ElementRef;

  ngAfterViewInit() {
    this.writeValue(this.value);
    this.switchContainer.nativeElement.addEventListener('change', e => {
      let value = e.target.value;
      if(this.parseInt) {
        value = parseInt(value);
      }
      this.writeValue(value);
      this.propagateChange(value);
    })
  }

  writeValue(value : any) {
    let appSwitch = this.switchContainer.nativeElement.querySelector(`input[value="${value}"]`);
    if(appSwitch) {
      appSwitch.checked = true;
    }
    this.value = value;
  }
}
