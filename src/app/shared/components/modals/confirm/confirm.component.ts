import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent {

  @Input() cancelText = 'Cancel';
  @Input() applyText = 'Apply';
  @Output() onSuccess : EventEmitter<boolean> = new EventEmitter();
  @Output() onCancel : EventEmitter<boolean> = new EventEmitter();
  @ViewChild('modal') private modal : ModalComponent;
  successCallback : Function;
  cancelCallback : Function;

  cancel() {
    this.close();
    this.onCancel.emit();
    if(this.cancelCallback) {
      this.cancelCallback();
    }
  }

  success() {
    this.onSuccess.emit();
    if(this.successCallback) {
      this.successCallback();
    }
  }

  open(successCallback ? : Function, cancelCallback ? : Function) {
    this.successCallback = successCallback;
    this.cancelCallback = cancelCallback;
    this.modal.open();
  }

  close() {
    this.modal.close();
  }

}
