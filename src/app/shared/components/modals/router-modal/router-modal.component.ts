import { Component, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from '../modal/modal.component';
import { PopupService } from '../../../../popups/popup.service';

@Component({
  selector : 'app-router-modal',
  templateUrl : './router-modal.component.html',
  styleUrls : ['./router-modal.component.scss']
})
export class RouterModalComponent {

  @Input() size : 'sm'|'md'|'lg' = 'lg';

  @ViewChild('modal') private modal : ModalComponent;

  constructor(
    private router : Router,
    private popupService : PopupService
  ) {}

  open() {
    this.modal.open();
  }

  close() {
    this.modal.close();
  }

  onClose() {
    this.popupService.closeRoutingPopup();
  }

}
