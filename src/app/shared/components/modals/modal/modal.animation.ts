import { animate, query, style, transition, trigger } from '@angular/animations';

export const modalFadeAnimation = trigger('modalFadeAnimation', [

  transition( '* => *', [
    query('.modal__dialog:leave',
      [
        style({ opacity: 1, transform: 'translateY(0)' }),
        animate('0.2s', style({ opacity: 0, transform: 'translateY(20px)' })),
      ],
      { optional: true }
    ),
    query('.modal__dialog:enter',
      [
        style({ opacity: 0, transform: 'translateY(20px)' }),
        animate('0.2s', style({ opacity: 1, transform: 'translateY(0)' })),
      ],
      { optional: true }
    )

  ])

]);
