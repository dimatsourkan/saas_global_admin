import { Component, ElementRef, EventEmitter, Input, OnDestroy, Output, ViewChild } from '@angular/core';
import { modalFadeAnimation } from './modal.animation';
import { scrollbarWidth } from '../../../../core/helpers/helpers';

@Component({
  selector : 'app-modal',
  templateUrl : './modal.component.html',
  styleUrls : ['./modal.component.scss'],
  animations : [modalFadeAnimation]
})
export class ModalComponent implements OnDestroy {

  @Input() size : 'sm'|'msm'|'md'|'lg' = 'lg';
  @Input() closable = true;
  @Input() customClass = '';
  @Input() header = true;
  @Input() footer = true;

  @Output('onClose') private onCLose = new EventEmitter<any>();

  @ViewChild('modal') private modal : ElementRef;
  @ViewChild('modalMove') private modalMove : ElementRef;
  @ViewChild('modalContainer') private modalContainer : ElementRef;

  _showed = false;

  _animationInProgress = false;

  /**
   * Костыли для правильного отображения, что бы попап не прыгал в стороны
   */
  private setHidden() {
    if(this.modal) {
      this.modal.nativeElement.classList.add('overflow-hidden');
    }
  }

  private setVisible() {
    if(this.modal) {
      this.modal.nativeElement.classList.remove('overflow-hidden');
    }
  }

  /**
   * Добавление паддинга к странице что бы не прыгал контент
   */
  private addBodyStyles() {
    let body = document.querySelector('body');
        body.classList.add('open-modal');
        body.style['padding-right'] = `${scrollbarWidth}px`;
  }

  private removaBodyStyles() {
    if(!this.hasModalsOpen) {
      let body = document.querySelector('body');
      body.classList.remove('open-modal');
      body.style['padding-right'] = null;
    }
  }

  /**
   * Перемежение попапа в body и обратно
   */
  private moveToBody() {
    document.querySelector('body').appendChild(this.modalMove.nativeElement);
  }

  private moveBack() {
    this.modalContainer.nativeElement.appendChild(this.modalMove.nativeElement);
  }

  _animationStart(event) {
    if(event.fromState === true) {
      this._animationInProgress = true;
      this.setHidden();
    }
  }

  _animationDone(event) {
    this.setVisible();
    if(event.fromState === true) {
      this.moveBack();
      this.onCLose.emit();
    }
  }

  open() {
    this._showed = true;
    this.moveToBody();
    this.addBodyStyles();

  }

  close() {
    if(this.modal.nativeElement) {
      this.modal.nativeElement.classList.add('overflow-hidden');
    }
    this._showed = false;
    this.removaBodyStyles();
  }

  clickOverlay(target : HTMLElement) {
    if(target.classList[0] === 'modal__dialog') {
      if(!this.closable) {
        return false;
      }
      this.close();
    }
  }

  get hasModalsOpen() {
    return document.querySelectorAll('body > .modal-container-move').length > 1;
  }

  ngOnDestroy() {
    this.moveBack();
    this.removaBodyStyles();
  }

}
