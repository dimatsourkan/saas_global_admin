import { AfterViewInit, Component, ElementRef, Input, OnDestroy, ViewChild } from '@angular/core';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector : 'app-accordion',
  templateUrl : './accordion.component.html',
  styleUrls : ['./accordion.component.scss']
})
export class AccordionComponent implements AfterViewInit, OnDestroy {

  @ViewChild('accordion') private accordion : ElementRef;
  @Input() closeOther = true;
  resize$ : Subscription;

  ngAfterViewInit() {

    let closeOther = this.closeOther;
    let accordionItems = this.accordion.nativeElement.querySelectorAll('.accordion__panel');

    accordionItems.forEach(function (item) {

      if(item.classList.contains('active')) {
        setAccordionBodyHeight(item.querySelector('.accordion__body'));
      }

      item.querySelector('.accordion__head').addEventListener('click', function () {

        let panel = this.nextElementSibling;

        if(this.parentElement.classList.contains('active')) {

          this.parentElement.classList.remove('active');
          panel.style.maxHeight = null;

        } else {

          if(closeOther) {

            accordionItems.forEach(function (elem) {
              elem.classList.remove('active');
              elem.querySelector('.accordion__body').style.maxHeight = null;
            });

          }

          this.parentElement.classList.add('active');
          panel.style.maxHeight = panel.scrollHeight + 'px';

        }
      });
    });


    this.resize$ = fromEvent(window, 'resize')
      .pipe(debounceTime(250)).subscribe(() => {

      accordionItems.forEach((item) => {

        if(item.classList.contains('active')) {
          setAccordionBodyHeight(item.querySelector('.accordion__body'));
        }
      });
    });

  }

  ngOnDestroy() {
    this.resize$.unsubscribe();
  }

}

function setAccordionBodyHeight(item : HTMLDivElement) {
  item.style.maxHeight = item.scrollHeight + 'px';
}
