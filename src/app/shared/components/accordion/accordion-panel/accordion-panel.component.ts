import { Component, Input } from '@angular/core';

@Component({
  selector : 'app-accordion-panel',
  templateUrl : './accordion-panel.component.html',
  styleUrls : ['./accordion-panel.component.scss']
})
export class AccordionPanelComponent {

  @Input() active = false;

}
