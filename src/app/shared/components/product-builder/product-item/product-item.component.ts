import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { InvoiceItem } from '../../../../core/entities/invoice/invoice.model';
import { ProductBuilderService } from '../product-builder.service';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { debounceTime, finalize } from 'rxjs/operators';
import { ProductService } from '../../../../core/entities/product/product.service';
import { Product } from '../../../../core/entities/product/product.model';
import findIndex from 'lodash-es/findIndex';
import { ProductPlan } from '../../../../core/entities/product-plan/product-plan.model';
import { ProductLimit } from '../../../../core/entities/product/product-limit.model';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnChanges {

  loading = false;
  form : FormGroup;
  @Input() item : InvoiceItem;
  @Input() excludeProducts : string[] = [];
  @Input() withoutOffers = false;
  @Output() private onFormChange = new EventEmitter<FormGroup>();
  @Output() private onRemoveCLick = new EventEmitter();

  constructor(
    private productBuilderService : ProductBuilderService,
    private productService : ProductService,
    private formBuilder : FormBuilder
  ) {

    this.form = this.formBuilder.group({
      product : [],
      productId : [],
      productPlan : [{value : '', disabled : true}],
      extraLimits : this.formBuilder.array([
        this.formBuilder.group({
          value : [],
          name : []
        })
      ])
    });

    this.form.get('product').valueChanges.subscribe(value => {
      this.form.get('productPlan').enable();
    });
  }

  emitChange() {
    this.onFormChange.emit(this.form);
  }

  ngOnChanges(changes : SimpleChanges) {

    if(changes.excludeProducts.currentValue === this.excludeProducts && !changes.excludeProducts.firstChange) {
      return;
    }

    if(this.item.product.id) {
      this.form.patchValue({ productId : this.item.product.id });
      this.form.patchValue({ productPlan : this.item.productPlan });
      this.setProduct(this.form.get('productId').value);
      if(this.item.extraLimits && this.item.extraLimits.length) {
        this.productExtraForm.patchValue({
          value : this.item.extraLimits[0].value
        });
      }
    }
    this.emitChange();
  }

  setProduct(id : string) {
    this.loading = true;
    this.productExtraForm.patchValue({ value : null });
    this.productService.get(id)
      .pipe(finalize((() => this.loading = false)))
      .subscribe(res => {
        this.patchForm(res);
        this.emitChange();
      });
  }

  private patchForm(product : Product) {

    const plan = this.form.get('productPlan').value;
    const planId = plan ? plan.id : null;
    const extraLimits = this.productExtraForm;

    extraLimits.patchValue({ name : product.productLimits[0].name });

    let planIndex = -1;
    if(planId) {
      planIndex = findIndex(product.productPlans, (i : ProductPlan) => {
        return i.id === planId;
      });
    }

    this.form.patchValue({ product : product });
    if(planIndex >= 0) {
      this.form.patchValue({ productPlan : product.productPlans[planIndex] });
    } else {
      this.form.patchValue({ productPlan : null });
    }

  }

  onClear() {
    this.productExtraForm.get('value').setValue(null);
  }

  removeItem() {
    this.onRemoveCLick.emit();
  }

  get product() : Product {
    return this.form.get('product').value;
  }

  get productExtraForm() : AbstractControl {
    return this.form.get('extraLimits').get([0]);
  }

  get productLimit() : ProductLimit {
    if(this.form.get('product').value) {
      return this.form.get('product').value.productLimits[0];
    } else {
      return null;
    }

  }

  get productPrice() {
    return this.productBuilderService.productPrice(this.form.getRawValue() as any);
  }
}
