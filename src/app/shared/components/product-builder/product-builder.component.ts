import { AfterViewInit, Component, EventEmitter, Input, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { ProjectService } from '../../../core/entities/project/project.service';
import { ProductService } from '../../../core/entities/product/product.service';
import { InvoiceItem } from '../../../core/entities/invoice/invoice.model';
import { ValidatorService } from '../../validation/validation.service';
import { ProductBuilderService } from './product-builder.service';
import { classToClass, plainToClass } from 'class-transformer';

@Component({
  selector : 'app-product-builder',
  templateUrl : './product-builder.component.html',
  styleUrls : ['./product-builder.component.scss']
})
export class ProductBuilderComponent implements AfterViewInit {

  @Input() withoutOffers = false;

  loading = false;
  form : FormGroup[];
  timeout = null;
  items : InvoiceItem[] = [];
  excludeProducts : string[] = [];

  @Output() private onFormChange = new EventEmitter();

  constructor(
    private formBuilder : FormBuilder,
    private projectService : ProjectService,
    private productService : ProductService,
    private validatorService : ValidatorService,
    private productBuilderService : ProductBuilderService
  ) {

    this.form = [];
  }

  emitChange() {
    this.onFormChange.emit(this.form);
    this.addNewProductItemIfAllIsFilled();
  }

  ngAfterViewInit() {
    this.emitChange();
  }

  replaceForm(form : FormGroup, index : number) {
    this.form.splice(index, 1, form);
    this.emitChange();
  }

  removeFormItem(index : number) {
    this.items.splice(index, 1);
    this.form.splice(index, 1);
    this.emitChange();
  }

  addNewProductItemIfAllIsFilled() {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      let emptyFieldsLength = 0;
      this.form.forEach(f => {
        if(!f.getRawValue().productId) {
          emptyFieldsLength++;
        }
      });
      if(this.form.length >= this.items.length && emptyFieldsLength < 1) {
        this.items.push(new InvoiceItem());
      }
      this.setExcludeProducts();
    }, 10);
  }

  setExcludeProducts() {
    this.excludeProducts = this.form
      .map((form : FormGroup) => {
        return plainToClass(InvoiceItem, form.getRawValue() as Object).product;
      })
      .filter(product => {
        if(product) {
          return product;
        }
      })
      .map(product => {
        return product.id;
      });
  }

  /**
   * Считает стоимость одного продукта
   * @param item
   */
  productPrice(item : AbstractControl) {
    return this.productBuilderService.productPrice(item.value);
  }

  /**
   * Добавление данных в форму из существующего инвойса
   * @param items
   */
  patchForm(items : InvoiceItem[]) {
    this.items = classToClass(items);
  }

  /**
   * Очистка формы
   */
  resetForm() {
    this.form = [];
    this.items = [];
    this.addNewProductItemIfAllIsFilled();
  }

}
