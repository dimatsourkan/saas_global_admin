import { Injectable } from '@angular/core';
import { InvoiceItem } from '../../../core/entities/invoice/invoice.model';
import { ProjectRequest, ProjectRequestItem } from '../../../core/entities/project-request/project-request.model';
import { ProductPlan } from '../../../core/entities/product-plan/product-plan.model';

class ProductBuilderPrice {
  taxPrice : string = '0.00';
  totalPrice : string = '0.00';
  grandTotal : string = '0.00';
}

function toFixed2(number : number) {
  return `${number * 100 / 100}`;
}

@Injectable()
export class ProductBuilderService {

  private _taxPercent = 0;

  set taxPercent(taxPercent : number) {
    this._taxPercent = taxPercent;
  }

  productPrice(item : InvoiceItem|ProjectRequestItem) {

    let plan : ProductPlan = item.productPlan;
    let planPrice : number = plan && plan.price ? Number(plan.price.amount) : 0;
    let extra : number = item.extraLimits[0].value || 0;
    let extraPrice : number = 0;
    if(plan && plan.limits && plan.limits.length) {
      extraPrice = Number(plan.limits[0].extraUnitPrice.amount);
    }
    return toFixed2(planPrice + extra * extraPrice);

  }

  filterEmptyExtralimits(i : InvoiceItem[]|ProjectRequestItem[]) {
    const items : any = i;
    items.forEach(item => {
      item.extraLimits = item.extraLimits.filter(limit => {
        if(limit.value) {
          return limit;
        }
      });
    });
    return items;
  }

}
