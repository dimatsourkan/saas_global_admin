import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss']
})
export class BadgeComponent {

  @Input() name : string;
  @Input() value : string;
  @Output() remove = new EventEmitter();

  _remove() {
    this.remove.emit();
  }

}
