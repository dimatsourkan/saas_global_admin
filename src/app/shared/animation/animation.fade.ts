import { animate, animateChild, group, query, style, transition, trigger } from '@angular/animations';

const slideTransition = transition('* <=> *', [
  /* order */
  animateChild(null),
  query('router-outlet + *:enter, router-outlet ~ *:leave',
    style({ position: 'absolute', width : '100%' })
    , { optional: true }),
  group([  // block executes in parallel
    query('router-outlet + :enter', [
      style({ transform: 'translateX(50px)' , opacity : 0}),
      animate('.3s ease-in-out', style({ transform: 'translateX(0)', opacity: 1 }))
    ], { optional: true }),
    query('router-outlet ~ *:leave', [
      style({ transform: 'translateX(0)', opacity: 1 }),
      animate('.3s ease-in-out', style({ transform: 'translateX(-50px)', opacity: 0 }))
    ], { optional: true }),
  ])
]);

const fadeTransition = transition('* <=> *', [
  /* order */
  animateChild(null),
  query('router-outlet + *:enter, router-outlet ~ *:leave',
    style({ position: 'absolute', width : '100%' })
    , { optional: true }),
  group([  // block executes in parallel
    query('router-outlet + :enter', [
      style({ transform: 'scale(0.95)' , opacity : 0}),
      animate('.2s ease-in-out', style({ transform: 'scale(1)', opacity: 1 }))
    ], { optional: true }),
    query('router-outlet ~ *:leave', [
      style({ transform: 'scale(1)', opacity: 1 }),
      animate('.2s ease-in-out', style({ transform: 'scale(1.05)', opacity: 0 }))
    ], { optional: true }),
  ])
]);

export const routerTransition = trigger('routerTransition', [
  slideTransition
]);
