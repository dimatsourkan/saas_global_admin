import { AfterViewInit, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../core/services/auth.service';
import { finalize } from 'rxjs/operators';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { HOME_ROUTE } from '../../../app.constants';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements AfterViewInit {

  token : string;
  loading : boolean = true;

  constructor(
    private notificationsService : NotificationsService,
    private activatedRoute : ActivatedRoute,
    private authService : AuthService,
    private router : Router
  ) {
    this.token = this.activatedRoute.snapshot.paramMap.get('token');
  }

  ngAfterViewInit() {
    this.authService.confirmEmailTenant(this.token)
      .pipe(finalize(() => {
        this.router.navigate([HOME_ROUTE]);
      }))
      .subscribe(() => {
        this.notificationsService.success('Email confirmed');
      });
  }

}
