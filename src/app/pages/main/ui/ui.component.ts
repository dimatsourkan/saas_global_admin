import { Component, ViewChild } from '@angular/core';
import { PLACEHOLDER_TYPE } from '../../../shared/components/form-controls/form-controls.component';
import { Pagination } from '../../../core/models/pagination.model';
import { ConfirmComponent } from '../../../shared/components/modals/confirm/confirm.component';
import { REGEXPS } from '../../../app.constants';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';

@Component({
  selector: 'app-ui',
  templateUrl: './ui.component.html',
  styleUrls: ['./ui.component.scss']
})
export class UiComponent {

  loading = false;

  @ViewChild('confirm') private confirm : ConfirmComponent;

  tags : string[] = [];

  pagination = Pagination.plainToClass({
    page : 10,
    totalPages : 50
  });

  PLACEHOLDER_TYPE = PLACEHOLDER_TYPE;
  REGEXPS = REGEXPS;

  constructor(public notificationsService : NotificationsService) {

  }

  toggleLoading() {
    this.loading = !this.loading;
  }

  onSearch(value) {
    alert(`Search text : ${value}`);
  }

  confirmOpen() {
    this.confirm.open(
      () => { alert('Confirmed'); this.confirm.close() },
      () => alert('Cancel')
    )
  }

  changePage(pagination : Pagination<any>) {
    this.pagination = pagination.clone();
  }

}
