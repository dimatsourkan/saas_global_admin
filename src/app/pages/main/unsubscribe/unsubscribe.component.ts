import { AfterViewInit, Component } from '@angular/core';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { ActivatedRoute, Router } from '@angular/router';
import { finalize } from 'rxjs/operators';
import { TenantService } from '../../../core/entities/tenant/tenant.service';
import { HOME_ROUTE } from '../../../app.constants';

@Component({
  selector: 'app-unsubscribe',
  templateUrl: './unsubscribe.component.html',
  styleUrls: ['./unsubscribe.component.scss']
})
export class UnsubscribeComponent implements AfterViewInit {

  token : string;
  groupId : string;
  loading : boolean = true;

  constructor(
    private notificationsService : NotificationsService,
    private activatedRoute : ActivatedRoute,
    private tenantService : TenantService,
    private router : Router
  ) {
    this.token = this.activatedRoute.snapshot.paramMap.get('token');
    this.groupId = this.activatedRoute.snapshot.paramMap.get('groupId');
  }

  ngAfterViewInit() {
    this.getRequestMethod()
      .pipe(finalize(() => {
        this.router.navigate([HOME_ROUTE]);
      }))
      .subscribe(() => {
        this.notificationsService.success('Unsubscribe confirmed');
      });
  }

  getRequestMethod() {
    return this.groupId ?
      this.tenantService.unsubscribeGroup(this.token, this.groupId) :
      this.tenantService.unsubscribe(this.token);
  }

}
