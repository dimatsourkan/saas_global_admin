import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { UiComponent } from './ui/ui.component';
import { SharedModule } from '../../shared/shared.module';
import { ConfirmEmailComponent } from './confirm-email/confirm-email.component';
import { UnsubscribeComponent } from './unsubscribe/unsubscribe.component';

@NgModule({
  imports : [
    CommonModule,
    RouterModule,
    SharedModule,
    TranslateModule
  ],
  declarations : [HomeComponent, UiComponent, ConfirmEmailComponent, UnsubscribeComponent]
})
export class MainModule {
}
