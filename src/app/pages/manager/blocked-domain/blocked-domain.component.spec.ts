import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockedDomainComponent } from './blocked-domain.component';

describe('BlockedDomainComponent', () => {
  let component: BlockedDomainComponent;
  let fixture: ComponentFixture<BlockedDomainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockedDomainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockedDomainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
