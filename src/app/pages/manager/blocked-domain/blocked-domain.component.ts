import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ConfirmComponent } from '../../../shared/components/modals/confirm/confirm.component';
import { BlockedMailService } from '../../../core/entities/blocked-mail/blocked-mail.service';
import { FilterService } from '../../../core/services/filter.service';
import { finalize } from 'rxjs/operators';
import { merge, Subscription } from 'rxjs';
import { BlockedMail } from '../../../core/entities/blocked-mail/blocked-mail.model';
import { DataStore } from '../../../core/data-store/data-store.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { Helpers } from '../../../core/helpers/helpers';

@Component({
  selector : 'app-blocked-domain',
  templateUrl : './blocked-domain.component.html',
  styleUrls : ['./blocked-domain.component.scss'],
  encapsulation : ViewEncapsulation.None
})
export class BlockedDomainComponent implements OnInit {

  @ViewChild('confirm') private confirm : ConfirmComponent;
  list$ = DataStore.blockedMail.list.asObservable$;
  subscription$ : Subscription;
  loading : boolean = false;
  filter : FilterService;
  form : FormGroup;
  helpers : Helpers = new Helpers();

  constructor(
    private blockedMailService : BlockedMailService,
    private validatorService : ValidatorService
  ) {
    this.filter = (new FilterService()).init();
    this.filter.limit = 100;
    this.form = new FormGroup({
      domain : new FormControl('')
    });
  }

  ngOnInit() {
    this.getDomains();
    this.subscription$ = merge(
      this.filter.onChangeFilter$
    ).subscribe(() => this.getDomains());
  }

  getDomains() {
    this.loading = true;
    this.blockedMailService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  removeDomain(id : string) {
    this.confirm.open(() => {
      this.loading = true;
      this.confirm.close();
      this.blockedMailService.delete(id)
        .pipe(finalize(() => this.loading = false))
        .subscribe();
    });
  }

  addDomain() {
    this.loading = true;
    this.blockedMailService.save(BlockedMail.plainToClass(this.form.value))
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        () => this.form.reset(),
        err => this.validatorService.addErrorToForm(this.form, err)
      );
  }

}
