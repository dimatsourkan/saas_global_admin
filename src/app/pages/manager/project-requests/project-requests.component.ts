import { Component, OnDestroy, OnInit } from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { DataStore } from '../../../core/data-store/data-store.service';
import { FilterService } from '../../../core/services/filter.service';
import { PopupService } from '../../../popups/popup.service';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { ProductPlanService } from '../../../core/entities/product-plan/product-plan.service';
import { Project } from '../../../core/entities/project/project.model';
import { ProjectRequestService } from '../../../core/entities/project-request/project-request.service';
import { ProjectRequest } from '../../../core/entities/project-request/project-request.model';
import { PROJECT_REQUEST_STATUS } from '../../../core/entities/project-request/project-request.enum';

@Component({
  selector: 'app-products',
  templateUrl: './project-requests.component.html',
  styleUrls: ['./project-requests.component.scss']
})
export class ProjectRequestsComponent implements OnDestroy, OnInit {

  list$ = DataStore.projectRequest.list.asObservable$;
  subscription$ : Subscription;
  loading : boolean = false;
  filter : FilterService;

  constructor(
    private projectRequestService : ProjectRequestService,
    private productPlanService : ProductPlanService,
    private activatedRoute : ActivatedRoute,
    private popupService : PopupService
  ) {
    this.filter = (new FilterService(true)).init();
  }

  ngOnInit() {
    this.getProjects();
    this.subscription$ = merge(
      this.filter.onChangeFilter$,
    ).subscribe(() => this.getProjects());
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getProjects() {
    this.loading = true;
    this.projectRequestService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  approveRequest(project : Project) {
    this.loading = true;
    this.projectRequestService.approve(project.id)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => {
        project.status = PROJECT_REQUEST_STATUS.ACTIVATED;
        DataStore.projectRequest.onUpdate.emit();
      });
  }

  declineRequest(project : Project) {
    this.loading = true;
    this.projectRequestService.decline(project.id)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => {
        project.status = PROJECT_REQUEST_STATUS.DEACTIVATED;
        DataStore.projectRequest.onUpdate.emit();
      });
  }

  makeInvoice(request : ProjectRequest) {
    this.loading = true;
    this.projectRequestService.makeInvoice(request.id)
      .pipe(finalize(() => this.loading = false))
      .subscribe((res : any) => {
        this.popupService.showInvoiceInfo(res.id);
      });
  }

  showProjectRequestInfo(id : string) {
    this.popupService.showProjectRequestInfo(id);
  }

}

