import { Component, OnDestroy, OnInit } from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { DataStore } from '../../../core/data-store/data-store.service';
import { FilterService } from '../../../core/services/filter.service';
import { PopupService } from '../../../popups/popup.service';
import { finalize } from 'rxjs/operators';
import { ProductPlanService } from '../../../core/entities/product-plan/product-plan.service';
import { RoleService } from '../../../core/entities/role/role.service';

@Component({
  selector: 'app-products',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnDestroy, OnInit {

  list$ = DataStore.role.list.asObservable$;
  subscription$ : Subscription;
  loading : boolean = false;
  filter : FilterService;

  constructor(
    private productPlanService : ProductPlanService,
    private roleService : RoleService,
    private popupService : PopupService
  ) {
    this.filter = (new FilterService(true)).init();
  }

  ngOnInit() {
    this.getRoles()
    this.subscription$ = merge(
      this.filter.onChangeFilter$,
      DataStore.role.onUpdate,
      DataStore.role.onCreate,
    ).subscribe(() => this.getRoles());
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getRoles() {
    this.loading = true;
    this.roleService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  showRoleInfo(id : string) {
    this.popupService.showRoleInfo(id);
  }

  showRoleNew() {
    this.popupService.showRoleNew();
  }

  showRoleEdit(id : string) {
    this.popupService.showRoleEdit(id);
  }
}

