import { Component, OnDestroy, OnInit } from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { DataStore } from '../../../core/data-store/data-store.service';
import { FilterService } from '../../../core/services/filter.service';
import { PopupService } from '../../../popups/popup.service';
import { finalize } from 'rxjs/operators';
import { ManagerService } from '../../../core/entities/manager/manager.service';
import { PERMISSIONS } from '../../../core/entities/permission/permissions.list';

@Component({
  selector: 'app-projects',
  templateUrl: './managers.component.html',
  styleUrls: ['./managers.component.scss']
})
export class ManagersComponent implements OnDestroy, OnInit {

  PERMISSIONS = PERMISSIONS;
  list$ = DataStore.manager.list.asObservable$;
  subscription$ : Subscription;
  loading : boolean = false;
  filter : FilterService;

  constructor(
    private managerService : ManagerService,
    private popupService : PopupService
  ) {
    this.filter = (new FilterService(true)).init();
  }

  ngOnInit() {
    this.getManagers();
    this.subscription$ = merge(
      this.filter.onChangeFilter$,
      DataStore.manager.onUpdate,
      DataStore.manager.onCreate,
    ).subscribe(() => this.getManagers())
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getManagers() {
    this.loading = true;
    this.managerService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  showManagerInfo(id : string) {
    this.popupService.showManagerInfo(id);
  }

  showManagerNew() {
    this.popupService.showManagerNew();
  }

  showManagerEdit(id : string) {
    this.popupService.showManagerEdit(id);
  }

  itsMe(id : string) {
    return DataStore.isMine(id);
  }

}

