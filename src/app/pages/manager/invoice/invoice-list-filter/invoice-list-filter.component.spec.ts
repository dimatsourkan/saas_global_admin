import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceListFilterComponent } from './invoice-list-filter.component';

describe('InvoiceListFilterComponent', () => {
  let component: InvoiceListFilterComponent;
  let fixture: ComponentFixture<InvoiceListFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoiceListFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceListFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
