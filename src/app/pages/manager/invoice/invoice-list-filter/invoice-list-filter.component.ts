import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import { InvoiceFilter } from '../../../../core/entities/invoice/invoice-filter.service';
import { INVOICE_TYPE } from '../../../../core/entities/invoice/invoice.enum';
import { Subscription } from 'rxjs';
import { DataStore } from '../../../../core/data-store/data-store.service';
import { TenantService } from '../../../../core/entities/tenant/tenant.service';
import { ManagerService } from '../../../../core/entities/manager/manager.service';
import { ProjectService } from '../../../../core/entities/project/project.service';

@Component({
  selector: 'app-invoice-list-filter',
  templateUrl: './invoice-list-filter.component.html',
  styleUrls: ['./invoice-list-filter.component.scss']
})
export class InvoiceListFilterComponent implements OnChanges {

  @Input() filter : InvoiceFilter;
  @Output() filterUpdate = new EventEmitter();
  $filterSubscriber : Subscription;
  INVOICE_TYPE = INVOICE_TYPE;
  tenant$ = DataStore.tenant.item.asObservable$;
  manager$ = DataStore.manager.item.asObservable$;
  project$ = DataStore.project.item.asObservable$;
  statuses = {
    1 : 'New',
    2 : 'Pending',
    3 : 'Paid',
    4 : 'Unpaid',
    5 : 'Canceled',
  };

  constructor(
    private tenantService : TenantService,
    private managerService : ManagerService,
    private projectService : ProjectService,
  ) {

  }

  clearFilterItem(items : number[]) {

    items.forEach(i => {
      this.filter[i] = null;
    });

    this.filterUpdate.emit();
  }

  getFilterStatuses() {
    return this.filter.statuses.map(i => {
      return this.statuses[i];
    }).join(', ');
  }

  ngOnChanges() {

    if(this.$filterSubscriber) {
      this.$filterSubscriber.unsubscribe();
    }

    this.$filterSubscriber = this.filter.onChangeFilter$.subscribe(() => {
      if(this.filter.tenantId) {
        this.tenantService.get(this.filter.tenantId).subscribe();
      }
      if(this.filter.managerId) {
        this.managerService.get(this.filter.managerId).subscribe();
      }
      if(this.filter.projectId) {
        this.projectService.get(this.filter.projectId).subscribe();
      }
    });
  }
}
