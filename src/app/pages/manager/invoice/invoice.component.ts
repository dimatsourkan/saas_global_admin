import { Component, ViewChild } from '@angular/core';
import { PopupService } from '../../../popups/popup.service';
import { DataStore } from '../../../core/data-store/data-store.service';
import { merge, Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { InvoiceService } from '../../../core/entities/invoice/invoice.service';
import { InvoiceFilter } from '../../../core/entities/invoice/invoice-filter.service';
import * as moment from 'moment';
import { InvoicePopupFilterComponent } from './invoice-popup-filter/invoice-popup-filter.component';
import { Invoice } from '../../../core/entities/invoice/invoice.model';

@Component({
  selector : 'app-billing',
  templateUrl : './invoice.component.html',
  styleUrls : ['./invoice.component.scss']
})
export class InvoiceComponent {

  @ViewChild('filterModal') private filterModal : InvoicePopupFilterComponent;
  list$ = DataStore.invoice.list.asObservable$;
  subscription$ : Subscription;
  httpSubscription$ : Subscription;
  loading : boolean = false;
  filter : InvoiceFilter;


  constructor(
    private invoiceService : InvoiceService,
    private popupService : PopupService
  ) {
    this.filter = (new InvoiceFilter(true)).init();

    if(!this.filter.dateFrom || !this.filter.dateTo) {
      this.filter.dateFrom = moment().subtract(7, 'days');
      this.filter.dateTo = moment();
      this.filter.cancelCurrentChangeEvent();
    }
  }

  ngOnInit() {
    this.getInvoices();
    this.subscription$ = merge(
      this.filter.onChangeFilter$,
      DataStore.invoice.onUpdate,
      DataStore.invoice.onCreate,
    ).subscribe(() => this.getInvoices())
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getInvoices() {
    this.loading = true;
    if(this.httpSubscription$) {
      this.httpSubscription$.unsubscribe();
    }
    this.httpSubscription$ = this.invoiceService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  changeStatus(invoice : Invoice, status : number) {
    this.loading = true;
    invoice.status = status;
    this.invoiceService.changeStatus(invoice, status)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  showNewBilling() {
    this.popupService.showInvoiceNew();
  }

  showBilling(id : string) {
    this.popupService.showInvoiceInfo(id);
  }

  showBillingEdit(id : string) {
    this.popupService.showInvoiceEdit(id);
  }

  filterUpdate() {
    this.filterModal.updateFilter(this.filter);
  }

  applyFilter(filter : InvoiceFilter) {
    this.filter.update(filter);
    this.filter.emitChange();
  }



}
