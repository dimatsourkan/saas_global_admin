import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Invoice } from '../../../../core/entities/invoice/invoice.model';
import { INVOICE_STATUS } from '../../../../core/entities/invoice/invoice.enum';

@Component({
  selector: 'app-status-dropdown',
  templateUrl: './status-dropdown.component.html',
  styleUrls: ['./status-dropdown.component.scss']
})
export class StatusDropdownComponent {
  @Output() change = new EventEmitter<number>();
  INVOICE_STATUS = INVOICE_STATUS;
  @Input() invoice : Invoice;
  showed = false;

  toggle() {
    this.showed = !this.showed;
  }

  hide() {
    this.showed = false;
  }

  toChange(status : number) {
    this.change.emit(status);
    this.hide();
  }
}
