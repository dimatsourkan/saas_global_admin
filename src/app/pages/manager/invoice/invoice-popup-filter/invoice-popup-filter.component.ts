import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  ViewChild
} from '@angular/core';
import { ModalComponent } from '../../../../shared/components/modals/modal/modal.component';
import { PLACEHOLDER_TYPE } from '../../../../shared/components/form-controls/form-controls.component';
import { InvoiceFilter } from '../../../../core/entities/invoice/invoice-filter.service';
import { Subscription } from 'rxjs';
import { INVOICE_STATUS, INVOICE_TYPE } from '../../../../core/entities/invoice/invoice.enum';

@Component({
  selector: 'app-billing-filter',
  templateUrl: './invoice-popup-filter.component.html',
  styleUrls: ['./invoice-popup-filter.component.scss']
})
export class InvoicePopupFilterComponent implements OnDestroy, OnChanges {

  @ViewChild('filterModal') private filterModal : ModalComponent;
  @Output() onChange = new EventEmitter<InvoiceFilter>();
  @Input() filter = new InvoiceFilter();

  readonly INVOICE_TYPE = INVOICE_TYPE;
  readonly INVOICE_STATUS = INVOICE_STATUS;
  readonly PLACEHOLDER_TYPE = PLACEHOLDER_TYPE;

  sliderOptions = {
    amountFrom : 0,
    amountTo : 99999.99,
    min : 0,
    max : 99999.99,
  };

  _filter = new InvoiceFilter();

  subscription$ : Subscription;

  ngOnDestroy() {
    if(this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  ngOnChanges() {
    this._filter.update(this.filter);
  }

  amountStartChange(start : string|number) {
    this._filter.amountFrom = Number(start);

    if(this._filter.amountFrom > this.sliderOptions.max) {
      return this.amountStartChange(this.sliderOptions.max);
    }

    this.sliderOptions.amountFrom = this._filter.amountFrom;
  }

  amountEndChange(end : string|number) {
    this._filter.amountTo = Number(end);

    if(this._filter.amountTo > this.sliderOptions.max) {
      return this.amountEndChange(this.sliderOptions.max);
    }

    this.sliderOptions.amountTo = this._filter.amountTo;
  }

  onChangeAmount(event : [string, string]) {
    this._filter.amountFrom = Number(event[0]);
    this._filter.amountTo = Number(event[1]);
  }

  applyFilter() {
    this.close();
    this.onChange.emit(this._filter);
  }

  open() {
    this.filterModal.open();
  }

  close() {
    this.filterModal.close();
  }

  updateFilter(filter : InvoiceFilter) {
    this._filter.update(filter);
  }

}
