import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicePopupFilterComponent } from './invoice-popup-filter.component';

describe('BillingFilterComponent', () => {
  let component: InvoicePopupFilterComponent;
  let fixture: ComponentFixture<InvoicePopupFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicePopupFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicePopupFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
