import { Component, OnDestroy, OnInit } from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { DataStore } from '../../../core/data-store/data-store.service';
import { FilterService } from '../../../core/services/filter.service';
import { PopupService } from '../../../popups/popup.service';
import { finalize } from 'rxjs/operators';
import { ProductPlanService } from '../../../core/entities/product-plan/product-plan.service';
import { ProjectService } from '../../../core/entities/project/project.service';
import { Project } from '../../../core/entities/project/project.model';
import { PROJECT_STATUS } from '../../../core/entities/project/project.enum';

@Component({
  selector: 'app-products',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnDestroy, OnInit {

  list$ = DataStore.project.list.asObservable$;
  subscription$ : Subscription;
  loading : boolean = false;
  filter : FilterService;

  constructor(
    private productPlanService : ProductPlanService,
    private projectService : ProjectService,
    private popupService : PopupService
  ) {
    this.filter = (new FilterService(true)).init();
  }

  ngOnInit() {
    this.getProjects();
    this.subscription$ = merge(
      this.filter.onChangeFilter$,
      DataStore.project.onUpdate,
      DataStore.project.onCreate,
    ).subscribe(() => this.getProjects());
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getProjects() {
    this.loading = true;
    this.projectService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  activateProject(project : Project) {
    this.loading = true;
    this.projectService.activate(project.id)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => project.status = PROJECT_STATUS.ACTIVATED);
  }

  deactivateProject(project : Project) {
    this.loading = true;
    this.projectService.deactivate(project.id)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => project.status = PROJECT_STATUS.DEACTIVATED);
  }

  showProjectInfo(id : string) {
    this.popupService.showProjectInfo(id);
  }

  showProjectEdit(id : string) {
    this.popupService.showProjectEdit(id);
  }

}

