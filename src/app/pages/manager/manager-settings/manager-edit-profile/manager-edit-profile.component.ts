import { Component, ViewChild } from '@angular/core';
import { TENANT_TYPE } from '../../../../core/entities/tenant/tenant.enum';
import { REGEXPS } from '../../../../app.constants';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Tenant } from '../../../../core/entities/tenant/tenant.model';
import { NotificationsService } from '../../../../core/components/notifications/notifications.service';
import { ValidatorService } from '../../../../shared/validation/validation.service';
import { patchFormValues } from '../../../../core/helpers/helpers';
import { classToClass, plainToClassFromExist } from 'class-transformer';
import { finalize } from 'rxjs/operators';
import { DataStore } from '../../../../core/data-store/data-store.service';
import { ModalComponent } from '../../../../shared/components/modals/modal/modal.component';
import { UserService } from '../../../../core/entities/user/user.service';
import { User } from '../../../../core/entities/user/user.model';

@Component({
  selector: 'app-manager-edit-profile',
  templateUrl: './manager-edit-profile.component.html',
  styleUrls: ['./manager-edit-profile.component.scss']
})
export class ManagerEditProfileComponent {


  @ViewChild('modal') private modal : ModalComponent;
  readonly REGEXPS = REGEXPS;
  loading : boolean = false;
  form : FormGroup;
  user : User;

  get showFromCompany() {
    return parseInt(this.form.value.type) === TENANT_TYPE.COMPANY;
  }

  constructor(
    protected notificationsService : NotificationsService,
    protected validatorService : ValidatorService,
    protected userService : UserService,
    protected formBuilder : FormBuilder
  ) {

    this.form = this.formBuilder.group({

      email : ['', [Validators.email]],
      person : this.formBuilder.group({
        firstName : ['', [Validators.required]],
        lastName : ['', [Validators.required]]
      })
    });


  }

  open() {
    this.user = <Tenant>DataStore.user.current.getValue();
    patchFormValues(this.form, this.user);
    this.modal.open();
  }

  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.userService.updateProfile(plainToClassFromExist(classToClass(this.user), this.form.value as Object))
      .pipe(finalize((() => this.loading = false)))
      .subscribe(
        () => this.onSuccess(),
        err => this.validatorService.addErrorToForm(this.form, err)
      );

  }

  onSuccess() {
    this.modal.close();
    this.notificationsService.success('Profile update successfully');
  }

}
