import { Component } from '@angular/core';
import { DataStore } from '../../../core/data-store/data-store.service';

@Component({
  selector : 'app-manager-settings',
  templateUrl : './manager-settings.component.html',
  styleUrls : ['./manager-settings.component.scss']
})
export class ManagerSettingsComponent {
  user$ = DataStore.user.current.asObservable$;
}
