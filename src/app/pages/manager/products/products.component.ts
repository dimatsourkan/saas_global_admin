import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { DataStore } from '../../../core/data-store/data-store.service';
import { FilterService } from '../../../core/services/filter.service';
import { PopupService } from '../../../popups/popup.service';
import { finalize } from 'rxjs/operators';
import { ProductService } from '../../../core/entities/product/product.service';
import { ConfirmComponent } from '../../../shared/components/modals/confirm/confirm.component';
import { ProductPlanService } from '../../../core/entities/product-plan/product-plan.service';
import { PERMISSIONS } from '../../../core/entities/permission/permissions.list';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnDestroy, OnInit {

  @ViewChild('confirmPlan') private confirmPlan : ConfirmComponent;
  @ViewChild('confirm') private confirm : ConfirmComponent;
  list$ = DataStore.product.list.asObservable$;
  subscription$ : Subscription;
  PERMISSIONS = PERMISSIONS;
  loading : boolean = false;
  filter : FilterService;

  constructor(
    private productPlanService : ProductPlanService,
    private productService : ProductService,
    private popupService : PopupService
  ) {
    this.filter = (new FilterService(true)).init();
  }

  ngOnInit() {
    this.getProduct();
    this.subscription$ = merge(
      this.filter.onChangeFilter$,
      DataStore.productPlan.onUpdate,
      DataStore.productPlan.onCreate,
      DataStore.product.onUpdate,
      DataStore.product.onCreate,
    ).subscribe(() => this.getProduct());
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getProduct() {
    this.loading = true;
    this.productService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  showProductNew() {
    this.popupService.showProductNew();
  }

  showProductInfo(id : string) {
    this.popupService.showProductInfo(id);
  }

  showProductEdit(id : string) {
    this.popupService.showProductEdit(id);
  }

  showProductPlanNew(productId : string) {
    this.popupService.showProductPlanNew(productId);
  }

  showProductPlanInfo(productId : string, id : string) {
    this.popupService.showProductPlanInfo(productId, id);
  }

  showProductPlanEdit(productId : string, id : string) {
    this.popupService.showProductPlanEdit(productId, id);
  }

  deleteProduct(id : string) {
    this.confirm.open(() => {
      this.loading = true;
      this.confirm.close();
      this.productService.delete(id)
        .pipe(finalize(() => this.loading = false))
        .subscribe(() => this.getProduct());
    });
  }

  deletePlan(productId : string, id : string) {
    this.confirmPlan.open(() => {
      this.loading = true;
      this.confirmPlan.close();
      this.productPlanService.productId = productId;
      this.productPlanService.delete(id)
        .pipe(finalize(() => this.loading = false))
        .subscribe(() => this.getProduct());
    });
  }

}

