import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MANAGER_ROUTES } from './manager.routing';
import { ManagerComponent } from './manager.component';
import { TenantsComponent } from './tenants/tenants.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { SharedModule } from '../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { ProductsComponent } from './products/products.component';
import { ManagersComponent } from './managers/managers.component';
import { BlockedDomainComponent } from './blocked-domain/blocked-domain.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectsComponent } from './projects/projects.component';
import { RolesComponent } from './roles/roles.component';
import { ProjectRequestsComponent } from './project-requests/project-requests.component';
import { MailingComponent } from './mailing/mailing.component';
import { MailingGroupsComponent } from './mailing/mailing-groups/mailing-groups.component';
import { MailingHistoryComponent } from './mailing/mailing-history/mailing-history.component';
import { InvoicePopupFilterComponent } from './invoice/invoice-popup-filter/invoice-popup-filter.component';
import { StatusDropdownComponent } from './invoice/status-dropdown/status-dropdown.component';
import { ManagerSettingsComponent } from './manager-settings/manager-settings.component';
import { ManagerEditProfileComponent } from './manager-settings/manager-edit-profile/manager-edit-profile.component';
import { InvoiceListFilterComponent } from './invoice/invoice-list-filter/invoice-list-filter.component';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(MANAGER_ROUTES)
  ],
  declarations : [
    ManagerComponent,
    TenantsComponent,
    InvoiceComponent,
    ProductsComponent,
    ManagersComponent,
    ProjectsComponent,
    BlockedDomainComponent,
    RolesComponent,
    ProjectRequestsComponent,
    MailingComponent,
    MailingGroupsComponent,
    MailingHistoryComponent,
    InvoicePopupFilterComponent,
    StatusDropdownComponent,
    ManagerSettingsComponent,
    ManagerEditProfileComponent,
    InvoiceListFilterComponent
  ]
})
export class ManagerModule {
}
