import { Component, OnDestroy, OnInit } from '@angular/core';
import { TenantService } from '../../../core/entities/tenant/tenant.service';
import { DataStore } from '../../../core/data-store/data-store.service';
import { merge, Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { PopupService } from '../../../popups/popup.service';
import { FilterService } from '../../../core/services/filter.service';
import { Helpers } from '../../../core/helpers/helpers';
import { Project } from '../../../core/entities/project/project.model';
import { PERMISSIONS } from '../../../core/entities/permission/permissions.list';
import { modelId } from '../../../core/models/base.model';

@Component({
  selector : 'app-tenants',
  templateUrl : './tenants.component.html',
  styleUrls : ['./tenants.component.scss']
})
export class TenantsComponent implements OnDestroy, OnInit {

  PERMISSIONS = PERMISSIONS;
  list$ = DataStore.tenant.list.asObservable$;
  subscription$ : Subscription;
  loading : boolean = false;
  filter : FilterService;
  helpers : Helpers = new Helpers();
  /**
   * Хранит данные о том, у каких тенантов показывается полный список проектов, у каких сокращенный
   */
  projectsShowed : {[id : string] : boolean} = {};

  constructor(
    private tenantService : TenantService,
    private popupService : PopupService
  ) {
    this.filter = (new FilterService(true)).init();
  }

  ngOnInit() {
    this.getTenants();
    this.subscription$ = merge(
      this.filter.onChangeFilter$,
      DataStore.tenant.onCreate,
    ).subscribe(() => this.getTenants());
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getTenants() {
    this.loading = true;
    this.subscription$ = this.tenantService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => this.projectsShowed = {});
  }

  showTenant(id : string) {
    this.popupService.showTenantInfo(id);
  }

  showNewTenant() {
    this.popupService.showTenantNew();
  }

  showEditTenant(id : string) {
    this.popupService.showTenantEdit(id);
  }

  showProjectInfo(id : string) {
    this.popupService.showProjectInfo(id);
  }

  firstProject(projects : Project[]) {
    return projects[0];
  }

  projectHiddenCount(length : number) {
    return length - 1;
  }

  toggleProjects(id : modelId) {
    if(this.projectsShowed[id]) {
      this.projectsShowed[id] = false;
    } else {
      this.projectsShowed[id] = true;
    }
  }

}
