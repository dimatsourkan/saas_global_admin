import { Routes } from '@angular/router';
import { ManagerComponent } from './manager.component';
import { TenantsComponent } from './tenants/tenants.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { ProductsComponent } from './products/products.component';
import { ManagersComponent } from './managers/managers.component';
import { BlockedDomainComponent } from './blocked-domain/blocked-domain.component';
import { ProjectsComponent } from './projects/projects.component';
import { RolesComponent } from './roles/roles.component';
import { ProjectRequestsComponent } from './project-requests/project-requests.component';
import { MailingComponent } from './mailing/mailing.component';
import { HasPermission } from '../../core/guards/has-permission.service';
import { PERMISSIONS } from '../../core/entities/permission/permissions.list';
import { ManagerSettingsComponent } from './manager-settings/manager-settings.component';

export const MANAGER_ROUTES : Routes = [

  {
    path : '',
    component : ManagerComponent,
    children : [
      {
        path : '',
        pathMatch : 'full',
        redirectTo : 'tenants'
      },
      {
        path : 'managers',
        canActivate : [HasPermission],
        data : { permissions : [PERMISSIONS.VIEW_MANAGERS] },
        component : ManagersComponent
      },
      {
        path : 'tenants',
        canActivate : [HasPermission],
        data : { permissions : [PERMISSIONS.VIEW_TENANTS] },
        component : TenantsComponent
      },
      {
        path : 'mailbox',
        canLoad : [HasPermission],
        data : { permissions : [PERMISSIONS.MAILBOX] },
        loadChildren : '../common/mailbox/mailbox.module#MailboxModule'
      },
      {
        path : 'roles',
        canActivate : [HasPermission],
        data : { permissions : [PERMISSIONS.MANAGE_ROLES] },
        component : RolesComponent
      },
      {
        path : 'mailing',
        canActivate : [HasPermission],
        data : { permissions : [PERMISSIONS.MASS_MAILING] },
        component : MailingComponent
      },
      {
        path : 'invoice',
        canActivate : [HasPermission],
        data : { permissions : [PERMISSIONS.MANAGE_INVOICES] },
        component : InvoiceComponent
      },
      {
        path : 'products',
        canActivate : [HasPermission],
        data : { permissions : [PERMISSIONS.MANAGE_PRODUCTS] },
        component : ProductsComponent
      },
      {
        path : 'projects',
        canActivate : [HasPermission],
        data : { permissions : [PERMISSIONS.MANAGE_PROJECTS] },
        component : ProjectsComponent
      },
      {
        path : 'requests',
        canActivate : [HasPermission],
        data : { permissions : [PERMISSIONS.MANAGE_PROJECT_REQUESTS] },
        component : ProjectRequestsComponent
      },
      {
        path : 'blocked-domain',
        canActivate : [HasPermission],
        data : { permissions : [PERMISSIONS.MANAGE_BLOCKED_DOMAINS] },
        component : BlockedDomainComponent
      },
      {
        path : 'settings',
        component : ManagerSettingsComponent
      }
    ]
  }

];
