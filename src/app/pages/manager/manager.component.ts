import { Component, OnDestroy, OnInit } from '@angular/core';
import { PERMISSIONS } from '../../core/entities/permission/permissions.list';
import { ProjectRequestService } from '../../core/entities/project-request/project-request.service';
import { DataStore } from '../../core/data-store/data-store.service';
import { interval, merge, Subscription } from 'rxjs';
import { ENVIRONMENT } from '../../../environments/environment';
import { MailboxService } from '../../core/entities/mailbox/mailbox.service';

@Component({
  selector : 'app-manager',
  templateUrl : './manager.component.html',
  styleUrls : ['./manager.component.scss']
})
export class ManagerComponent implements OnInit, OnDestroy {

  subscription$ : Subscription;
  requestCount$ = DataStore.projectRequest.count.asObservable$;
  mailboxCount$ = DataStore.mailbox.count.asObservable$;
  PERMISSIONS = PERMISSIONS;

  constructor(
    private projectRequestService : ProjectRequestService,
    private mailboxService : MailboxService
  ) {}

  ngOnInit() {
    this.getRequestCounts();
    this.getNewMailboxCount();
    this.subscription$ = merge(
      interval(ENVIRONMENT.COUNTERS_UPDATE_TIME),
      DataStore.projectRequest.onUpdate,
      DataStore.mailbox.onUpdate,
    ).subscribe(() => {
      this.getRequestCounts();
      this.getNewMailboxCount();
    });
  }

  ngOnDestroy() {
    if(this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  getRequestCounts() {
    this.projectRequestService.getNewRequestCount().subscribe();
  }

  getNewMailboxCount() {
    this.mailboxService.getNewUnreadCount().subscribe();
  }
}
