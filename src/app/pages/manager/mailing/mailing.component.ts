import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { MailingService } from '../../../core/entities/mailing/mailing.service';
import { MailingSend } from '../../../core/entities/mailing/mailing.model';
import { finalize } from 'rxjs/operators';
import { NotificationsService } from '../../../core/components/notifications/notifications.service';
import { PERMISSIONS } from '../../../core/entities/permission/permissions.list';
import { MailingGroupsComponent } from './mailing-groups/mailing-groups.component';

@Component({
  selector : 'app-mailing',
  templateUrl : './mailing.component.html',
  styleUrls : ['./mailing.component.scss']
})
export class MailingComponent {

  @ViewChild('mailingGroups') private mailingGroups : MailingGroupsComponent;
  PERMISSIONS = PERMISSIONS;
  loading = false;
  form : FormGroup;

  constructor(
    private formBuilder : FormBuilder,
    private mailingService : MailingService,
    private notificationsService : NotificationsService,
    private validatorService : ValidatorService
  ) {
    this.form = this.formBuilder.group({
      subject : ['', [Validators.required]],
      body : ['', [Validators.required]],
      email : [],
      emails : [[]],
      groups : [[]]
    });

    this.form.valueChanges.subscribe(() => {
      this.form.get('email').setErrors(null);
    });
  }

  setGroups(ids : number[]) {
    this.form.get('emails').setErrors({});
    this.form.get('emails').updateValueAndValidity();
    this.form.patchValue({
      groups : ids
    });
  }

  submit() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.mailingService.send(MailingSend.plainToClass(this.form.value))
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        () => this.onSuccess(),
        (err) => this.validatorService.addErrorToForm(this.form, err)
      );
  }

  onSuccess() {
    this.form.reset();
    this.mailingGroups.resetGroups();
    this.notificationsService.success('Mailing send successfully');
  }

}
