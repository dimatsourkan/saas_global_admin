import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailingGroupsComponent } from './mailing-groups.component';

describe('MailingGroupsComponent', () => {
  let component: MailingGroupsComponent;
  let fixture: ComponentFixture<MailingGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailingGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailingGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
