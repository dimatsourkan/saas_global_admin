import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PopupService } from '../../../../popups/popup.service';
import { MailingService } from '../../../../core/entities/mailing/mailing.service';
import { DataStore } from '../../../../core/data-store/data-store.service';
import { merge, Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { MailingGroup } from '../../../../core/entities/mailing/mailing.model';
import { ResultList } from '../../../../core/models/result.model';
import { modelId } from '../../../../core/models/base.model';

@Component({
  selector: 'app-mailing-groups',
  templateUrl: './mailing-groups.component.html',
  styleUrls: ['./mailing-groups.component.scss']
})
export class MailingGroupsComponent implements OnInit {

  list = DataStore.mailing.list.asObservable$;
  subscription$ : Subscription;
  loading : boolean = false;
  checked : modelId[] = [];

  @Output() onChange = new EventEmitter();

  constructor(
    private popupService : PopupService,
    private mailingService : MailingService
  ) {}

  ngOnInit() {
    this.getGroups();
    this.subscription$ = merge(
      DataStore.mailing.onUpdate,
      DataStore.mailing.onCreate,
    ).subscribe(() => this.getGroups())
  }

  ngOnDestroy() {
    if(this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  getGroups() {
    this.loading = true;
    this.mailingService.getAll()
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  toggleCheck(id : modelId) {

    if(this.isChecked(id)) {
      this.checked.splice(this.checked.indexOf(id), 1);
    } else {
      this.checked.push(id);
    }

    this.onChange.emit(this.checked);
  }

  isChecked(id : modelId) {
    return this.checked.indexOf(id) >= 0;
  }

  showMailingGroupNew() {
    this.popupService.showMailingGroupNew()
  }

  resetGroups() {
    this.checked = [];
  }

  showMailingGroupEdit(id : string, event : any) {
    this.popupService.showMailingGroupEdit(id);
    event.preventDefault();
    event.stopPropagation();
  }

}
