import { Component } from '@angular/core';
import { PLACEHOLDER_TYPE } from '../../../shared/components/form-controls/form-controls.component';

@Component({
  selector : 'app-support',
  templateUrl : './support.component.html',
  styleUrls : ['./support.component.scss']
})
export class SupportComponent {

  PLACEHOLDER_TYPE = PLACEHOLDER_TYPE;

}
