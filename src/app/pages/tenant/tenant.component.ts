import { Component, OnInit } from '@angular/core';
import { DataStore } from '../../core/data-store/data-store.service';
import { AuthService } from '../../core/services/auth.service';
import { User } from '../../core/entities/user/user.model';
import { MailboxService } from '../../core/entities/mailbox/mailbox.service';
import { interval, merge, Subscription } from 'rxjs';
import { ENVIRONMENT } from '../../../environments/environment';

@Component({
  selector : 'app-tenant',
  templateUrl : './tenant.component.html',
  styleUrls : ['./tenant.component.scss']
})
export class TenantComponent implements OnInit {

  subscription$ : Subscription;
  user$ = DataStore.user.current.asObservable$;
  mailboxCount$ = DataStore.mailbox.count.asObservable$;

  constructor(
    private authService : AuthService,
    private mailboxService : MailboxService
  ) {
  }

  ngOnInit() {
    this.getNewMailboxCount();
    this.subscription$ = merge(
      interval(ENVIRONMENT.COUNTERS_UPDATE_TIME),
      DataStore.projectRequest.onUpdate,
      DataStore.mailbox.onUpdate,
    ).subscribe(() => {
      this.getNewMailboxCount();
    });
  }

  ngOnDestroy() {
    if(this.subscription$) {
      this.subscription$.unsubscribe();
    }
  }

  getNewMailboxCount() {
    if(DataStore.user.current.getValue().level.isTermsAccepted) {
      this.mailboxService.getNewUnreadCount().subscribe();
    }
  }

  get isAuthenticated() : boolean {
    return this.authService.isAuthenticated;
  }

  userLevelTrues(user : User) {
    return (!user.level.isTermsAccepted || !user.level.isEmailConfirmed || !user.level.isProfileFilled)
  }

  showUserLevel(user : User) {
    return this.isAuthenticated && this.userLevelTrues(user);
  }

}
