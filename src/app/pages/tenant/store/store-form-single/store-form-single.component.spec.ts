import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreFormSingleComponent } from './store-form-single.component';

describe('StoreFormSingleComponent', () => {
  let component: StoreFormSingleComponent;
  let fixture: ComponentFixture<StoreFormSingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreFormSingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreFormSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
