import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '../../../../shared/components/modals/modal/modal.component';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../../../core/entities/user/user.model';
import { ValidatorService } from '../../../../shared/validation/validation.service';
import { DataStore } from '../../../../core/data-store/data-store.service';
import { debounceTime, finalize } from 'rxjs/operators';
import { ProjectService } from '../../../../core/entities/project/project.service';
import { ProductService } from '../../../../core/entities/product/product.service';
import { ProjectRequest } from '../../../../core/entities/project-request/project-request.model';
import { PLACEHOLDER_TYPE } from '../../../../shared/components/form-controls/form-controls.component';
import { Product } from '../../../../core/entities/product/product.model';
import { ProductLimit } from '../../../../core/entities/product/product-limit.model';
import { ProductBuilderService } from '../../../../shared/components/product-builder/product-builder.service';
import { InvoiceTotal } from '../../../../core/entities/invoice/invoice.model';
import { Subject } from 'rxjs';
import { InvoiceService } from '../../../../core/entities/invoice/invoice.service';

@Component({
  selector : 'app-store-form-single',
  templateUrl : './store-form-single.component.html',
  styleUrls : ['./store-form-single.component.scss']
})
export class StoreFormSingleComponent implements OnInit, OnDestroy {

  @ViewChild('modal') private modal : ModalComponent;
  PLACEHOLDER_TYPE = PLACEHOLDER_TYPE;
  form : FormGroup;
  formItems : FormArray;
  loading = false;
  success = false;
  product : Product = new Product();
  user : User = <User>DataStore.user.current.getValue();
  totalPrice = new InvoiceTotal();
  totalSubscriber$ = new Subject();

  constructor(
    private formBuilder : FormBuilder,
    private projectService : ProjectService,
    private invoiceService : InvoiceService,
    private productService : ProductService,
    private validatorService : ValidatorService,
    private productBuilderService : ProductBuilderService
  ) {

    this.productBuilderService.taxPercent = this.user.billingInfo.taxPercent;

    this.form = this.formBuilder.group({
      projectId : [, Validators.required],
      payerName : [, Validators.required],
      items : this.formBuilder.array([
        this.formBuilder.group({
          productPlan : ['', [Validators.required]],
          product : [],
          extraLimits : this.formBuilder.array([
            this.formBuilder.group({
              value : [],
              name : []
            })
          ])
        })
      ])
    });

    this.formItems = this.form.get('items') as FormArray;

    this.form.valueChanges.subscribe(() => {
      this.totalSubscriber$.next();
    });

  }

  ngOnInit() {
    this.totalSubscriber$
      .pipe(debounceTime(500))
      .subscribe((value : string) => {
        this.invoiceService.getTotal(this.getInvoiceFullModel()).subscribe(res => {
          this.totalPrice = res;
        });
      });
  }

  ngOnDestroy() {
    this.totalSubscriber$.unsubscribe();
  }

  onClear() {
    this.productExtraForm.get('value').setValue(null);
  }

  open(id : string) {

    this.form.reset();
    this.modal.open();
    this.success = false;

    this.loading = true;
    this.productService.get(id)
      .pipe(finalize(() => this.loading = false))
      .subscribe((res) => {
        this.product = res;
        this.productItemForm.patchValue({
          product : res
        });
        this.productExtraForm.patchValue({
          name : this.product.productLimits[0].name
        });
      });

  }

  /**
   * Отдает продукт план из формы
   */
  get productLimit() : ProductLimit {
    return this.product.productLimits[0];
  }

  /**
   * Отдает extra form
   */
  get productExtraForm() {
    return this.productItemForm.get('extraLimits').get([0]);
  }

  get productItemForm() {
    return this.form.get('items').get([0]);
  }

  /**
   * Отправка реквеста
   */
  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.projectService.request(this.form.value.projectId, this.getInvoiceFullModel())
      .pipe(finalize((() => this.loading = false)))
      .subscribe(
        (res) => this.onSuccess(res),
        err => {
          this.validatorService.addErrorToForm(this.form, err);
        }
      );

  }

  getInvoiceFullModel() : ProjectRequest {
    console.log(this.form.getRawValue() as Object);
    let request = ProjectRequest.plainToClass(this.form.getRawValue());
    request.items = this.productBuilderService
      .filterEmptyExtralimits(request.items)
      .filter((i) => i.product);

    return request;
  }

  onSuccess(res : any) {
    this.form.reset();
    this.success = true;
  }

}
