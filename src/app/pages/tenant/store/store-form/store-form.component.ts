import { Component, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../../../core/entities/product/product.service';
import { debounceTime, finalize } from 'rxjs/operators';
import { ValidatorService } from '../../../../shared/validation/validation.service';
import { ProjectService } from '../../../../core/entities/project/project.service';
import { ProjectRequest } from '../../../../core/entities/project-request/project-request.model';
import { ProductPlanService } from '../../../../core/entities/product-plan/product-plan.service';
import { DataStore } from '../../../../core/data-store/data-store.service';
import { User } from '../../../../core/entities/user/user.model';
import { ProductBuilderComponent } from '../../../../shared/components/product-builder/product-builder.component';
import { ProductBuilderService } from '../../../../shared/components/product-builder/product-builder.service';
import { InvoiceTotal } from '../../../../core/entities/invoice/invoice.model';
import { PLACEHOLDER_TYPE } from '../../../../shared/components/form-controls/form-controls.component';
import { InvoiceService } from '../../../../core/entities/invoice/invoice.service';
import { Subject } from 'rxjs';

@Component({
  selector : 'app-market-form',
  templateUrl : './store-form.component.html',
  styleUrls : ['./store-form.component.scss']
})
export class StoreFormComponent {

  form : FormGroup;
  formItems : FormArray;
  loading = false;
  success = false;
  PLACEHOLDER_TYPE = PLACEHOLDER_TYPE;
  user : User = DataStore.user.current.getValue();
  @ViewChild('productBuilder') private productBuilder : ProductBuilderComponent;
  totalPrice = new InvoiceTotal();
  totalSubscriber$ = new Subject();

  constructor(
    private formBuilder : FormBuilder,
    private projectService : ProjectService,
    private invoiceService : InvoiceService,
    private productService : ProductService,
    private productPlanService : ProductPlanService,
    private validatorService : ValidatorService,
    private productBuilderService : ProductBuilderService
  ) {

    this.form = this.formBuilder.group({
      projectId : [, Validators.required],
      payerName : [, Validators.required],
      items : this.formBuilder.array([])
    });

    this.formItems = this.form.get('items') as FormArray;
    this.productBuilderService.taxPercent = this.user.billingInfo.taxPercent;
    this.form.valueChanges.subscribe(() => {
      this.totalSubscriber$.next();
    });

  }

  ngOnInit() {
    this.totalSubscriber$
      .pipe(debounceTime(500))
      .subscribe((value : string) => {
        this.invoiceService.getTotal(this.getInvoiceFullModel()).subscribe(res => {
          this.totalPrice = res;
        });
      });
  }

  ngOnDestroy() {
    this.totalSubscriber$.unsubscribe();
  }

  /**
   * Записывает форму из компоненты
   * @param event
   */
  onFormChange(event : FormGroup[]) {
    this.formItems.controls = event;
    this.formItems.patchValue([]);
    this.form.patchValue({});
  }

  /**
   * Отправка реквеста
   */
  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;

    this.projectService.request(this.form.value.projectId, this.getInvoiceFullModel())
      .pipe(finalize((() => this.loading = false)))
      .subscribe(
        (res) => this.onSuccess(res),
        err => {
          this.validatorService.addErrorToForm(this.form, err);
        }
      );

  }

  getInvoiceFullModel() : ProjectRequest {
    let request = ProjectRequest.plainToClass(this.form.getRawValue());
    request.items = this.productBuilderService
      .filterEmptyExtralimits(request.items)
      .filter((i) => i.product);

    return request;
  }

  onSuccess(res : any) {
    this.form.reset();
    this.success = true;
    this.productBuilder.resetForm();
  }

}
