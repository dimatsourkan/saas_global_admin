import { Component, ViewChild } from '@angular/core';
import { ModalComponent } from '../../../../../shared/components/modals/modal/modal.component';
import { PLACEHOLDER_TYPE } from '../../../../../shared/components/form-controls/form-controls.component';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../../../shared/validation/validation.service';
import { finalize } from 'rxjs/operators';
import { ProjectService } from '../../../../../core/entities/project/project.service';
import { Project } from '../../../../../core/entities/project/project.model';
import { NotificationsService } from '../../../../../core/components/notifications/notifications.service';

@Component({
  selector : 'app-project-form',
  templateUrl : './project-form.component.html',
  styleUrls : ['./project-form.component.scss']
})
export class ProjectFormComponent {

  @ViewChild('projectModal') private projectModal : ModalComponent;

  PLACEHOLDER_TYPE = PLACEHOLDER_TYPE;
  loading = false;

  form : FormGroup;

  constructor(
    private notificationsService : NotificationsService,
    private validatorService : ValidatorService,
    private projectService : ProjectService
  ) {
    this.form = new FormGroup({
      name : new FormControl('', Validators.required)
    });
  }

  open() {
    this.projectModal.open();
  }

  close() {
    this.projectModal.close();
  }

  submit() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.projectService.save(Project.plainToClass(this.form.value))
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        (res) => this.onSuccess(),
        err => this.validatorService.addErrorToForm(this.form, err)
      );

  }

  onSuccess() {
    this.projectModal.close();
    this.notificationsService.success('Project created successfully');
  }

  onClose() {
    this.form.reset();
  }
}
