import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Product } from '../../../../../core/entities/product/product.model';

@Component({
  selector: 'app-store-item',
  templateUrl: './store-item.component.html',
  styleUrls: ['./store-item.component.scss']
})
export class StoreItemComponent {

  @Input() item : Product;
  @Input() disabledBuy : boolean = false;
  @Output() onBtnCLick = new EventEmitter();

  click() {
    this.onBtnCLick.emit();
  }

}
