import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../../../../core/entities/product/product.model';

@Pipe({
  name : 'productsByType'
})
export class ProductsByPipe implements PipeTransform {

  transform(value : Product[], type : number) : Product[] {
    return value.filter(p => p.type === type);
  }

}
