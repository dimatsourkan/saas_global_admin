import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProductService } from '../../../../core/entities/product/product.service';
import { finalize } from 'rxjs/operators';
import { merge, Subscription } from 'rxjs';
import { DataStore } from '../../../../core/data-store/data-store.service';
import { FilterService } from '../../../../core/services/filter.service';
import { ActivatedRoute } from '@angular/router';
import { PRODUCT_TYPE } from '../../../../core/entities/product/product.enum';

@Component({
  selector : 'app-market',
  templateUrl : './store-list.component.html',
  styleUrls : ['./store-list.component.scss']
})
export class StoreListComponent implements OnInit, OnDestroy {

  list$ = DataStore.product.list.asObservable$;
  subscription$ : Subscription;
  PRODUCT_TYPE = PRODUCT_TYPE;
  loading : boolean = false;
  filter : FilterService;

  constructor(
    private activatedRoute : ActivatedRoute,
    private productService : ProductService
  ) {
    this.filter = (new FilterService()).init();
    this.filter.limit = 100;
  }

  ngOnInit() {
    this.subscription$ = merge(
      this.activatedRoute.queryParams,
      this.filter.onChangeFilter$
    ).subscribe(() => {
      this.getProducts();
    });
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getProducts() {
    this.loading = true;
    this.productService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe(() => {
        this.scrollToBuilder()
      });
  }

  scrollToBuilder() {
    let hash = document.location.hash;
    if(hash) {
      let builder = document.querySelector(hash);
      if(builder) {
        setTimeout(() => {
          builder.scrollIntoView(true);
        }, 100);
      }
    }
  }

}
