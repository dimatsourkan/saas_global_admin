import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TENANT_ROUTES } from './tenant.routing';
import { HomeComponent } from './home/home.component';
import { TenantComponent } from './tenant.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { StoreListComponent } from './store/store-list/store-list.component';
import { ProjectsComponent } from './projects/projects.component';
import { SettingsComponent } from './settings/settings.component';
import { SupportComponent } from './support/support.component';
import { SharedModule } from '../../shared/shared.module';
import { StoreFormComponent } from './store/store-form/store-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ProjectFormComponent } from './store/store-form/project-form/project-form.component';
import { ProfileComponent } from './settings/profile/profile.component';
import { ReferralComponent } from './settings/referral/referral.component';
import { ActivityLogComponent } from './settings/activity-log/activity-log.component';
import { StoreFormSingleComponent } from './store/store-form-single/store-form-single.component';
import { PendingChangesGuard } from './settings/settings.guard';
import { ProductsByPipe } from './store/store-list/store-list.pipe';
import { StoreItemComponent } from './store/store-list/store-item/store-item.component';
import { BannerComponent } from './home/banner/banner.component';
import { StatisticComponent } from './home/statistic/statistic.component';
import { ViewComponent } from './home/view/view.component';

@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    RouterModule.forChild(TENANT_ROUTES)
  ],
  declarations : [
    ProductsByPipe,
    HomeComponent,
    TenantComponent,
    InvoiceComponent,
    StoreListComponent,
    ProjectsComponent,
    SettingsComponent,
    SupportComponent,
    StoreFormComponent,
    ProjectFormComponent,
    ProfileComponent,
    ReferralComponent,
    ActivityLogComponent,
    StoreFormSingleComponent,
    StoreItemComponent,
    BannerComponent,
    StatisticComponent,
    ViewComponent
  ],
  providers : [
    PendingChangesGuard
  ]
})
export class TenantModule {
}
