import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { SettingsComponent } from './settings.component';

@Injectable()
export class PendingChangesGuard implements CanDeactivate<SettingsComponent> {

  canDeactivate(component : SettingsComponent) {
    if(component) {
      return component.canDeactivate();
    } else {
      return true;
    }
  }

}
