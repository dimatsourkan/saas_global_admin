import { Component, ViewChild } from '@angular/core';
import { TENANT_TYPE } from '../../../../core/entities/tenant/tenant.enum';
import { REGEXPS } from '../../../../app.constants';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Tenant } from '../../../../core/entities/tenant/tenant.model';
import { NotificationsService } from '../../../../core/components/notifications/notifications.service';
import { ValidatorService } from '../../../../shared/validation/validation.service';
import { CustomValidators } from '../../../../shared/validation/validators/custom.validator';
import { patchFormValues } from '../../../../core/helpers/helpers';
import { classToClass, plainToClassFromExist } from 'class-transformer';
import { finalize } from 'rxjs/operators';
import { DataStore } from '../../../../core/data-store/data-store.service';
import { UserService } from '../../../../core/entities/user/user.service';
import { User } from '../../../../core/entities/user/user.model';
import { ConfirmComponent } from '../../../../shared/components/modals/confirm/confirm.component';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {

  @ViewChild('confirm') private confirm : ConfirmComponent;
  readonly TENANT_TYPE = TENANT_TYPE;
  readonly REGEXPS = REGEXPS;
  loading : boolean = false;
  form : FormGroup;
  mailGroups : FormArray;
  user : User = <Tenant>DataStore.user.current.getValue();

  get showFromCompany() {
    return parseInt(this.form.value.type) === TENANT_TYPE.COMPANY;
  }

  constructor(
    protected notificationsService : NotificationsService,
    protected validatorService : ValidatorService,
    protected userService : UserService,
    protected formBuilder : FormBuilder
  ) {

    this.form = this.formBuilder.group({

      type : ['', Validators.required],
      phone : ['', [Validators.required, CustomValidators.phone]],
      email : [{ value : '', disabled : true }, [Validators.required, Validators.email]],
      billingAddress : [''],
      person : this.formBuilder.group({
        firstName : ['', [Validators.required]],
        lastName : ['', [Validators.required]]
      }),

      address : this.formBuilder.group({
        countryCode : ['', [Validators.required]],
        address : ['', [Validators.required]],
        zipCode : ['', [CustomValidators.numbersOrLetters]]
      }),

      billingInfo : this.formBuilder.group({
        company : [''],
        invoiceEmail : ['', [Validators.required, Validators.email]],
        taxPercent : ['', CustomValidators.intOrFloat],
        vat : ['', CustomValidators.numeric]
      }),

      mailGroups : this.formBuilder.array([])
    });

    this.mailGroups = this.form.get('mailGroups') as FormArray;

    this.user.mailGroups.forEach(group => {
      let formGroup = this.mailGroupForm();
      formGroup.patchValue(group);
      this.mailGroups.push(formGroup);
    });

    patchFormValues(this.form, this.user);

    /**
     * TODO - Можно сказать что костыль
     * Пока что не нашел решения как вынести это в CustomValidators
     * Валидация для поля company
     * Оно должно быть обязательным если поле type === TENANT_TYPE.COMPANY
     */
    const companyControl = this.form.get('billingInfo').get('company');
    this.form.get('type').valueChanges.subscribe((type : string) => {
      if(parseInt(type) === TENANT_TYPE.COMPANY) {
        companyControl.setValidators([Validators.required])
      } else {
        companyControl.clearValidators();
      }
      companyControl.updateValueAndValidity();

    });


  }

  mailGroupForm() {
    return this.formBuilder.group({
      id : [null],
      name : [null],
      subscribed : [false]

    })
  }

  downloadData() {
    this.userService.downloadData().subscribe(res => {
      FileSaver.saveAs(res.body, `Profile data`);
    });
  }

  canDeactivate() : Promise<boolean> {
    return new Promise((resolve, reject) => {

      if(this.form.dirty) {
        this.confirm.open(
          () => {
            resolve(false);
            this.confirm.close();
            this.submitForm();
          },
          () => {
            this.confirm.close();
            setTimeout(() => {
              resolve(true);
            }, 300);
          }
        );
        return false;
      } else {
        return resolve(true);
      }
    });
  }

  submitForm() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.userService.updateProfile(plainToClassFromExist(classToClass(this.user), this.form.value as Object))
      .pipe(finalize((() => this.loading = false)))
      .subscribe(
        () => {
          this.onSuccess();
        },
        err => this.validatorService.addErrorToForm(this.form, err)
      );

  }

  onSuccess() {
    this.form.markAsPristine({onlySelf : true});
    this.notificationsService.success('Profile update successfully');
  }

}
