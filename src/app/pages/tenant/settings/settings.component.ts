import { Component, ViewChild } from '@angular/core';
import { ProfileComponent } from './profile/profile.component';

@Component({
  selector : 'app-settings',
  templateUrl : './settings.component.html',
  styleUrls : ['./settings.component.scss']
})
export class SettingsComponent {

  @ViewChild('profile') private profile : ProfileComponent;

  canDeactivate() {
    return this.profile.canDeactivate();
  }


}
