import { Component, OnDestroy, OnInit } from '@angular/core';
import { merge, Subscription } from 'rxjs';
import { DataStore } from '../../../core/data-store/data-store.service';
import { FilterService } from '../../../core/services/filter.service';
import { PopupService } from '../../../popups/popup.service';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { ProductPlanService } from '../../../core/entities/product-plan/product-plan.service';
import { ProjectService } from '../../../core/entities/project/project.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnDestroy, OnInit {

  list$ = DataStore.project.list.asObservable$;
  subscription$ : Subscription;
  loading : boolean = false;
  filter : FilterService;

  constructor(
    private productPlanService : ProductPlanService,
    private activatedRoute : ActivatedRoute,
    private projectService : ProjectService,
    private popupService : PopupService
  ) {
    this.filter = (new FilterService(true)).init();
  }

  ngOnInit() {
    this.subscription$ = merge(
      this.activatedRoute.queryParams,
      this.filter.onChangeFilter$,
      DataStore.project.onCreate
    ).subscribe(() => this.getProjects());
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getProjects() {
    this.loading = true;
    this.projectService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  showProjectInfo(id : string) {
    this.popupService.showProjectInfo(id);
  }

}

