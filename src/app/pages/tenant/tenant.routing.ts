import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TenantComponent } from './tenant.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { ProjectsComponent } from './projects/projects.component';
import { SettingsComponent } from './settings/settings.component';
// import { SupportComponent } from './support/support.component';
import { StoreListComponent } from './store/store-list/store-list.component';
import { PendingChangesGuard } from './settings/settings.guard';

export const TENANT_ROUTES : Routes = [

  {
    path : '',
    component : TenantComponent,
    children : [
      {
        path : '',
        pathMatch : 'full',
        redirectTo : 'home'
      },
      {

        path : 'home',
        component : HomeComponent
      },
      {
        path : 'mailbox',
        loadChildren : '../common/mailbox/mailbox.module#MailboxModule'
      },
      {

        path : 'invoice',
        component : InvoiceComponent
      },
      {

        path : 'store',
        component : StoreListComponent
      },
      {

        path : 'projects',
        component : ProjectsComponent
      },
      {

        path : 'settings',
        canDeactivate : [PendingChangesGuard],
        component : SettingsComponent
      },
      // {
      //
      //   path : 'support',
      //   component : SupportComponent
      // }
    ]
  }

];
