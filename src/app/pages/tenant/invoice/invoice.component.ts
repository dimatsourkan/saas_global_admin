import { Component, OnInit } from '@angular/core';
import { InvoiceFilter } from '../../../core/entities/invoice/invoice-filter.service';
import { DataStore } from '../../../core/data-store/data-store.service';
import { merge, Subscription } from 'rxjs';
import { InvoiceService } from '../../../core/entities/invoice/invoice.service';
import { PopupService } from '../../../popups/popup.service';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';

@Component({
  selector : 'app-billing',
  templateUrl : './invoice.component.html',
  styleUrls : ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {

  list$ = DataStore.invoice.list.asObservable$;
  subscription$ : Subscription;
  loading : boolean = false;
  filter : InvoiceFilter;

  constructor(
    private invoiceService : InvoiceService,
    private popupService : PopupService
  ) {
    this.filter = (new InvoiceFilter(true)).init();

    if(!this.filter.dateFrom || !this.filter.dateTo) {
      this.filter.dateFrom = moment().subtract(7, 'days');
      this.filter.dateTo = moment();
      this.filter.cancelCurrentChangeEvent();
    }
  }

  ngOnInit() {
    this.getInvoices();
    this.subscription$ = merge(
      this.filter.onChangeFilter$,
    ).subscribe(() => this.getInvoices())
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getInvoices() {
    this.loading = true;
    this.invoiceService.getAll(this.filter.filter)
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  showBilling(id : string) {
    this.popupService.showInvoiceInfo(id);
  }

}
