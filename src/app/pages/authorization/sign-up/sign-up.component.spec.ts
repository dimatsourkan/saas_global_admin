import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpSendMailComponent } from './sign-up.component';

describe('SignUpSendMailComponent', () => {
  let component : SignUpSendMailComponent;
  let fixture : ComponentFixture<SignUpSendMailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations : [SignUpSendMailComponent]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpSendMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
