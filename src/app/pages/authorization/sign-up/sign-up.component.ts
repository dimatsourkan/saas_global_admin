import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { AuthDataModel, AuthService } from '../../../core/services/auth.service';
import { ENVIRONMENT } from '../../../../environments/environment';
import { ReCaptcha2Component, ReCaptchaV3Service } from 'ngx-captcha';
import { UserService } from '../../../core/entities/user/user.service';
import { ApiUrlService } from '../../../core/services/api-url.service';
import { Router } from '@angular/router';
import { AppInitializer } from '../../../core/services/app-initializer.service';

@Component({
  selector : 'app-sign-up',
  templateUrl : './sign-up.component.html',
  styleUrls : ['./sign-up.component.scss']
})
export class SignUpComponent {

  @ViewChild('captchaElem') private captchaElem : ReCaptcha2Component;
  loading = false;
  form : FormGroup;

  constructor(private router : Router,
              private authService : AuthService,
              private formBuilder : FormBuilder,
              private userService : UserService,
              private apiUrlService : ApiUrlService,
              private appInitializer : AppInitializer,
              private validatorService : ValidatorService,
              private reCaptchaV3Service : ReCaptchaV3Service
  ) {

    this.form = this.formBuilder.group({
      _username : ['', [Validators.required, Validators.email]],
      _password : ['', [Validators.required, Validators.minLength(8)]],
      _recaptcha_response : [''],
      privacyPolicy : [true]
    });
  }

  signup() {

    this.form.setErrors(null);
    this.form.updateValueAndValidity();

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;

    this.getRacaptcha(() => {

      this.signUpRequest().subscribe(
        res => this.onSuccessSignup(),
        err => this.onErrorLogin(err)
      );
    });

  }

  signUpRequest() {
    return this.authService.signUp(new AuthDataModel(this.form.value));
  }

  getRacaptcha(callback : Function) {
    this.reCaptchaV3Service.execute(ENVIRONMENT.CAPTCHA_KEY, 'login', _recaptcha_response => {
      this.form.patchValue({_recaptcha_response});
      callback()
    });
  }

  onSuccessSignup() {
    this.appInitializer.initialize().then(() => {
      this.router.navigate(['/', this.apiUrlService.linkPathFromRole()]);
    });
  }

  onErrorLogin(err : any) {
    this.loading = false;
    this.validatorService.addErrorToForm(this.form, err);
  }

}
