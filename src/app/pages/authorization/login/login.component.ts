import { Component } from '@angular/core';
import { AuthDataModel, AuthService } from '../../../core/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../../../shared/validation/validation.service';
import { ApiUrlService } from '../../../core/services/api-url.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../core/entities/user/user.service';
import { ROLES } from '../../../core/entities/user/user.enum';
import { role } from '../../../core/entities/user/user.type';
import { hasKey } from '../../../core/helpers/helpers';
import { ENVIRONMENT } from '../../../../environments/environment';
import { ReCaptchaV3Service } from 'ngx-captcha';
import { AppInitializer } from '../../../core/services/app-initializer.service';

@Component({
  selector : 'app-authorization',
  templateUrl : './login.component.html',
  styleUrls : ['./login.component.scss']
})
export class LoginComponent {

  form : FormGroup;
  loading = false;
  /**
   * В зависимости от этого значения запрос будет лететь на соответствующий ендпоинт
   */
  private endpointRole : role = ROLES.TENANT;

  constructor(
    private router : Router,
    private userService : UserService,
    private authService : AuthService,
    private formBuilder : FormBuilder,
    private apiUrlService : ApiUrlService,
    private activatedRoute : ActivatedRoute,
    private appInitializer : AppInitializer,
    private validatorService : ValidatorService,
    private reCaptchaV3Service : ReCaptchaV3Service
  ) {

    this.activatedRoute.data.subscribe(data => {
      if(hasKey(data, 'endpointRole')) {
        this.endpointRole = data.endpointRole;
      }
    });

    this.form = this.formBuilder.group({
      _username : ['', [Validators.required, Validators.email]],
      _password : ['', [Validators.required]],
      _recaptcha_response : ['']
    });

  }

  login() {

    this.form.setErrors(null);
    this.form.updateValueAndValidity();

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;

    this.reCaptchaV3Service.execute(ENVIRONMENT.CAPTCHA_KEY, 'login', _recaptcha_response => {

      this.form.patchValue({_recaptcha_response});

      this.getLoginMethod(new AuthDataModel(this.form.value))
        .subscribe(
          res => this.onSuccessLogin(),
          err => this.onErrorLogin(err)
        )

    });
  }

  getLoginMethod(authData : AuthDataModel) {
    switch(this.endpointRole) {
      case ROLES.MANAGER : return this.authService.managerLogin(authData);
      case ROLES.TENANT : return this.authService.tenantLogin(authData);
      default : return this.authService.tenantLogin(authData);
    }
  }

  onSuccessLogin() {
    this.appInitializer.initialize().then(() => {
      this.loading = false;
      this.router.navigate(['/', this.apiUrlService.linkPathFromRole()]);
    });
  }

  onErrorLogin(err : any) {
    this.loading = false;
    this.validatorService.addErrorToForm(this.form, err)
  }

  get isManagerEndpoint() {
    return this.endpointRole === ROLES.MANAGER;
  }

  get isTenantEndpoint() {
    return this.endpointRole === ROLES.TENANT;
  }

}
