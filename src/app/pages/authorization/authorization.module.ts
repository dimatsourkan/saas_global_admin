import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { LOGIN_ROUTES } from './authorization.routing';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthorizationComponent } from './authorization.component';
import { TranslateModule } from '@ngx-translate/core';
import { ResetPasswordMainComponent } from './reset-password/reset-password-main/reset-password-main.component';
import {
  ResetPasswordConfirmComponent
} from './reset-password/reset-password-confirm/reset-password-confirm.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { NgxCaptchaModule } from 'ngx-captcha';

@NgModule({
  imports : [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TranslateModule,
    NgxCaptchaModule,
    RouterModule.forChild(LOGIN_ROUTES)
  ],
  declarations : [
    LoginComponent,
    SignUpComponent,
    AuthorizationComponent,
    ResetPasswordMainComponent,
    ResetPasswordConfirmComponent
  ],
  providers : []
})
export class AuthorizationModule {
}
