import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthorizationComponent } from './authorization.component';
import { ResetPasswordMainComponent } from './reset-password/reset-password-main/reset-password-main.component';
import {
  ResetPasswordConfirmComponent
} from './reset-password/reset-password-confirm/reset-password-confirm.component';
import { ROLES } from '../../core/entities/user/user.enum';
import { SignUpComponent } from './sign-up/sign-up.component';
import { NotAuthenticated } from '../../core/guards/not-authenticated';

export const LOGIN_ROUTES : Routes = [

  {
    path : '',
    component : AuthorizationComponent,
    children : [
      {
        path : '',
        pathMatch : 'full',
        redirectTo : 'login'
      },

      {
        path : 'login',
        component : LoginComponent
      },

      {
        path : 'signup',
        component : SignUpComponent
      },

      {
        path : 'reset-password',
        component : ResetPasswordMainComponent
      },

      {
        path : 'reset-password/:token',
        component : ResetPasswordConfirmComponent
      },

      /**
       * Для менеджера используются те же компоненты что и для тенанта
       * Только передается значение endpointRole
       * В зависимости от него запрос будет лететь на соответствующий роут
       */
      {
        path : 'manager',
        children : [

          {
            path : '',
            pathMatch : 'full',
            redirectTo : 'login'
          },

          {
            path : 'login',
            canActivate : [NotAuthenticated],
            data : { endpointRole : ROLES.MANAGER },
            component : LoginComponent
          },

          {
            path : 'reset-password',
            canActivate : [NotAuthenticated],
            data : { endpointRole : ROLES.MANAGER },
            component : ResetPasswordMainComponent
          },

          {
            path : 'reset-password/:token',
            canActivate : [NotAuthenticated],
            data : { endpointRole : ROLES.MANAGER },
            component : ResetPasswordConfirmComponent
          },

        ]
      }
    ]
  }

];
