import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../core/services/auth.service';
import { ValidatorService } from '../../../../shared/validation/validation.service';
import { finalize } from 'rxjs/operators';
import { ROLES } from '../../../../core/entities/user/user.enum';
import { role } from '../../../../core/entities/user/user.type';
import { ActivatedRoute } from '@angular/router';
import { hasKey } from '../../../../core/helpers/helpers';

@Component({
  selector: 'app-reset-password-main',
  templateUrl: './reset-password-main.component.html',
  styleUrls: ['./reset-password-main.component.scss']
})
export class ResetPasswordMainComponent {

  loading = false;
  success = false;
  form : FormGroup;
  /**
   * В зависимости от этого значения запрос будет лететь на соответствующий ендпоинт
   */
  private endpointRole : role = ROLES.TENANT;

  constructor(private authService : AuthService,
              private formBuilder : FormBuilder,
              private activatedRoute : ActivatedRoute,
              private validatorService : ValidatorService) {

    this.activatedRoute.data.subscribe(data => {
      if(hasKey(data, 'endpointRole')) {
        this.endpointRole = data.endpointRole;
      }
    });

    this.form = this.formBuilder.group({
      email : ['', [Validators.required, Validators.email]],
      authRole : [ROLES.TENANT, [Validators.required]],
    });
  }

  reset() {

    this.form.setErrors(null);
    this.form.updateValueAndValidity();

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.getResetMethod(this.form.value.email)
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        () => this.onSuccess(),
        err => this.validatorService.addErrorToForm(this.form, err)
      );

  }

  getResetMethod(email : string) {
    switch(this.endpointRole) {
      case ROLES.MANAGER : return this.authService.resetPassManager(email);
      case ROLES.TENANT : return this.authService.resetPassTenant(email);
      default : return this.authService.resetPassTenant(email);
    }
  }

  onSuccess() {
    this.form.reset();
    this.success = true;
  }

}
