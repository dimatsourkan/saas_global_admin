import { Component } from '@angular/core';
import { role } from '../../../../core/entities/user/user.type';
import { ROLES } from '../../../../core/entities/user/user.enum';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../core/services/auth.service';
import { ValidatorService } from '../../../../shared/validation/validation.service';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Notification } from '../../../../core/components/notifications/notifications.model';
import { NOTIFICATION_TYPE } from '../../../../core/components/notifications/notifications.const';
import { NotificationsService } from '../../../../core/components/notifications/notifications.service';
import { hasKey } from '../../../../core/helpers/helpers';
import { LOGIN_ROUTE, MANAGER_LOGIN_ROUTE } from '../../../../app.constants';
import { plainToClass } from 'class-transformer';

@Component({
  selector: 'app-reset-password-confirm',
  templateUrl: './reset-password-confirm.component.html',
  styleUrls: ['./reset-password-confirm.component.scss']
})
export class ResetPasswordConfirmComponent {

  loading = false;
  form : FormGroup;
  /**
   * В зависимости от этого значения запрос будет лететь на соответствующий ендпоинт
   */
  private endpointRole : role = ROLES.TENANT;

  constructor(
    private router : Router,
    private authService : AuthService,
    private formBuilder : FormBuilder,
    private activatedRoute : ActivatedRoute,
    private notificationsService : NotificationsService,
    private validatorService : ValidatorService
  ) {



    this.form = this.formBuilder.group({
      token : [this.activatedRoute.snapshot.params.token, [Validators.required]],
      password : ['', [Validators.required, Validators.minLength(8)]],
    });

    this.activatedRoute.data.subscribe(data => {
      if(hasKey(data, 'endpointRole')) {
        this.endpointRole = data.endpointRole;
      }
    });
  }

  reset() {

    this.form.setErrors(null);
    this.form.updateValueAndValidity();

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.getResetMethod(this.form.value.password, this.form.value.token)
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        () => this.onSuccess(),
        err => this.validatorService.addErrorToForm(this.form, err)
      );

  }

  getResetMethod(password : string, token : string) {
    switch(this.endpointRole) {
      case ROLES.MANAGER : return this.authService.resetPassConfirmManager(password, password, token);
      case ROLES.TENANT : return this.authService.resetPassConfirmTenant(password, password, token);
      default : return this.authService.resetPassConfirmTenant(password, password, token);
    }
  }

  onSuccess() {
    this.form.reset();
    this.notificationsService.push(Notification.plainToClass({
      time : 5000,
      type : NOTIFICATION_TYPE.SUCCESS,
      title : 'Success',
      message : 'Your password has been changed, you can log in'
    }));

    switch(this.endpointRole) {
      case ROLES.MANAGER : this.router.navigate([MANAGER_LOGIN_ROUTE]); break;
      case ROLES.TENANT : this.router.navigate([LOGIN_ROUTE]); break;
      default : this.router.navigate([LOGIN_ROUTE]); break;
    }
  }

}
