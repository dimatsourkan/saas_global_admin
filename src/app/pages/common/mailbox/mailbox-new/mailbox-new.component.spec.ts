import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailboxNewComponent } from './mailbox-new.component';

describe('MailboxNewComponent', () => {
  let component: MailboxNewComponent;
  let fixture: ComponentFixture<MailboxNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailboxNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailboxNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
