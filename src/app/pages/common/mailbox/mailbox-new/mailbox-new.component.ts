import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { ModalComponent } from '../../../../shared/components/modals/modal/modal.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MailboxService } from '../../../../core/entities/mailbox/mailbox.service';
import { Mailbox } from '../../../../core/entities/mailbox/mailbox.model';
import { finalize } from 'rxjs/operators';
import { ValidatorService } from '../../../../shared/validation/validation.service';
import { AuthService } from '../../../../core/services/auth.service';
import { NotificationsService } from '../../../../core/components/notifications/notifications.service';
import { plainToClass } from 'class-transformer';

@Component({
  selector: 'app-mailbox-new',
  templateUrl: './mailbox-new.component.html',
  styleUrls: ['./mailbox-new.component.scss']
})
export class MailboxNewComponent {

  @ViewChild('createModal') private createModal : ModalComponent;
  @Output() success = new EventEmitter();
  loading = false;
  form : FormGroup;

  constructor(
    private authService : AuthService,
    private formBuilder : FormBuilder,
    private mailboxService : MailboxService,
    private validatorService : ValidatorService,
    private notificationsService : NotificationsService
  ) {
    this.form = this.formBuilder.group({
      to : ['', this.isManager ? Validators.required : null],
      subject : ['', Validators.required],
      message : ['', Validators.required]
    });
  }

  send() {

    if(this.form.invalid) {
      return this.validatorService.setTouchToControls(this.form);
    }

    this.loading = true;
    this.mailboxService.save(Mailbox.plainToClass(this.form.value))
      .pipe(finalize(() => this.loading = false))
      .subscribe(
        () => this.onSuccess(),
        err => this.validatorService.addErrorToForm(this.form, err)
      )
  }

  onSuccess() {
    this.close();
    this.success.emit();
    this.notificationsService.success('Message send successfully');
  }

  open() {
    this.form.reset();
    this.createModal.open();
  }

  close() {
    this.createModal.close();
  }

  get isManager() {
    return this.authService.isManager;
  }

}
