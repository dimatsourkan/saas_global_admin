import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MAILBOX_ROUTES } from './mailbox.routing';
import { SharedModule } from '../../../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MailboxComponent } from './mailbox.component';
import { MailboxNewComponent } from './mailbox-new/mailbox-new.component';
import { MailboxInfoComponent } from './mailbox-info/mailbox-info.component';
@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(MAILBOX_ROUTES)
  ],
  declarations : [
    MailboxComponent,
    MailboxNewComponent,
    MailboxInfoComponent
  ]
})
export class MailboxModule {
}
