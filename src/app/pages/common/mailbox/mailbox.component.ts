import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MailboxInfoComponent } from './mailbox-info/mailbox-info.component';
import { ConfirmComponent } from '../../../shared/components/modals/confirm/confirm.component';
import { Helpers } from '../../../core/helpers/helpers';
import { DataStore } from '../../../core/data-store/data-store.service';
import { merge, Subscription } from 'rxjs';
import { FilterService } from '../../../core/services/filter.service';
import { AuthService } from '../../../core/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { MailboxService } from '../../../core/entities/mailbox/mailbox.service';
import { Mailbox } from '../../../core/entities/mailbox/mailbox.model';
import { finalize } from 'rxjs/operators';
import { MESSAGE_TYPES } from '../../../core/entities/mailbox/mailbox.enum';

@Component({
  selector : 'app-mailbox',
  templateUrl : './mailbox.component.html',
  styleUrls : ['./mailbox.component.scss']
})
export class MailboxComponent  implements OnDestroy, OnInit {

  @ViewChild('mailboxInfo') private mailboxInfo : MailboxInfoComponent;
  @ViewChild('confirm') private confirm : ConfirmComponent;
  helpers : Helpers = new Helpers();
  list$ = DataStore.mailbox.list.asObservable$;
  subscription$ : Subscription;
  loading : boolean = false;
  filter : FilterService;
  messageType : string;

  constructor(
    private authService : AuthService,
    private activatedRoute : ActivatedRoute,
    private mailboxService : MailboxService
  ) {
    this.messageType = this.activatedRoute.snapshot.data['type'];
    this.filter = new FilterService(true);
  }

  ngOnInit() {
    this.subscription$ = merge(
      this.activatedRoute.url,
      this.filter.onChangeFilter$
    ).subscribe(() => this.getMailbox());
  }

  ngOnDestroy() {
    this.subscription$.unsubscribe();
  }

  getMailboxMethod() {
    switch (this.messageType) {
      case MESSAGE_TYPES.STARRED : return this.mailboxService.starred(this.filter.filter);
      case MESSAGE_TYPES.INBOX : return this.mailboxService.inbox(this.filter.filter);
      case MESSAGE_TYPES.SENT : return this.mailboxService.sent(this.filter.filter);
    }
  }

  getMailbox() {
    this.loading = true;
    this.getMailboxMethod()
      .pipe(finalize(() => this.loading = false))
      .subscribe();
  }

  deleteMail(mail : Mailbox) {
    this.confirm.open(() => {
      this.loading = true;
      this.confirm.close();
      this.mailboxService.delete(mail.id)
        .pipe(finalize(() => this.loading = false))
        .subscribe(() => this.getMailbox());
    });
  }

  toggleStarred(mail : Mailbox) {
    this.mailboxService.toggleStarred(mail.id).subscribe()
  }

  onSendMessage() {
    if(this.messageType === MESSAGE_TYPES.SENT) {
      this.getMailbox();
    }
  }

  readMail(mail : Mailbox) {
    this.mailboxInfo.open(mail);
  }

  get isManager() {
    return this.authService.isManager;
  }

}
