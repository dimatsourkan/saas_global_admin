import { Routes } from '@angular/router';
import { MESSAGE_TYPES } from '../../../core/entities/mailbox/mailbox.enum';
import { MailboxComponent } from './mailbox.component';

export const MAILBOX_ROUTES : Routes = [

  {
    path : '',
    pathMatch : 'full',
    redirectTo : 'inbox'
  },
  {
    path : 'inbox',
    data : { type : MESSAGE_TYPES.INBOX },
    component : MailboxComponent
  },
  {
    path : 'starred',
    data : { type : MESSAGE_TYPES.STARRED },
    component : MailboxComponent
  },
  {
    path : 'sent',
    data : { type : MESSAGE_TYPES.SENT },
    component : MailboxComponent
  }

];
