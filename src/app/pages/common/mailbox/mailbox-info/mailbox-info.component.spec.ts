import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailboxInfoComponent } from './mailbox-info.component';

describe('MailboxInfoComponent', () => {
  let component: MailboxInfoComponent;
  let fixture: ComponentFixture<MailboxInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailboxInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailboxInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
