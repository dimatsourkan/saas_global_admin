import { Component, ViewChild } from '@angular/core';
import { Mailbox } from '../../../../core/entities/mailbox/mailbox.model';
import { ModalComponent } from '../../../../shared/components/modals/modal/modal.component';
import { MailboxService } from '../../../../core/entities/mailbox/mailbox.service';

@Component({
  selector: 'app-mailbox-info',
  templateUrl: './mailbox-info.component.html',
  styleUrls: ['./mailbox-info.component.scss']
})
export class MailboxInfoComponent {

  @ViewChild('modal') private modal : ModalComponent;

  item : Mailbox;

  constructor(private mailboxService : MailboxService) {

  }

  open(mail : Mailbox) {
    this.item = mail;
    this.modal.open();
    this.markRead();
  }

  close() {
    this.modal.close();
  }

  private markRead() {
    if(this.item.read) {
      return;
    }
    this.mailboxService.markRead(this.item.id).subscribe()
  }

}
